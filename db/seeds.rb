#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Event.create(name:'Создание')						#1
Event.create(name:'Удаление')						#2
Event.create(name:'Изменение')						#3
Event.create(name:'Перемещение')					#4
Event.create(name:'Списание')						#5
Event.create(name:'Ремонт')							#6
Event.create(name:'Проверка')						#7
Event.create(name:'Передача')						#8
Event.create(name:'Возврат')						#9
Event.create(name:'Восстановление из списания')		#10

Location.create(name:'Склад')						#1
Location.create(name:'Списание')					#2
Location.create(name:'Пользователь')				#3
Location.create(name:'Внутреннее использование')	#4
Location.create(name:'Ремонт')						#5
Location.create(name:'Комплект')					#6
Location.create(name:'Временное использование')		#7

