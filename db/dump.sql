-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- Dump data of "bundle_checks" ----------------------------
/*!40000 ALTER TABLE `bundle_checks` DISABLE KEYS */

BEGIN;

INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '1', 'admin', '1', NULL, '2014-04-20 16:01:13', '2014-04-20 16:01:13', 'ыва' );
INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '2', 'admin', '1', NULL, '2014-04-20 16:02:07', '2014-04-20 16:02:07', 'ыва' );
INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '3', 'admin', '1', NULL, '2014-04-20 16:04:47', '2014-04-20 16:04:47', 'примечание' );
INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '4', 'admin', '1', NULL, '2014-04-20 16:05:45', '2014-04-20 16:05:45', 'в' );
INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '5', 'admin', '4', NULL, '2014-04-20 16:06:27', '2014-04-20 16:06:27', '234' );
INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '6', 'admin', '4', '18', '2014-04-20 16:08:10', '2014-04-20 16:08:10', 'ууу' );
INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '7', 'admin', '1', '1', '2014-04-20 16:08:21', '2014-04-20 16:08:21', '1212' );
INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '8', 'admin', '4', '18', '2014-04-20 16:16:34', '2014-04-20 16:16:34', 'лло' );
INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '9', 'admin', '4', '18', '2014-04-20 16:17:08', '2014-04-20 16:17:08', 'лло' );
INSERT INTO `bundle_checks`(`id`,`username`,`bundle_id`,`external_user_id`,`created_at`,`updated_at`,`note`) VALUES ( '10', 'admin', '4', '18', '2014-04-20 16:17:08', '2014-04-20 16:17:08', 'цу' );
COMMIT;
/*!40000 ALTER TABLE `bundle_checks` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "bundle_histories" -------------------------
/*!40000 ALTER TABLE `bundle_histories` DISABLE KEYS */

BEGIN;

INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '1', '1', '', 'admin', NULL, '1', '1', '2014-04-03 18:00:52', '2014-04-03 18:00:52', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '2', '1', 'ДК14', 'admin', NULL, '3', '1', '2014-04-03 18:01:04', '2014-04-03 18:01:04', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '3', '1', 'Комплект: ДК14 передан: Иванов  ', 'admin', NULL, '8', '1', '2014-04-05 13:54:55', '2014-04-05 13:54:55', '1' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '4', '2', '', 'admin', NULL, '1', '2', '2014-04-09 19:25:21', '2014-04-09 19:25:21', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '5', '3', '', 'admin', NULL, '1', '3', '2014-04-09 19:25:26', '2014-04-09 19:25:26', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '6', '4', '', 'admin', NULL, '1', '4', '2014-04-09 19:26:17', '2014-04-09 19:26:17', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '7', '4', '234', 'admin', NULL, '3', '4', '2014-04-09 19:26:20', '2014-04-09 19:26:20', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '8', '5', '', 'admin', NULL, '1', '5', '2014-04-09 19:26:22', '2014-04-09 19:26:22', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '9', '5', '234', 'admin', NULL, '3', '5', '2014-04-09 19:26:24', '2014-04-09 19:26:24', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '10', '6', '', 'admin', NULL, '1', '6', '2014-04-13 07:54:04', '2014-04-13 07:54:04', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '11', '6', '123', 'admin', NULL, '3', '6', '2014-04-13 07:54:10', '2014-04-13 07:54:10', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '12', '7', '', 'admin', NULL, '1', '7', '2014-04-13 08:02:22', '2014-04-13 08:02:22', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '13', '7', '334', 'admin', NULL, '3', '7', '2014-04-13 08:02:25', '2014-04-13 08:02:25', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '14', '2', '123', 'admin', NULL, '3', '2', '2014-04-13 09:51:12', '2014-04-13 09:51:12', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '15', '4', '2341', 'admin', NULL, '3', '4', '2014-04-13 10:29:34', '2014-04-13 10:29:34', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '16', '4', '2341й', 'admin', NULL, '3', '4', '2014-04-13 10:29:53', '2014-04-13 10:29:53', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '17', '8', '', 'admin', NULL, '1', '8', '2014-04-20 07:40:39', '2014-04-20 07:40:39', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '18', '8', 'компл1', 'admin', NULL, '3', '8', '2014-04-20 07:40:44', '2014-04-20 07:40:44', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '19', '4', 'Комплект: 2341й передан: Преп 2 Имя Отчество', 'admin', NULL, '8', '4', '2014-04-20 16:06:18', '2014-04-20 16:06:18', '18' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '20', '4', '', 'admin', NULL, '7', '4', '2014-04-20 16:17:08', '2014-04-20 16:17:08', '18' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '21', '4', '', 'admin', NULL, '7', '4', '2014-04-20 16:17:08', '2014-04-20 16:17:08', '18' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '22', '9', '', 'admin', NULL, '1', '9', '2014-04-21 18:10:34', '2014-04-21 18:10:34', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '23', '10', '', 'admin', NULL, '1', '10', '2014-04-21 18:12:46', '2014-04-21 18:12:46', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '24', '10', '', 'admin', NULL, '3', '10', '2014-04-21 18:12:52', '2014-04-21 18:12:52', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '25', '2', 'Комплект: 123 премещён в кабинет: 100(2)', 'admin', NULL, '4', '2', '2014-04-27 18:24:07', '2014-04-27 18:24:07', '3' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '26', '8', 'Комплект: компл1 премещён в кабинет: 101(1)', 'admin', NULL, '4', '8', '2014-04-27 18:24:29', '2014-04-27 18:24:29', '1' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '27', '7', '', 'admin', NULL, '3', '7', '2014-05-22 17:08:33', '2014-05-22 17:08:33', '0' );
INSERT INTO `bundle_histories`(`id`,`before`,`after`,`username`,`actiondate`,`event_id`,`bundle_id`,`created_at`,`updated_at`,`external_users_id`) VALUES ( '28', '7', 'Комплект: 1-1(1) передан: Преп 1sdf Aaa ', 'admin', NULL, '8', '7', '2014-05-22 17:08:43', '2014-05-22 17:08:43', '16' );
COMMIT;
/*!40000 ALTER TABLE `bundle_histories` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "bundles" ----------------------------------
/*!40000 ALTER TABLE `bundles` DISABLE KEYS */

BEGIN;

INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '1', 'ДК14', '', '2014-04-03 18:00:52', '2014-04-05 13:54:55', '3', '1' );
INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '2', '123', '', '2014-04-09 19:25:21', '2014-04-27 18:24:07', '4', '3' );
INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '3', '', '', '2014-04-09 19:25:26', '2014-04-09 19:25:26', '1', '0' );
INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '4', '2341й', '', '2014-04-09 19:26:17', '2014-04-20 16:06:18', '3', '18' );
INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '5', '234', '', '2014-04-09 19:26:22', '2014-04-09 19:26:24', '1', '0' );
INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '6', '123', '', '2014-04-13 07:54:04', '2014-04-13 07:54:10', '1', '0' );
INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '7', '1-1(1)', '', '2014-04-13 08:02:22', '2014-05-22 17:08:43', '3', '16' );
INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '8', 'компл1', '', '2014-04-20 07:40:39', '2014-04-27 18:24:29', '4', '1' );
INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '9', '', '', '2014-04-21 18:10:34', '2014-04-21 18:10:34', '1', '0' );
INSERT INTO `bundles`(`id`,`title`,`note`,`created_at`,`updated_at`,`location_id`,`external_users_id`) VALUES ( '10', 'компл2', '', '2014-04-21 18:12:46', '2014-04-21 18:12:52', '1', '0' );
COMMIT;
/*!40000 ALTER TABLE `bundles` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "events" -----------------------------------
/*!40000 ALTER TABLE `events` DISABLE KEYS */

BEGIN;

INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '1', 'Создание', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '2', 'Удаление', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '3', 'Изменение', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '4', 'Перемещение', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '5', 'Списание', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '6', 'Ремонт', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '7', 'Проверка', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '8', 'Передача', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '9', 'Возврат', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `events`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '10', 'Восстановление из списания', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
COMMIT;
/*!40000 ALTER TABLE `events` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "external_users" ---------------------------
/*!40000 ALTER TABLE `external_users` DISABLE KEYS */

BEGIN;

INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '1', '0', 'Иванов', '2цу', '2уу', 'адрес', '123-123', '2', '22', '2', '2', '0', '2014-04-08 04:00:00', '2014-04-27 04:00:00', '1', '2014-04-05 13:54:39', '2014-04-21 18:37:22', 'провайдер 2' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '5', '1', '11', '', '', '', '', '', '', '', '', '1', NULL, NULL, NULL, '2014-04-09 19:27:23', '2014-04-09 19:47:20', '' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '10', '0', 'Fghsd', 'Sdfhg', 'Sdjfhgsd', '', '', '', '', '', '', '0', NULL, NULL, NULL, '2014-04-09 19:30:16', '2014-04-12 11:18:38', '' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '16', '1', 'Преп 1sdf', 'Aaa', '', 'улица город  дом', '', '', '', '', '', '0', '2014-05-21 00:00:00', '2014-05-28 00:00:00', '7', '2014-04-09 19:36:54', '2014-05-22 17:15:46', 'провайдер 1' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '17', '0', '12', 'Fgh', '', '', '', '', '', '', '', '0', NULL, NULL, NULL, '2014-04-09 19:37:16', '2014-04-13 12:56:27', 'провайдер 3' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '18', '1', 'Преп 2', 'Имя', 'Отчество', '', '', '', '', '', '', '0', NULL, NULL, '4', '2014-04-11 17:54:08', '2014-04-27 19:00:34', 'провайдер 2' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '19', '0', 'Qq', 'Ww', 'Eee', 'q', '', '', '', '', '', '0', NULL, NULL, NULL, '2014-04-12 10:26:44', '2014-04-12 11:54:30', 'w' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '20', '1', 'Преп 3', 'Qqq', '', '', '', '', '', '', '', '0', NULL, NULL, NULL, '2014-04-13 12:50:58', '2014-04-16 17:05:31', '' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '21', '1', 'Фамилия', 'Fgh', 'Sdjfhgsd', '', '', '', '', '', '', '0', NULL, NULL, NULL, '2014-04-13 12:51:08', '2014-04-13 13:14:29', 'провайдер 2' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '28', '0', '', '', '', '', '', '', '', '', '', '0', NULL, NULL, NULL, '2014-04-27 18:31:52', '2014-04-27 18:31:52', '' );
INSERT INTO `external_users`(`id`,`is_teacher`,`surname`,`name`,`patronymic`,`address`,`phone`,`email`,`skype`,`vklogin`,`password`,`archive`,`installdate`,`uninstalldate`,`bundle_id`,`created_at`,`updated_at`,`provider`) VALUES ( '29', '0', 'Фамилия 2', '', '', '', '', '', '', '', '', '0', NULL, NULL, NULL, '2014-04-27 18:37:41', '2014-04-27 18:37:52', '' );
COMMIT;
/*!40000 ALTER TABLE `external_users` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "internal_users" ---------------------------
/*!40000 ALTER TABLE `internal_users` DISABLE KEYS */

BEGIN;

INSERT INTO `internal_users`(`id`,`room`,`place`,`note`,`created_at`,`updated_at`) VALUES ( '1', '101', '1', '', '2014-04-27 04:31:22', '2014-04-27 04:33:58' );
INSERT INTO `internal_users`(`id`,`room`,`place`,`note`,`created_at`,`updated_at`) VALUES ( '3', '100', '2', 'главный', '2014-04-27 04:32:23', '2014-04-27 05:27:52' );
COMMIT;
/*!40000 ALTER TABLE `internal_users` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "item_checks" ------------------------------
/*!40000 ALTER TABLE `item_checks` DISABLE KEYS */

BEGIN;

INSERT INTO `item_checks`(`id`,`username`,`item_id`,`created_at`,`updated_at`) VALUES ( '1', 'admin', NULL, '2014-04-16 20:15:46', '2014-04-16 20:15:46' );
INSERT INTO `item_checks`(`id`,`username`,`item_id`,`created_at`,`updated_at`) VALUES ( '2', 'admin', '11', '2014-04-16 20:16:18', '2014-04-16 20:16:18' );
INSERT INTO `item_checks`(`id`,`username`,`item_id`,`created_at`,`updated_at`) VALUES ( '3', 'admin', '12', '2014-04-16 20:41:59', '2014-04-16 20:41:59' );
INSERT INTO `item_checks`(`id`,`username`,`item_id`,`created_at`,`updated_at`) VALUES ( '4', 'admin', '11', '2014-04-16 20:41:59', '2014-04-16 20:41:59' );
INSERT INTO `item_checks`(`id`,`username`,`item_id`,`created_at`,`updated_at`) VALUES ( '5', 'admin', '13', '2014-04-16 20:41:59', '2014-04-16 20:41:59' );
INSERT INTO `item_checks`(`id`,`username`,`item_id`,`created_at`,`updated_at`) VALUES ( '6', 'admin', '11', '2014-04-16 20:42:50', '2014-04-16 20:42:50' );
INSERT INTO `item_checks`(`id`,`username`,`item_id`,`created_at`,`updated_at`) VALUES ( '7', 'admin', '12', '2014-04-16 20:43:41', '2014-04-16 20:43:41' );
INSERT INTO `item_checks`(`id`,`username`,`item_id`,`created_at`,`updated_at`) VALUES ( '8', 'admin', NULL, '2014-04-20 16:00:50', '2014-04-20 16:00:50' );
COMMIT;
/*!40000 ALTER TABLE `item_checks` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "item_histories" ---------------------------
/*!40000 ALTER TABLE `item_histories` DISABLE KEYS */

BEGIN;

INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '1', '', 'admin', '1', NULL, NULL, '2014-04-03 18:01:12', '2014-04-03 18:01:12', '1' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '2', 'Элемент 1; 1111; ; ; 0; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-03 18:01:19', '2014-04-03 18:01:19', '1' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '3', 'Из комплекта на склад', 'admin', '4', NULL, NULL, '2014-04-03 18:01:29', '2014-04-03 18:01:29', '1' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '4', 'Из комплекта на склад', 'admin', '4', NULL, NULL, '2014-04-03 18:01:29', '2014-04-03 18:01:29', '1' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '5', '', 'admin', '1', NULL, NULL, '2014-04-05 13:54:49', '2014-04-05 13:54:49', '2' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '6', 'э1; ; ; ; 0; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-05 13:54:53', '2014-04-05 13:54:53', '2' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '7', 'э1; ; ; ; 0; 100000.0; ', 'admin', '3', NULL, NULL, '2014-04-09 17:43:25', '2014-04-09 17:43:25', '2' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '8', 'Элемент 1; 1111; ; ; 0; 100002.0; ', 'admin', '3', NULL, NULL, '2014-04-09 18:03:01', '2014-04-09 18:03:01', '1' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '9', '', 'admin', '1', NULL, NULL, '2014-04-09 18:07:26', '2014-04-09 18:07:26', '3' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '10', 'rere; ; ; ; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-09 18:07:32', '2014-04-09 18:07:32', '3' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '11', '', 'admin', '1', NULL, NULL, '2014-04-09 18:08:23', '2014-04-09 18:08:23', '4' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '12', 'qwqw; ; ; ; 2; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-09 18:08:30', '2014-04-09 18:08:30', '4' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '13', '', 'admin', '1', NULL, NULL, '2014-04-09 18:20:25', '2014-04-09 18:20:25', '5' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '14', '', 'admin', '1', NULL, NULL, '2014-04-09 18:21:53', '2014-04-09 18:21:53', '6' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '15', 'aawe; ; ; ; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-09 18:21:58', '2014-04-09 18:21:58', '6' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '16', '', 'admin', '1', NULL, NULL, '2014-04-09 18:22:28', '2014-04-09 18:22:28', '7' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '17', 'werwer; ; ; ; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-09 18:22:35', '2014-04-09 18:22:35', '7' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '18', 'werwer; ---------------; ; ; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-09 18:22:35', '2014-04-09 18:22:35', '7' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '19', '', 'admin', '1', NULL, NULL, '2014-04-09 18:23:36', '2014-04-09 18:23:36', '8' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '20', '', 'admin', '1', NULL, NULL, '2014-04-09 18:24:30', '2014-04-09 18:24:30', '9' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '21', '', 'admin', '1', NULL, NULL, '2014-04-09 18:24:42', '2014-04-09 18:24:42', '10' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '22', 'dd; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-09 18:24:50', '2014-04-09 18:24:50', '10' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '23', '', 'admin', '1', NULL, NULL, '2014-04-09 18:25:14', '2014-04-09 18:25:14', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '24', '', 'admin', '1', NULL, NULL, '2014-04-13 08:02:27', '2014-04-13 08:02:27', '12' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '25', 'йцу; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-13 08:02:30', '2014-04-13 08:02:30', '12' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '26', 'э1; 1; ; ; 0; 100000.0; ', 'admin', '3', NULL, NULL, '2014-04-13 10:33:05', '2014-04-13 10:33:05', '2' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '27', 'qwqw; 12; ; 234; 2; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-13 10:33:24', '2014-04-13 10:33:24', '4' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '28', '', 'admin', '1', NULL, NULL, '2014-04-13 10:33:26', '2014-04-13 10:33:26', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '29', '123; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-13 10:38:58', '2014-04-13 10:38:58', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '30', 'В место ремонта 1', 'admin', '6', NULL, '1', '2014-04-13 14:37:54', '2014-04-13 14:37:54', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '31', 'В Мастерская 1', 'admin', '6', NULL, '2', '2014-04-13 14:39:23', '2014-04-13 14:39:23', '4' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '32', 'Из комплекта на склад', 'admin', '4', NULL, NULL, '2014-04-13 16:06:15', '2014-04-13 16:06:15', '2' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '33', '', 'admin', '5', NULL, NULL, '2014-04-13 16:06:15', '2014-04-13 16:06:15', '2' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '34', 'Из комплекта на склад', 'admin', '4', NULL, NULL, '2014-04-13 16:06:22', '2014-04-13 16:06:22', '4' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '35', '', 'admin', '5', NULL, NULL, '2014-04-13 16:06:22', '2014-04-13 16:06:22', '4' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '36', '; ---------------; ; ---------------; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-13 19:32:41', '2014-04-13 19:32:41', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '37', 'aawe; ; ; ; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-15 20:15:30', '2014-04-15 20:15:30', '5' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '38', 'aawe; ---------------; ; ; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-15 20:15:30', '2014-04-15 20:15:30', '5' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '40', '', 'admin', '5', NULL, NULL, '2014-04-15 20:15:57', '2014-04-15 20:15:57', '14' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '41', '', 'admin', '1', NULL, NULL, '2014-04-09 18:20:25', '2014-04-09 18:20:25', '14' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '42', 'aawe; ; ; ; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-15 20:15:30', '2014-04-15 20:15:30', '14' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '43', 'aawe; ---------------; ; ; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-15 20:15:30', '2014-04-15 20:15:30', '14' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '44', 'Cписание нескольких штук:2 из 4', 'admin', '5', NULL, NULL, '2014-04-15 20:15:57', '2014-04-15 20:15:57', '5' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '45', 'aawe; ; ; ; 2; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-15 20:15:57', '2014-04-15 20:15:57', '5' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '46', 'aawe; ---------------; ; ; 2; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-15 20:15:57', '2014-04-15 20:15:57', '5' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '47', 'йцу; ---------------; ; ---------------; 1; 123.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:05:39', '2014-04-16 17:05:39', '12' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '48', 'йцу; ; ; ---------------; 1; 123.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:05:45', '2014-04-16 17:05:45', '12' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '49', 'йцу; ---------------; ; ---------------; 1; 123.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:05:45', '2014-04-16 17:05:45', '12' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '50', '123; ; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:05:53', '2014-04-16 17:05:53', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '51', '123; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:05:53', '2014-04-16 17:05:53', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '52', '; ; ; ---------------; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:06:07', '2014-04-16 17:06:07', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '53', '; ---------------; ; ---------------; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:06:07', '2014-04-16 17:06:07', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '54', '123; ; ; ; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:06:14', '2014-04-16 17:06:14', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '55', '123; ---------------; ; ; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:06:14', '2014-04-16 17:06:14', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '56', '; ; ; ; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:06:17', '2014-04-16 17:06:17', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '57', '; ---------------; ; ; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:06:17', '2014-04-16 17:06:17', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '58', '123; 123; ; ; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 17:06:22', '2014-04-16 17:06:22', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '59', '12344; 1122; ; ; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-16 20:30:19', '2014-04-16 20:30:19', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '60', '', 'admin', '7', NULL, NULL, '2014-04-16 20:42:50', '2014-04-16 20:42:50', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '61', '', 'admin', '7', NULL, NULL, '2014-04-16 20:43:41', '2014-04-16 20:43:41', '12' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '62', '', 'admin', '1', NULL, NULL, '2014-04-20 07:40:46', '2014-04-20 07:40:46', '15' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '63', 'эл1; ---------------; ; ---------------; 1; 123.0; ', 'admin', '3', NULL, NULL, '2014-04-20 07:40:54', '2014-04-20 07:40:54', '15' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '64', '', 'admin', '1', NULL, NULL, '2014-04-20 07:40:56', '2014-04-20 07:40:56', '16' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '65', 'эл2; ---------------; ; ---------------; 1; 223.0; ', 'admin', '3', NULL, NULL, '2014-04-20 07:41:01', '2014-04-20 07:41:01', '16' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '66', 'эл2; ---------------; ; ---------------; 1; 23.0; ', 'admin', '3', NULL, NULL, '2014-04-20 07:41:32', '2014-04-20 07:41:32', '16' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '67', 'эл2; ; ; ---------------; 1; 123.0; ', 'admin', '3', NULL, NULL, '2014-04-20 07:41:40', '2014-04-20 07:41:40', '16' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '68', 'эл2; ---------------; ; ---------------; 1; 123.0; ', 'admin', '3', NULL, NULL, '2014-04-20 07:41:40', '2014-04-20 07:41:40', '16' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '69', 'эл2; 2; ; ---------------; 1; 123.0; ', 'admin', '3', NULL, NULL, '2014-04-20 07:41:45', '2014-04-20 07:41:45', '16' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '70', 'эл1; ; ; ---------------; 1; 1123.0; ', 'admin', '3', NULL, NULL, '2014-04-20 07:41:58', '2014-04-20 07:41:58', '15' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '71', 'эл1; ---------------; ; ---------------; 1; 1123.0; ', 'admin', '3', NULL, NULL, '2014-04-20 07:41:58', '2014-04-20 07:41:58', '15' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '72', '', 'admin', '1', NULL, NULL, '2014-04-20 16:06:09', '2014-04-20 16:06:09', '17' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '73', 'итем 1; ---------------; ; ---------------; 1; 100.0; ', 'admin', '3', NULL, NULL, '2014-04-20 16:06:16', '2014-04-20 16:06:16', '17' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '74', '', 'admin', '1', NULL, NULL, '2014-04-21 18:10:54', '2014-04-21 18:10:54', '18' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '75', '', 'admin', '1', NULL, NULL, '2014-04-21 18:12:18', '2014-04-21 18:12:18', '19' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '76', 'эл2; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:12:28', '2014-04-21 18:12:28', '19' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '77', '', 'admin', '1', NULL, NULL, '2014-04-21 18:12:54', '2014-04-21 18:12:54', '20' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '78', '344; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:12:58', '2014-04-21 18:12:58', '20' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '79', 'эл2; 21; ; ---------------; 1; 123.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:13:24', '2014-04-21 18:13:24', '16' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '80', 'эл1; 22; ; ---------------; 1; 1123.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:13:30', '2014-04-21 18:13:30', '15' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '81', 'эл1; 12341234; ; ---------------; 1; 1123.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:14:15', '2014-04-21 18:14:15', '15' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '82', '12344; 1122; ; ; 2; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:31:10', '2014-04-21 18:31:10', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '83', '12344; 1122; ; ; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:37:39', '2014-04-21 18:37:39', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '84', '12344; 1122; ; 1; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:51:25', '2014-04-21 18:51:25', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '85', '12344; 1122; ; 11; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:52:35', '2014-04-21 18:52:35', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '86', '12344; 1122; ; 111; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 18:54:32', '2014-04-21 18:54:32', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '87', '12344; 1122; ; 1; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:08:19', '2014-04-21 19:08:19', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '88', '12344; 1122; ; 11; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:08:56', '2014-04-21 19:08:56', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '89', '12344; 1122; ; 112; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:09:35', '2014-04-21 19:09:35', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '90', '12344; 1122; ; 1123; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:10:07', '2014-04-21 19:10:07', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '91', '12344; 1122; ; 11231; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:10:40', '2014-04-21 19:10:40', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '92', '12344; 1122; ; 1123; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:12:10', '2014-04-21 19:12:10', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '93', '12344; 1122; ; 112; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:12:51', '2014-04-21 19:12:51', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '94', '12344; 1122; ; 1121; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:13:33', '2014-04-21 19:13:33', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '95', '12344; 1122; ; 112; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:14:28', '2014-04-21 19:14:28', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '96', '12344; 1122; ; 11; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:15:13', '2014-04-21 19:15:13', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '97', '12344; 1122; ; 113; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:16:30', '2014-04-21 19:16:30', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '98', '12344; 1122; ; 1132; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:19:09', '2014-04-21 19:19:09', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '99', '12344; 1122; ; 113; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:19:48', '2014-04-21 19:19:48', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '100', '12344; 1122; ; 1131; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:20:50', '2014-04-21 19:20:50', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '101', '12344; 1122; ; 11311; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:21:09', '2014-04-21 19:21:09', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '102', '12344; 1122; ; 1131; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:21:42', '2014-04-21 19:21:42', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '103', '12344; 1122; ; 113; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:22:53', '2014-04-21 19:22:53', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '104', '12344; 1122; ; 1132; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:23:16', '2014-04-21 19:23:16', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '105', '12344; 1122; ; 113; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:23:35', '2014-04-21 19:23:35', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '106', '12344; 1122; ; 11; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:24:09', '2014-04-21 19:24:09', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '107', '12344; 1122; ; 1; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:25:56', '2014-04-21 19:25:56', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '108', '12344; 1122; ; 12; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:26:48', '2014-04-21 19:26:48', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '109', '12344; 1122; ; 1; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:27:12', '2014-04-21 19:27:12', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '110', '12344; 1122; ; 12; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:27:40', '2014-04-21 19:27:40', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '111', '12344; 1122; ; 1; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:28:36', '2014-04-21 19:28:36', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '112', '12344; 1122; ; 12; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:31:45', '2014-04-21 19:31:45', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '113', '12344; 1122; ; 1; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:32:01', '2014-04-21 19:32:01', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '114', '12344; 1122; ; 12; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:32:33', '2014-04-21 19:32:33', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '115', '12344; 1122; ; 1; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:33:21', '2014-04-21 19:33:21', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '116', '12344; 1122; ; 12; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:33:45', '2014-04-21 19:33:45', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '117', '12344; 1122; ; 1; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:34:27', '2014-04-21 19:34:27', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '118', '12344; 1122; ; 12; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:34:52', '2014-04-21 19:34:52', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '119', '12344; 1122; ; 123; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:36:20', '2014-04-21 19:36:20', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '120', '12344; 1122; ; 121; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:37:11', '2014-04-21 19:37:11', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '121', '12344; 1122; ; 12; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:37:36', '2014-04-21 19:37:36', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '122', '; 1122; ; 1; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:37:45', '2014-04-21 19:37:45', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '123', '; 1121; ; 1; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:37:52', '2014-04-21 19:37:52', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '124', '; 1122; ; 1; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:37:58', '2014-04-21 19:37:58', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '125', '; 1122; ; 12; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:39:29', '2014-04-21 19:39:29', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '126', '; 1122; ; 1211; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:39:38', '2014-04-21 19:39:38', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '127', '; 1122; ; 12; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:40:06', '2014-04-21 19:40:06', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '128', '12344; 1122; ; 1; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:41:45', '2014-04-21 19:41:45', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '129', '12344; 1122; ; 12; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:42:10', '2014-04-21 19:42:10', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '130', '12344; 1122; ; 122; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:43:08', '2014-04-21 19:43:08', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '131', '; 1122; ; 121; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:43:34', '2014-04-21 19:43:34', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '132', '12344; 1122; ; 1221; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:50:49', '2014-04-21 19:50:49', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '133', '12344; 11221; ; 1221; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:50:58', '2014-04-21 19:50:58', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '134', '12344; 11221; ; 122; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 19:58:43', '2014-04-21 19:58:43', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '135', '12344; 1122; ; 122; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:05:34', '2014-04-21 20:05:34', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '136', '12344; 11222; ; 122; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:08:46', '2014-04-21 20:08:46', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '137', '12344; 1122; ; 122; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:08:50', '2014-04-21 20:08:50', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '138', '12344; 1122; ; 122; 4; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:08:57', '2014-04-21 20:08:57', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '139', '12344; 1122; ; 122; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:10:45', '2014-04-21 20:10:45', '11' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '140', '; 1122; ; 121; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:11:50', '2014-04-21 20:11:50', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '141', '; 1; ; 121; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:11:53', '2014-04-21 20:11:53', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '142', '; 12; ; 121; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:11:56', '2014-04-21 20:11:56', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '143', '; 123; ; 121; 3; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:11:57', '2014-04-21 20:11:57', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '144', '', 'admin', '1', NULL, NULL, '2014-04-21 20:12:01', '2014-04-21 20:12:01', '21' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '145', '; 123123; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:12:07', '2014-04-21 20:12:07', '21' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '146', '; 123123; ; ---------------; 2; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-21 20:12:12', '2014-04-21 20:12:12', '21' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '147', '', 'admin', '1', NULL, NULL, '2014-04-22 17:47:47', '2014-04-22 17:47:47', '22' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '148', '222; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-22 17:47:49', '2014-04-22 17:47:49', '22' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '149', '', 'admin', '1', NULL, NULL, '2014-04-22 17:51:37', '2014-04-22 17:51:37', '23' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '150', '12344; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-22 17:51:41', '2014-04-22 17:51:41', '23' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '151', '', 'admin', '1', NULL, NULL, '2014-04-22 17:52:13', '2014-04-22 17:52:13', '24' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '152', '222; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-22 17:52:18', '2014-04-22 17:52:18', '24' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '153', '', 'admin', '1', NULL, NULL, '2014-04-22 17:52:26', '2014-04-22 17:52:26', '25' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '154', '222; 123123; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-22 17:52:32', '2014-04-22 17:52:32', '25' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '155', '222; 12312; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-22 17:52:43', '2014-04-22 17:52:43', '25' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '156', '', 'admin', '1', NULL, NULL, '2014-04-22 17:52:45', '2014-04-22 17:52:45', '26' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '157', '12344; ---------------; ; ---------------; 1; 0.0; ', 'admin', '3', NULL, NULL, '2014-04-22 17:52:49', '2014-04-22 17:52:49', '26' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '158', '; 123; ; 121; 3; 12.0; ', 'admin', '3', NULL, NULL, '2014-05-22 16:58:01', '2014-05-22 16:58:01', '13' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '159', '; 123123; ; ---------------; 2; 23.0; ', 'admin', '3', NULL, NULL, '2014-05-22 16:58:05', '2014-05-22 16:58:05', '21' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '160', '', 'admin', '1', NULL, NULL, '2014-05-22 17:08:12', '2014-05-22 17:08:12', '27' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '161', 'Элемент 1; ---------------; ; ---------------; 1; 112.0; ', 'admin', '3', NULL, NULL, '2014-05-22 17:08:20', '2014-05-22 17:08:20', '27' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '162', 'йцу; ---------------; ; ---------------; 1; 123.0; примечание 1', 'admin', '3', NULL, NULL, '2014-05-22 17:44:51', '2014-05-22 17:44:51', '12' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '163', 'Элемент 1; ---------------; ; ---------------; 1; 112.0; примечание 2', 'admin', '3', NULL, NULL, '2014-05-22 17:44:56', '2014-05-22 17:44:56', '27' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '164', 'эл2; ---------------; ; ---------------; 1; 0.0; прим 1', 'admin', '3', NULL, NULL, '2014-05-22 18:08:27', '2014-05-22 18:08:27', '19' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '165', '222; ---------------; ; ---------------; 1; 0.0; прим 55', 'admin', '3', NULL, NULL, '2014-05-22 18:08:31', '2014-05-22 18:08:31', '24' );
INSERT INTO `item_histories`(`id`,`after`,`username`,`event_id`,`bundle_id`,`repair_id`,`created_at`,`updated_at`,`item_id`) VALUES ( '166', '222; 12312; ; ---------------; 1; 0.0; прп арп воарп воарп оварп воарп', 'admin', '3', NULL, NULL, '2014-05-22 18:08:36', '2014-05-22 18:08:36', '25' );
COMMIT;
/*!40000 ALTER TABLE `item_histories` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "items" ------------------------------------
/*!40000 ALTER TABLE `items` DISABLE KEYS */

BEGIN;

INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '1', 'Элемент 1', '1111', '', '', '0', '100002.00', '', '2014-04-03 18:01:12', '2014-04-09 18:03:01', '0', '1', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '2', 'э1', '1', '', '', '0', '100000.00', '', '2014-04-05 13:54:49', '2014-04-13 16:06:15', '0', '2', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '3', 'rere', '', '', '', '1', '0.00', '', '2014-04-09 18:07:26', '2014-04-09 18:07:32', '0', '1', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '4', 'qwqw', '12', '', '234', '2', '0.00', '', '2014-04-09 18:08:23', '2014-04-13 16:06:22', '0', '2', '2', NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '5', 'aawe', '---------------', '', '', '2', '0.00', '', '2014-04-09 18:20:25', '2014-04-15 20:15:57', '0', '1', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '6', 'aawe', '', '', '', '1', '0.00', '', '2014-04-09 18:21:53', '2014-04-09 18:21:58', '0', '1', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '7', 'werwer', '---------------', '', '', '1', '0.00', '', '2014-04-09 18:22:28', '2014-04-09 18:22:35', '0', '1', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '8', '', '---------------', '', '', '1', '0.00', '', '2014-04-09 18:23:36', '2014-04-09 18:23:36', '0', '1', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '9', '', '---------------', '', '', '1', '0.00', '', '2014-04-09 18:24:30', '2014-04-09 18:24:30', '0', '1', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '10', 'dd', '---------------', '', '---------------', '1', '0.00', '', '2014-04-09 18:24:42', '2014-04-09 18:24:50', '0', '1', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '11', '12344', '1122', '', '122', '3', '0.00', '', '2014-04-09 18:25:14', '2014-04-21 20:10:45', '1', '5', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '12', 'йцу', '---------------', '', '---------------', '1', '123.00', 'примечание 1', '2014-04-13 08:02:27', '2014-05-22 17:44:51', '7', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '13', '', '123', '', '121', '3', '12.00', '', '2014-04-13 10:33:26', '2014-05-22 16:58:01', '1', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '14', 'aawe', '---------------', '', '', '2', '0.00', '', '2014-04-15 20:15:57', '2014-04-15 20:15:57', '0', '2', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '15', 'эл1', '12341234', '', '---------------', '1', '1123.00', '', '2014-04-20 07:40:46', '2014-04-21 18:14:15', '8', '6', NULL, '1' );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '16', 'эл2', '21', '', '---------------', '1', '123.00', '', '2014-04-20 07:40:56', '2014-04-21 18:13:24', '8', '6', NULL, '1' );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '17', 'итем 1', '---------------', '', '---------------', '1', '100.00', '', '2014-04-20 16:06:09', '2014-04-20 16:06:16', '4', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '18', '', '---------------', '', '---------------', '1', '0.00', '', '2014-04-21 18:10:54', '2014-04-21 18:10:54', '1', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '19', 'эл2', '---------------', '', '---------------', '1', '0.00', 'прим 1', '2014-04-21 18:12:18', '2014-05-22 18:08:27', '1', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '20', '344', '---------------', '', '---------------', '1', '0.00', '', '2014-04-21 18:12:54', '2014-04-21 18:12:58', '10', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '21', '', '123123', '', '---------------', '2', '23.00', '', '2014-04-21 20:12:01', '2014-05-22 16:58:05', '1', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '22', '222', '---------------', '', '---------------', '1', '0.00', '', '2014-04-22 17:47:47', '2014-04-22 17:47:49', '1', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '23', '12344', '---------------', '', '---------------', '1', '0.00', '', '2014-04-22 17:51:37', '2014-04-22 17:51:41', '1', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '24', '222', '---------------', '', '---------------', '1', '0.00', 'прим 55', '2014-04-22 17:52:13', '2014-05-22 18:08:31', '1', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '25', '222', '12312', '', '---------------', '1', '0.00', 'прп арп воарп воарп оварп воарп', '2014-04-22 17:52:26', '2014-05-22 18:08:36', '1', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '26', '12344', '---------------', '', '---------------', '1', '0.00', '', '2014-04-22 17:52:45', '2014-04-22 17:52:49', '1', '6', NULL, NULL );
INSERT INTO `items`(`id`,`title`,`main_serial`,`additional_serial`,`inventory_number`,`amount`,`cost`,`note`,`created_at`,`updated_at`,`bundle_id`,`location_id`,`repair_id`,`internal_users_id`) VALUES ( '27', 'Элемент 1', '---------------', '', '---------------', '1', '112.00', 'примечание 2', '2014-05-22 17:08:12', '2014-05-22 17:44:56', '7', '6', NULL, NULL );
COMMIT;
/*!40000 ALTER TABLE `items` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "locations" --------------------------------
/*!40000 ALTER TABLE `locations` DISABLE KEYS */

BEGIN;

INSERT INTO `locations`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '1', 'Склад', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `locations`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '2', 'Списание', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `locations`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '3', 'Пользователь', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `locations`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '4', 'Внутреннее использование', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `locations`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '5', 'Ремонт', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
INSERT INTO `locations`(`id`,`name`,`created_at`,`updated_at`) VALUES ( '6', 'Комплект', '2014-04-03 17:59:05', '2014-04-03 17:59:05' );
COMMIT;
/*!40000 ALTER TABLE `locations` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "repairs" ----------------------------------
/*!40000 ALTER TABLE `repairs` DISABLE KEYS */

BEGIN;

INSERT INTO `repairs`(`id`,`place`,`sent`,`receive`,`control_days`,`description`,`created_at`,`updated_at`,`archive`) VALUES ( '1', 'место ремонта 1', '2014-04-06 04:00:00', NULL, '2', '', '2014-04-13 14:37:54', '2014-04-13 14:38:49', '0' );
INSERT INTO `repairs`(`id`,`place`,`sent`,`receive`,`control_days`,`description`,`created_at`,`updated_at`,`archive`) VALUES ( '2', 'Мастерская 1', '2014-04-06 04:00:00', NULL, '10', '', '2014-04-13 14:39:23', '2014-04-13 14:39:23', '0' );
COMMIT;
/*!40000 ALTER TABLE `repairs` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "schema_migrations" ------------------------
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */

BEGIN;

INSERT INTO `schema_migrations`(`version`) VALUES ( '20140113175529' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140130190025' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140201114158' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140214115705' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140214120058' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140215072758' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140215073523' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140222085526' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140302084346' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140311063020' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140322080722' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140326132418' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140326132749' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140327062436' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140327085704' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140330050443' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140330164554' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140330171425' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140401195748' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140402062827' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140416201009' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140420075947' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140420083907' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140426055856' );
INSERT INTO `schema_migrations`(`version`) VALUES ( '20140426055926' );
COMMIT;
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "test_items" -------------------------------
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
/*!40000 ALTER TABLE `users` DISABLE KEYS */

BEGIN;

INSERT INTO `users`(`id`,`email`,`encrypted_password`,`reset_password_token`,`reset_password_sent_at`,`remember_created_at`,`sign_in_count`,`current_sign_in_at`,`last_sign_in_at`,`current_sign_in_ip`,`last_sign_in_ip`,`created_at`,`updated_at`) VALUES ( '1', 'admin', '$2a$10$.z9RYkCjck7gTCPw7Qdq9.VmzqJQ9aquxsYqsrDWCKNzHl/T0TS8a', NULL, NULL, NULL, '15', '2014-05-22 16:15:30', '2014-05-21 16:58:48', '127.0.0.1', '127.0.0.1', '2014-04-03 18:00:36', '2014-05-22 16:15:30' );
COMMIT;
/*!40000 ALTER TABLE `users` ENABLE KEYS */

-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


