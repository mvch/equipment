# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150518191143) do

  create_table "bundle_checks", force: true do |t|
    t.string   "username"
    t.integer  "bundle_id"
    t.integer  "external_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "note"
  end

  add_index "bundle_checks", ["bundle_id"], name: "index_bundle_checks_on_bundle_id", using: :btree
  add_index "bundle_checks", ["external_user_id"], name: "index_bundle_checks_on_external_user_id", using: :btree

  create_table "bundle_histories", force: true do |t|
    t.string   "before"
    t.string   "after"
    t.string   "username"
    t.datetime "actiondate"
    t.integer  "event_id"
    t.integer  "bundle_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "external_users_id"
  end

  add_index "bundle_histories", ["bundle_id"], name: "index_bundle_histories_on_bundle_id", using: :btree
  add_index "bundle_histories", ["event_id"], name: "index_bundle_histories_on_event_id", using: :btree
  add_index "bundle_histories", ["external_users_id"], name: "index_bundle_histories_on_external_users_id", using: :btree

  create_table "bundles", force: true do |t|
    t.string   "title"
    t.string   "note"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "location_id"
    t.integer  "external_users_id"
    t.integer  "internal_users_id"
  end

  add_index "bundles", ["external_users_id"], name: "index_bundles_on_external_users_id", using: :btree
  add_index "bundles", ["internal_users_id"], name: "index_bundles_on_internal_users_id", using: :btree
  add_index "bundles", ["location_id"], name: "index_bundles_on_location_id", using: :btree

  create_table "events", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "external_users", force: true do |t|
    t.boolean  "is_teacher"
    t.string   "surname"
    t.string   "name"
    t.string   "patronymic"
    t.string   "address"
    t.string   "phone"
    t.string   "email"
    t.string   "skype"
    t.string   "vklogin"
    t.string   "password"
    t.boolean  "archive"
    t.datetime "installdate"
    t.datetime "uninstalldate"
    t.integer  "bundle_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "note"
  end

  add_index "external_users", ["bundle_id"], name: "index_external_users_on_bundle_id", using: :btree

  create_table "internal_users", force: true do |t|
    t.string   "room"
    t.string   "place"
    t.string   "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "item_checks", force: true do |t|
    t.string   "username"
    t.integer  "item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "item_checks", ["item_id"], name: "index_item_checks_on_item_id", using: :btree

  create_table "item_histories", force: true do |t|
    t.string   "after"
    t.string   "username"
    t.integer  "event_id"
    t.integer  "bundle_id"
    t.integer  "repair_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "item_id"
  end

  add_index "item_histories", ["bundle_id"], name: "index_item_histories_on_bundle_id", using: :btree
  add_index "item_histories", ["event_id"], name: "index_item_histories_on_event_id", using: :btree
  add_index "item_histories", ["item_id"], name: "index_item_histories_on_item_id", using: :btree
  add_index "item_histories", ["repair_id"], name: "index_item_histories_on_repair_id", using: :btree

  create_table "items", force: true do |t|
    t.string   "title"
    t.string   "main_serial"
    t.string   "additional_serial"
    t.string   "inventory_number"
    t.integer  "amount"
    t.decimal  "cost",              precision: 10, scale: 2
    t.string   "note"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "bundle_id"
    t.integer  "location_id"
    t.integer  "repair_id"
    t.integer  "internal_users_id"
    t.integer  "order_number"
  end

  add_index "items", ["internal_users_id"], name: "index_items_on_internal_users_id", using: :btree
  add_index "items", ["location_id"], name: "index_items_on_location_id", using: :btree
  add_index "items", ["repair_id"], name: "index_items_on_repair_id", using: :btree

  create_table "locations", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "repairs", force: true do |t|
    t.string   "place"
    t.datetime "sent"
    t.datetime "receive"
    t.integer  "control_days"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "archive"
  end

  create_table "temporary_uses", force: true do |t|
    t.datetime "from"
    t.datetime "before"
    t.datetime "receive"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "item_id"
    t.integer  "external_users_id"
    t.boolean  "archive"
  end

  add_index "temporary_uses", ["external_users_id"], name: "index_temporary_uses_on_external_users_id", using: :btree
  add_index "temporary_uses", ["item_id"], name: "index_temporary_uses_on_item_id", using: :btree

  create_table "test_items", force: true do |t|
    t.string   "title"
    t.string   "main_serial"
    t.text     "additional_serial"
    t.text     "inventory_number"
    t.integer  "amount"
    t.decimal  "cost",              precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
