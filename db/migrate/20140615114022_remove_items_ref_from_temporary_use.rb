class RemoveItemsRefFromTemporaryUse < ActiveRecord::Migration
  def change
    remove_reference :temporary_uses, :items, index: true
  end
end
