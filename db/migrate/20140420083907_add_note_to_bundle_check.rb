class AddNoteToBundleCheck < ActiveRecord::Migration
  def change
    add_column :bundle_checks, :note, :string
  end
end
