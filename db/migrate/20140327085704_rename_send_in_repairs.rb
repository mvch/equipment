class RenameSendInRepairs < ActiveRecord::Migration
  def change
  	rename_column :repairs, :send, :sent
  end
end
