class CreateExternalUsers < ActiveRecord::Migration
  def change
    create_table :external_users do |t|
      t.boolean :is_teacher
      t.string :surname
      t.string :name
      t.string :patronymic
      t.string :address
      t.string :phone
      t.string :email
      t.string :skype
      t.string :vklogin
      t.string :password
      t.boolean :archive
      t.datetime :installdate
      t.datetime :uninstalldate
      t.references :bundle, index: true

      t.timestamps
    end
  end
end
