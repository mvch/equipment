class CreateItemChecks < ActiveRecord::Migration
  def change
    create_table :item_checks do |t|
      t.string :username
      t.references :item, index: true

      t.timestamps
    end
  end
end
