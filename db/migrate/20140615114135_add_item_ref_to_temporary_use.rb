class AddItemRefToTemporaryUse < ActiveRecord::Migration
  def change
    add_reference :temporary_uses, :item, index: true
  end
end
