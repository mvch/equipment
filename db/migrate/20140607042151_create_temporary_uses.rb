class CreateTemporaryUses < ActiveRecord::Migration
  def change
    create_table :temporary_uses do |t|
      t.datetime :from
      t.datetime :before
      t.datetime :receive
      t.string :description
      t.references :external_user, index: true
      t.references :items, index: true

      t.timestamps
    end
  end
end
