class AddLocationsToBundle < ActiveRecord::Migration
  def change
    add_reference :bundles, :location, index: true
  end
end
