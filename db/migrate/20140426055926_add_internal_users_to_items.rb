class AddInternalUsersToItems < ActiveRecord::Migration
  def change
    add_reference :items, :internal_users, index: true
  end
end
