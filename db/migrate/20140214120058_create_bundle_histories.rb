class CreateBundleHistories < ActiveRecord::Migration
  def change
    create_table :bundle_histories do |t|
      t.string :before
      t.string :after
      t.string :username
      t.datetime :actiondate
      t.references :event, index: true
      t.references :bundle, index: true

      t.timestamps
    end
  end
end
