class RenameRepairsIdInItems < ActiveRecord::Migration
  def change
  	rename_column :items, :repairs_id, :repair_id
  end
end
