class AddItemsToItemHistories < ActiveRecord::Migration
  def change
    add_reference :item_histories, :item, index: true
  end
end
