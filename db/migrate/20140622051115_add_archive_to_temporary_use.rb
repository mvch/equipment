class AddArchiveToTemporaryUse < ActiveRecord::Migration
  def change
    add_column :temporary_uses, :archive, :boolean
  end
end
