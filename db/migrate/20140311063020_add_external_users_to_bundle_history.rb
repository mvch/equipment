class AddExternalUsersToBundleHistory < ActiveRecord::Migration
  def change
    add_reference :bundle_histories, :external_users, index: true
  end
end
