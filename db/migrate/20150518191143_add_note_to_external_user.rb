class AddNoteToExternalUser < ActiveRecord::Migration
  def change
    add_column :external_users, :note, :string
  end
end
