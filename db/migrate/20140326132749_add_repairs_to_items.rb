class AddRepairsToItems < ActiveRecord::Migration
  def change
    add_reference :items, :repairs, index: true
  end
end
