class CreateBundles < ActiveRecord::Migration
  def change
    create_table :bundles do |t|
      t.string :title
      t.string :note
      t.timestamps
    end
    change_table :items do |t|
    	t.belongs_to :bundle
    end

  end
end
