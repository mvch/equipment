class AddProviderToExternalUsers < ActiveRecord::Migration
  def change
    add_column :external_users, :provider, :string
  end
end
