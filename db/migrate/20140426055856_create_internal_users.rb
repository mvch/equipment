class CreateInternalUsers < ActiveRecord::Migration
  def change
    create_table :internal_users do |t|
      t.string :room
      t.string :place
      t.string :note

      t.timestamps
    end
  end
end
