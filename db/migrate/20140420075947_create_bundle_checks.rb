class CreateBundleChecks < ActiveRecord::Migration
  def change
    create_table :bundle_checks do |t|
      t.string :username
      t.references :bundle, index: true
      t.references :external_user, index: true

      t.timestamps
    end
  end
end
