class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :title
      t.string :main_serial
      t.string :additional_serial
      t.string :inventory_number
      t.integer :amount
      t.decimal :cost, :precision => 10, :scale => 2
      t.string :note

      t.timestamps
    end
  end
end
