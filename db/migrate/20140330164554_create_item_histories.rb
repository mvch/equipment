class CreateItemHistories < ActiveRecord::Migration
  def change
    create_table :item_histories do |t|
      t.string :after
      t.string :username
      t.references :event, index: true
      t.references :bundle, index: true
      t.references :repair, index: true

      t.timestamps
    end
  end
end
