class AddExternalUsersRefToTemporaryUse < ActiveRecord::Migration
  def change
    add_reference :temporary_uses, :external_users, index: true
  end
end
