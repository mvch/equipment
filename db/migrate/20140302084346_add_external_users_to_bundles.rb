class AddExternalUsersToBundles < ActiveRecord::Migration
  def change
    add_reference :bundles, :external_users, index: true
  end
end
