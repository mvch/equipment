class AddInternalUsersToBundles < ActiveRecord::Migration
  def change
    add_reference :bundles, :internal_users, index: true
  end
end
