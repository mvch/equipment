class AddOrderNumberToItems < ActiveRecord::Migration
  def change
    add_column :items, :order_number, :integer
  end
end
