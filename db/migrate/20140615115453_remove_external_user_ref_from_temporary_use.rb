class RemoveExternalUserRefFromTemporaryUse < ActiveRecord::Migration
  def change
    remove_reference :temporary_uses, :external_user, index: true
  end
end
