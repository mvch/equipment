class CreateRepairs < ActiveRecord::Migration
  def change
    create_table :repairs do |t|
      t.string :place
      t.datetime :send
      t.datetime :receive
      t.integer :control_days
      t.string :description

      t.timestamps
    end
  end
end
