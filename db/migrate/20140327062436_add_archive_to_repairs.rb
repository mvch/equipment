class AddArchiveToRepairs < ActiveRecord::Migration
  def change
    add_column :repairs, :archive, :boolean
  end
end
