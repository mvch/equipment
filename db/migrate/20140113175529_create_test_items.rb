class CreateTestItems < ActiveRecord::Migration
  def change
    create_table :test_items do |t|
      t.string :title
      t.string :main_serial
      t.text :additional_serial
      t.text :inventory_number
      t.integer :amount
      t.decimal :cost, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
