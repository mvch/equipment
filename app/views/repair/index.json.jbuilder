json.repair @repair do |b|
	json.id b.id
	json.place b.place
	json.sent b.sent
	json.receive b.receive
	json.control_days b.control_days
	json.archive b.archive
	json.item_id b.getItemId()
	json.item_title b.getItemTitle()
	json.item_serial b.getItemSerial()
end
