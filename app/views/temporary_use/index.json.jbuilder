json.temporaryUse @tempUse do |b|
	json.id b.id
	json.from b.from
	json.before b.before
	json.receive b.receive
	json.description b.description
	json.external_users_id b.external_users_id
	json.item_id b.item_id
	json.item_serial b.item.main_serial
	json.item_title b.item.title
	json.fio b.external_users.fio
	json.is_teacher b.external_users.is_teacher	
end
