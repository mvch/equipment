# json.bundles @bundles do |b|
# 	json.id b.id
# 	json.title b.title
# 	json.note b.note
# 	json.itemid b.items.title

# end

json.bundles @bundles do |b|
	json.id b.id
	json.title b.title
	json.note b.note
	json.location_id b.location_id
	json.location b.location.name
	json.external_users_id b.external_users_id

#	json.items b.items do |i|
#		json.id i.id
#		json.title i.title
#		json.note i.note
#		json.bundle_id i.bundle_id
#	end
end
