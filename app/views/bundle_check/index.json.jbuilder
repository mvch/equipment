json.bundle_check @bundleCheck do |b|
	json.id					b.id
	json.username 			b.username
	json.bundle_id			b.bundle_id
	json.external_user_id	b.external_user_id
	json.bundle_title		b.getBundleTitle()
	json.external_user_fio	b.getExternalUserFIO()
	json.created_at			b.created_at
	json.note				b.note
end

