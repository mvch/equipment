json.bundlehistory @bundleHistory do |b|
	json.id b.id
	json.event b.event.name
	json.before b.before
	json.after b.after
	json.who b.username
	#json.who 'user1'
	json.external_users_id b.external_users_id
	json.when b.created_at
end

