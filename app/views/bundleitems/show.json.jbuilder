json.items @bundleItems do |b|
	json.id b.id
	json.title b.title
	json.main_serial b.main_serial
	json.additional_serial b.additional_serial
	json.inventory_number b.inventory_number
	json.amount b.amount
	json.cost b.cost
	json.note b.note
	json.bundle_id b.bundle_id
	json.location_id b.location_id	
end


