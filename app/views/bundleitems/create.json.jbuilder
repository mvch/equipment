json.items @bundleItems do |b|
	json.id b.id
	json.title b.title
	json.note b.note
	json.bundle_id b.bundle_id
end

