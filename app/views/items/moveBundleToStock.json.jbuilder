json.bundles @bundles do |b|
	json.id b.id
	json.title b.title
	json.note b.note
	json.location_id b.location_id
	json.location b.location.name
	json.external_users_id b.external_users_id
end
