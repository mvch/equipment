json.items @items do |b|
	json.id b.id
	json.title b.title
	json.main_serial b.main_serial
	json.additional_serial b.additional_serial
	json.inventory_number b.inventory_number
	json.amount b.amount
	json.cost b.cost
	json.note b.note
	json.bundle_id b.bundle_id
	json.bundle b.getBundleTitle()
	json.location_id b.location_id
	json.location b.location.name
end
