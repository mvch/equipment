json.externalusers @eu do |b|
	json.id b.id
	json.surname b.surname
	json.patronymic b.patronymic
	json.name b.name
	json.is_teacher b.is_teacher
	json.address b.address
	json.phone b.phone
	json.email b.email
	json.skype b.skype
	json.vklogin b.vklogin
	json.password b.password
	json.archive b.archive
	json.installdate b.installdate#.to_date
	json.uninstalldate b.uninstalldate
	json.bundle_id b.bundle_id
	json.bundle_title b.getBundleTitle()
	json.provider b.provider
	json.note b.note
end
