json.externalusers @eu do |b|
	json.id b.id
	json.fio b.fio
	json.is_teacher b.is_teacher
	json.address b.address
	json.skype b.skype
	json.vklogin b.vklogin
end
