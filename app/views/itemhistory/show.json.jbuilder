json.itemhistory @ih do |b|
	json.id b.id
	json.event b.event.name
	json.after b.after
	json.who b.username
	#json.who 'user1'
	json.when b.created_at
	json.bundle_id b.bundle_id
	json.repair_id b.repair_id
	json.item_id b.item_id
end

