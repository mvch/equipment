json.item_check @itemCheck do |b|
	json.id				b.id
	json.username 		b.username
	json.item_id		b.item_id
	json.item_title		b.getItemTitle()
	json.item_serial	b.getItemSerial()
	json.created_at		b.created_at
end

