/**
 *= require_self
 */

Ext.require(['Ext.data.*', 'Ext.grid.*', 'Ext.ux.grid.FiltersFeature',
  'Ext.ux.form.field.ClearButton' ]);

Ext.Loader.setConfig({
	// enabled:true,
	// disableCaching: false,
	// paths: { 'EQ': '/assets/app' }
});
// Ext.require(["EQ.view.TestItems"]);
// create a new instance of Application class
Ext.application({
  // the global namespace
  name: 'EQ',
  // store: ['Items','NBundles'],
  controllers: ['TestItems', 'BundleHistory', 'Bundles', 'Items', 'NBundles','ItemTitles'
    , 'ExternalUsers', 'Repairs', 'ItemHistories', 'ProvidersList', 'NamesList', 'PatronymicsList'
    , 'RepairPlacesList', 'ItemChecks', 'BundleChecks', 'InternalUsers' , 'TemporaryUses', 'Searches' ],
  appFolder: '/assets/app',


  launch: function () {  
    
  	// Ext.create("EQ.view.TestItems");
    // Ext.create("EQ.view.HistoryGrid");

  // EQ.model.NBundles.load('bundles', {
  //     success: function(record, operation) {
  //       console.log('!');
  //         console.log(record.get('id'));
  //         console.log(record.items().getCount());
  //         // console.log(record.items().get('bundle_id'));
  //         // console.log(record.comments().getCount());
  //     }
  //   });    
  }


});

  //autoCreateViewport: true
// });