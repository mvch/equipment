Ext.define('EQ.view.NBundles', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.nbundles',
    title: 'Комплекты',
    requires:[
        'EQ.view.Items'
        ],
        store: 'NBundles',
        columns: [{
            header: 'Комплект',
            dataIndex: 'title',
            flex:1
        }, {
            header: 'Примечание',
            dataIndex: 'note',
            flex:1
        }, {
            header: 'id',
            dataIndex: 'id',
            flex:1
        }]
        , renderTo: 'bundlesGrid'
        ,
          listeners: {            
            cellclick: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                // console.log('cell_click:'+ record.get('id') + '; ' + record.items().getCount() + '; ');
                // var view = Ext.widget('itemsGrid');
                // view.setTitle(record.get('id'));
                itemsGrid.setTitle('Элементы комплекта: ' + record.get('id'));
                itemsGrid.getStore().loadRawData(record.raw['items']);
                },


        }
});
