Ext.define('EQ.view.HistoryWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.historyWindow',
    requires:[
        'EQ.view.HistoryGrid'
        ],
    title: 'Просмотр истории',
    // header: {
    //     titlePosition: 2,
    //     titleAlign: 'left'
    // },
	//store: ['EQ.store.Bundles'],
    closable: true,
    closeAction: 'destroy',
    autoShow: false,
    floatable: false,
    modal: true,
    width: 800,
    height: 550,
    items: [{
		xtype: 'container',
		itemId: 'hwContainer',
		height: 40,
		margin: '10 10 0 10',
		items : [{
			xtype:'label',	
			componentCls: 'labelHeader',
			text: 'История изменения комплекта:',
			margin: '10 10 0 10'
		}, {
			xtype:'label',	
			componentCls: 'labelHeader strongText',
			text: 'title',			
			itemId: 'bundleTitle'
		}
		]
	},
	{
        xtype: 'historyBundle',
        height: 450

    }]
});