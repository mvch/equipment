Ext.define('EQ.view.ExternalUsers.ShowWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.showEUWindow',
    id: 'showEUWindow',
    title: 'Просмотр пользователя',    
    closable: true,
    closeAction: 'destroy',
    modal: true,
    width: 780,
    height: 380,
    
    initComponent: function() {
        this.items = [{
            xtype: 'form',
            margin: 20,
            id: 'showEUWindowForm',
            layout: 'vbox',
            items: [
            {
            xtype: 'panel',
            border: false,
            layout:'hbox',
            items: [                
            {
                xtype: 'panel',
                width: 330,
                border: false,
                layout:'vbox',
                items: [                
                {
                    xtype: 'textfield',
                    name: 'surname',                    
                    fieldLabel: 'Фамилия',
                    itemId: 'surname'
                },{
                    name: 'name',
                    fieldLabel: 'Имя', 
                    itemId: 'name',
                    xtype:'combo',
                    store: 'NamesList',
                    displayField: 'name',                    
                    valueField: 'name',
                    queryMode: 'local',
                    editable: true
                },{
                    name: 'patronymic',
                    fieldLabel: 'Отчество',
                    itemId: 'patronymic',
                    xtype:'combo',
                    store: 'PatronymicsList',
                    displayField: 'patronymic',                    
                    valueField: 'patronymic',
                    queryMode: 'local',
                    editable: true
                },{
                    xtype: 'checkbox',
                    name: 'is_teacher',
                    inputValue: 'true',
                    uncheckedValue: 'false',
                    fieldLabel: 'Преподаватель' 
                },{
                    xtype: 'checkbox',
                    name: 'archive',
                    inputValue: 'true',
                    uncheckedValue: 'false',
                    fieldLabel: 'Архивный' 
                },{
                    xtype: 'textfield',
                    name: 'bundle_title',
                    readOnly: true,
                    fieldLabel: 'Комплект' 
                },{
                    xtype: 'datefield',//
                    // width: 265,
                    format: 'd.m.Y',
                    submitFormat: 'Y-m-d\\TH:i:s',
                    name: 'installdate',
                    // readOnly: true,
                    fieldLabel: 'Дата передачи' 
                },{
                    name: 'uninstalldate',
                    // width: 265,
                    xtype: 'datefield',
                    format: 'd.m.Y',
                    submitFormat: 'Y-m-d\\TH:i:s',
                    // readOnly: true,
                    fieldLabel: 'Дата возврата' 
                }]
            },{
                xtype: 'panel',                
                border: false,
                layout:'vbox',
                items: [
                {
                    name: 'provider',
                    width: 400,
                    fieldLabel: 'Провайдер',
                    xtype:'combo',
                    store: 'ProvidersList',
                    displayField: 'provider',                    
                    valueField: 'provider',
                    queryMode: 'local',
                    editable: true
                },{
                    xtype: 'textarea',
                    name: 'address',
                    width: 400,
                    height: 53,
                    fieldLabel: 'Адрес' 
                },{
                    xtype: 'textfield',
                    name: 'phone',
                    width: 400,
                    fieldLabel: 'Телефон' 
                },{
                    xtype: 'textfield',
                    name: 'email',
                    width: 400,
                    fieldLabel: 'E-mail' 
                },{
                    xtype: 'textfield',
                    name: 'skype',
                    width: 400,
                    fieldLabel: 'Skype' 
                },{
                    xtype: 'textfield',
                    name: 'vklogin',
                    width: 400,
                    fieldLabel: 'Логин ВК' 
                },{
                    xtype: 'textfield',
                    name: 'password',
                    width: 400,
                    fieldLabel: 'Пароль' 
                }]
            }
            ]},
            {
                xtype: 'panel',                
                border: false,
                layout: 'vbox',
                // width: 430,
                items: [
                    {
                        xtype: 'textfield',
                        name: 'note',
                        width: 730,
                        fieldLabel: 'Примечание' 
                    }
                ]
        }]
        }];
        this.buttons = [{
            text: 'Сохранить',
            action: 'save',
            id: 'saveButton'
        },{
            text: 'Отмена',
            scope: this,
            handler: this.close
        }];

        this.callParent(arguments);
    },

    listeners: {
        // beforerender: function ( ) {
        //     // console.log(1);
        //     // canEditrowEditingItems = false;
        //     // var bbar = Ext.getCmp('itemsGridBbar');
        //     // bbar.hide();
        //     // grid.getBottomToolbar().hide();
        // }
    }
});
