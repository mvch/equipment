// var filters = {
//         ftype: 'filters',
//         encode: true,
//         local: true
//     };

Ext.define('EQ.view.ExternalUsers.SelectWindowGrid', {
    extend: 'Ext.grid.Panel',
    // extend: 'Ext.ux.grid.Search',    
    alias : 'widget.selectWindowGrid',  
    id: 'selectWindowGrid',  
    // title: 'Все пользователи',
    store: 'ExternalUsersSelect',    

    // features: [
    //   filters,
    //   {
    //     ftype: 'summary', 
    //     dock: 'bottom'
    //   }
    // ],
    // viewConfig : 
    // {
    // enableTextSelection: true

    // },
	// height: 650,
    columnLines: true,
    columns: [{
        header: 'ФИО',
        dataIndex: 'fio',
        filter: {
            type: 'string'
        },
        flex:1        
    },{
        header: 'Препод.',
        dataIndex: 'is_teacher',
        xtype: 'checkcolumn',
        editor: 'checkbox',
        hidden: false,
        itemId: 'columnIsTeacher',
        width: 70// flex:1
    },{
        header: 'Адрес',
        dataIndex: 'address',
        flex:1
    },{
        header: 'Skype',
        dataIndex: 'skype',
        width: 100// flex:1
    },{
        header: 'Логин ВК',
        dataIndex: 'vklogin',
        width: 100// flex:1
    }
    // , {
    //     header: 'id',
    //     dataIndex: 'id',
    //     width: 100// flex:1
    // }
    ],
    bbar: [{
        iconCls: 'icon-add',
        tooltip: 'Добавить',
        action: 'add'
        // handler : function() {
        //     rowEditingEU.cancelEdit();
        //     var isTeacher = false;
        //     var euType = Ext.ComponentQuery.query('externalUsersContainer')[0].euType;
        //     if (euType == 'teachers')
        //         isTeacher = true;
        //     var r = Ext.create('EQ.model.ExternalUser', {
        //         surname: '',
        //         name: '',
        //         patronymic: '',
        //         is_teacher: isTeacher
        //     });
        //     var store = Ext.data.StoreManager.get("ExternalUsers");
        //     store.add(r);

            // store.load({
            //     params: {type : euType},
            //     callback: function() {                    
            //         var newRecord = store.last();
            //         var sm = Ext.ComponentQuery.query('externalUsersGrid')[0].getSelectionModel();
            //         sm.select(newRecord,true);
            //         canEdit = true;
            //         rowEditingEU.startEdit(newRecord,0);    
            //         canEdit = false;
            //     }
            // });                

        // }
    }, {
        iconCls: 'icon-edit',
        action: 'edit',
        tooltip: 'Редактировать'
        // ,
        // handler: function() {
        //     rowEditingEU.cancelEdit();
        //     canEdit = true;
        //     var selectedRecord = Ext.ComponentQuery.query('externalUsersGrid')[0].getSelectionModel().getSelection()[0];                        
        //     rowEditingEU.startEdit(selectedRecord, 0);
        //     canEdit = false;
        // }
    },{
        iconCls: 'icon-delete',
        tooltip: 'Удалить',      
        action: 'delete',        
        disabled: false
    }
    ]
});