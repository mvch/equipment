Ext.define('EQ.view.ExternalUsers.Main', {
    extend: 'Ext.container.Container',
    alias : 'widget.externalUsersContainer',
    id: 'externalUsersContainer',
    requires:[
        'EQ.view.ExternalUsers.Grid',
        'EQ.view.ExternalUsers.SetBundleWindow'        
        ],    
    constructor: function(euType) {
        this.height = $(window).height() - 115;
        if (euType) { // 'all' or 'teachers' or 'students'
            this.euType = euType;            
        };
        this.callParent(arguments);
        //return this;
    },
    flex:1,
    padding: 0,    
    // plugins: ,
    // layout:'column',
    items: [
        {
            xtype: 'textfield',
            id: 'externalUsersSearchField',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            tooltip: 'tooltip2',
            width: 300,
            enableKeyEvents: true,
            anchor : '95%',            
            inputAttrTpl: " data-qtip='Поиск по Ф. И. О., адресу и комплекту' ",
            listeners : {   
                change: {
                    fn: function (txt, newValue, oldValue) {                        
                        var grid = Ext.getCmp('externalUsersGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            if ( txt.value.indexOf('#') == 0 ) // поиск по идентификатору
                            {
                                var matcher = new RegExp( '^' + Ext.String.escapeRegex(txt.value.substring(1)) + '$', "i");
                                grid.store.filter({
                                    filterFn: function(record) {
                                        return matcher.test(record.get('id'));
                                    }
                                });
                                grid.getView().select(0);
                            }
                            else // поиск по тексту
                            {
                                var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                                grid.store.filter({
                                    filterFn: function(record) {
                                        return matcher.test(record.get('surname')) ||
                                            matcher.test(record.get('name')) ||
                                            matcher.test(record.get('patronymic')) ||
                                            matcher.test(record.get('address')) ||
                                            matcher.test(record.get('bundle_title'));
                                    }
                                });
                            }
                        };                        
                    },
                    buffer: 1000
                }
            }
        },
        {
            xtype: 'externalUsersGrid',
            listeners: {
                beforerender: function(grid) {
                    var store = grid.store;
                    var euType = Ext.ComponentQuery.query('externalUsersContainer')[0].euType;
                    switch (euType) {
                        case 'all':                             
                            grid.headerCt.child('#columnIsTeacher').show();
                            grid.headerCt.child('#columnArchive').show();
                            grid.title = 'Все пользователи';
                            break;
                        case 'teachers':
                            grid.title = 'Преподаватели';
                            break;
                        case 'students':
                            grid.title = 'Ученики';
                            break;
                    }
                    store.load(
                    {
                        params: {type : euType},
                    });
                }
            }
            
        }, {
            xtype: 'container',
            margin: '10 0 0 0',

            items:  [{
                xtype: 'button',
                id: 'setBundleToEUButton',
                scale: 'medium',                
                text: 'Передать комплект',
                handler: function() {
                    if (Ext.getCmp('externalUsersGrid').getSelectionModel().hasSelection())
                    {
                        var userFIO = Ext.getCmp('externalUsersGrid').getSelectionModel().getSelection()[0].data;
                        var win = Ext.widget('setBundleWindow');
                        var bundleTitle = win.getComponent('selectBundleContainer').getComponent('userFIO');
                        bundleTitle.text = userFIO.surname + ' ' + userFIO.name + ' ' + userFIO.patronymic;
                        win.show();
                    } else {
                        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите кому нужно передать комплект');
                    }
                }
            },{
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                text: 'Вернуть комплект',
                id: 'receiveBundleFromEUButton',
                disabled: true
                // handler: function() {
                    // Ext.MessageBox.confirm('Подтверждение', 'Вернуть комплект?', showResult);
                    // function showResult(btn){
                    //     if (btn == 'yes')
                    //     {
                    //         var currentUserId = Ext.getCmp('externalUsersGrid').getSelectionModel().getSelection()[0].data.id;
                    //         var store = Ext.getCmp('externalUsersGrid').getStore();
                    //         var currentUser = store.getById(currentUserId);
                    //         // удаляем у комплекта ссылку на  пользователя
                    //         var bundleStore = Ext.data.StoreManager.get("Bundles");
                    //         bundleStore.load();
                    //         // console.log(currentUser.data.bundle_id);
                    //         var bundleRecord = bundleStore.getById(currentUser.data.bundle_id);

                    //         bundleRecord.set({ external_users_id: null, location_id: 1 });

                    //         // обновляем у пользователя ссылку на комплект и дату возврата комплекта
                    //         var todayDate = today.getDate();
                    //         currentUser.set({ bundle_id: null, bundle_title: null, uninstalldate: todayDate });
                    //         // запрос на печать отчёта
                    //         Ext.MessageBox.confirm('Печать', 'Распечатать акт возврата?', showPrintResult);
                    //         function showResult(btn){
                    //             if (btn == 'yes')
                    //             {

                    //             }
                    //         }
                    //     }
                    // }
                // }
            },{
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                text: 'Проверка оборудования',
                id: 'bundleCheckButton',
                disabled: true
            },{
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                text: 'Печать',
                id: 'printMenuButton',
                disabled: true,
                arrowAlign: 'right',
                menu: { 
                    items: [
                        { text: 'Печать договора передачи',
                          itemId: 'prnContractSend'
                        },
                        { text: 'Печать акта возврата',
                          itemId: 'prnContractReceive'
                        }
                    
                ]}
            }]
        }],
    listeners : {
        // afterrender: function() {
        afterlayout: function() {
            function getUrlVar(key){
                var result = new RegExp(key + "=([^&]*)", "i").exec(decodeURIComponent(window.location.search)); 
                return result && unescape(result[1]) || ""; 
            }
            // console.log(getUrlVar('b'));
            var searchField = Ext.getCmp('externalUsersSearchField');
            // для поиска по идентификаторам добавляем символ #
            if (getUrlVar('id') != "")
                searchField.setValue('#' + getUrlVar('id'));
        },
        beforerender: function ( container, eOpts ) {
            var grid = Ext.getCmp('externalUsersGrid');
            grid.height = this.height - 70;
            // var store = Ext.data.StoreManager.get("ItemChecks");
            // store.load();
        }
    },
    renderTo: 'externalUsers'
});

// Ext.tip.QuickTipManager.register({
//     target: 'externalUsersSearchField-inputEl',
//     text: 'This tooltip was added in code'
// });