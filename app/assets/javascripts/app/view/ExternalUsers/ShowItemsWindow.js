Ext.define('EQ.view.ExternalUsers.ShowItemsWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.showItemsWindow',
    requires:[
    	'EQ.view.Items'
        ],
    title: 'Состав комплекта',
    header: {
        titlePosition: 2,
        titleAlign: 'left'
    },
    closable: true,
    closeAction: 'destroy',
    autoShow: false,
    floatable: false,
    modal: true,
    width: 900,
    height: 460,
    items: [{
        xtype: 'container',
        itemId: 'showItemsContainer',
        height: 40,
        margin: '10 10 0 10',
        items : [{
            xtype:'label',  
            componentCls: 'labelHeader',
            text: 'Информация о комплекте:',            
            margin: '0 10 0 0'
        }, {
            xtype:'label',  
            componentCls: 'labelHeader strongText',
            text: ' ',
            itemId: 'bundleTitle'
        }
        ]
    }
    ,{
        xtype: 'itemsGrid',
        margin: '0 10 10 10',
        height: 350
    }
    ],
    listeners: {
        beforerender: function ( ) {
            canEditrowEditingItems = false;
            var bbar = Ext.getCmp('itemsGridBbar');
            bbar.hide();
            // grid.getBottomToolbar().hide();
        }
    }
});
