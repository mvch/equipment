Ext.define('EQ.view.ExternalUsers.ReceiveBundleWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.ReceiveBundleWindow',
    title: 'Подтверждение',
    // header: {
    //     titlePosition: 2,
    //     titleAlign: 'left'
    // },
	//store: ['EQ.store.Bundles'],
    closable: true,
    closeAction: 'destroy',
    autoShow: false,
    floatable: false,
    modal: true,
    width: 400,
    height: 250,
    items: [{
		xtype:'label',	
		componentCls: 'labelHeader',
		text: 'Вернуть комплект:',
		margin: '20 20 10 10'
	}, {
		xtype:'label',	
		componentCls: 'labelHeader strongText',
		text: 'title',			
		itemId: 'bundleTitle'		
	}, {
        xtype:'label',  
        componentCls: 'labelHeader strongText',
        text: 'title',          
        itemId: 'bundleTitle'       
    }, {
        xtype: 'container',
        // itemId: 'hwContainer',
        // height: 40,
        margin: '10 10 10 10',
        items : [{
            xtype:'button',
            scale: 'medium',                
            text: 'Да',
            margin: '10 10 0 10'
        }, {
            xtype:'button',
            scale: 'medium',                
            text: 'Отмена',
            margin: '10 10 0 10'
        }]
    }]
});