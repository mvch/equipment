
var canEdit = false;
var rowEditingEU = Ext.create('Ext.grid.plugin.RowEditing', {
        clicksToEdit: 2,
        clicksToMoveEditor: 2,
        listeners: {
            beforeedit: function( editor, context, eOpts ) {
                context.colIdx = 0;
                return canEdit;
            }
        }

    });
var filters = {
        ftype: 'filters',
        encode: true,
        local: true
    };

Ext.define('EQ.view.ExternalUsers.Grid', {
    extend: 'Ext.grid.Panel',
    // extend: 'Ext.ux.grid.Search',    
    alias : 'widget.externalUsersGrid',  
    id: 'externalUsersGrid',  
    title: 'Все пользователи',
    store: 'ExternalUsers',
    requires: [ 'EQ.view.ExternalUsers.ShowItemsWindow' ],
    selType : 'cellmodel',
    sortOnLoad: 'true',

    features: [
      filters,
      {
        ftype: 'summary', 
        dock: 'bottom'
      }
    ],
    // viewConfig : 
    // {
    // enableTextSelection: true

    // },
	height: 650,
    columnLines: true,
    columns: [{
        header: 'Фамилия',
        dataIndex: 'surname',
        editor: 'textfield',
        // locked: true,
        sortable: true,
        filter: {
            type: 'string'
        },
        width: 100// flex:1
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                return ('<b>Всего: ' + value + '</b>');
        }
    }, {
        header: 'Имя',
        dataIndex: 'name',
        // locked   : true,
        sortable: true,
        editor: 'textfield',
        width: 100,// flex:1        
        editor: {
            xtype:'combo',
            store: 'NamesList',
            displayField: 'name',                    
            valueField: 'name',
            queryMode: 'local',
            editable: true
        }
    },{
        header: 'Отчество',
        dataIndex: 'patronymic',
        editor: 'textfield',
        // locked   : true,
        sortable: true,
        width: 100,// flex:1
        editor: {
            xtype:'combo',
            store: 'PatronymicsList',
            displayField: 'patronymic',                    
            valueField: 'patronymic',
            queryMode: 'local',
            editable: true
        }
    },{
        header: 'Преподаватель',
        dataIndex: 'is_teacher',
        sortable: true,
        xtype: 'checkcolumn',
        editor: 'checkbox',
        hidden: true,
        itemId: 'columnIsTeacher',
        id: 'columnIsTeacher',
        width: 120// flex:1
    },{
        header: 'Комплект',
        sortable: true,
        dataIndex: 'bundle_title',
        width: 120// flex:1
        // , summaryType: 'count'
        , summaryRenderer: function (value, summaryData, dataIndex) {
            return ('<b>Передано: ' + value + '</b>');
        }
        ,summaryType: function (records) {
                var total = records.reduce(function (sums, record) {
                    return Number(sums) + Number( (record.data.bundle_title != '') ? 1 : 0 );
                }, [0]);

                return total;
        } 
    },{
        header: 'Провайдер',
        dataIndex: 'provider',
        sortable: true,        
        editor: 'textfield',
        width: 100,// flex:1
        editor: {
            xtype:'combo',
            store: 'ProvidersList',
            displayField: 'provider',                    
            valueField: 'provider',
            queryMode: 'local',
            editable: true
        }
    },{
        header: 'Адрес',
        dataIndex: 'address',
        sortable: true,
        editor: 'textfield',
        width: 200// flex:1
    },{
        header: 'Телефоны',
        dataIndex: 'phone',
        sortable: true,
        editor: 'textfield',
        width: 100// flex:1
    },{
        header: 'E-mail',
        dataIndex: 'email',
        editor: 'textfield',
        width: 100// flex:1
    },{
        header: 'Skype',
        dataIndex: 'skype',
        sortable: true,
        editor: 'textfield',
        width: 100// flex:1
    },{
        header: 'Логин ВК',
        dataIndex: 'vklogin',
        editor: 'textfield',
        sortable: true,
        width: 100// flex:1
    },{
        header: 'Пароль',
        dataIndex: 'password',
        editor: 'textfield',
        width: 100// flex:1
    },{
        header: 'Архивный',
        dataIndex: 'archive',
        xtype: 'checkcolumn',
        editor: 'checkbox',
        sortable: true,
        hidden: true,
        itemId: 'columnArchive',
        id: 'columnArchive',
        width: 100// flex:1
    },{
        header: 'Дата установки',
        dataIndex: 'installdate',
        sortable: true,
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        editor: {
                xtype: 'datefield',
                allowBlank: true,
                format: 'd.m.Y'                
            },  
        width: 140
    },{
        header: 'Дата возвращения',
        dataIndex: 'uninstalldate',
        sortable: true,
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        editor: {
                xtype: 'datefield',
                allowBlank: true,
                format: 'd.m.Y'
            },  
        width: 140
    },{
        header: 'Примечание',
        dataIndex: 'note',
        sortable: true,
        editor: 'textfield',
        width: 200// flex:1
    }
    // , {
    //     header: 'id',
    //     dataIndex: 'id',
    //     width: 100// flex:1
    // }, {
    //     header: 'bundle_id',
    //     dataIndex: 'bundle_id',
    //     width: 100// flex:1
    // }
    ],
    bbar: {
      id: 'EUGridBbar',
      items: [{
        // text: '+',
        iconCls: 'icon-add',
        itemId: 'addButton',
        tooltip: 'Добавить',
        handler : function() {
            rowEditingEU.cancelEdit();
            var isTeacher = false;
            var euType = Ext.ComponentQuery.query('externalUsersContainer')[0].euType;
            if (euType == 'teachers')
                isTeacher = true;
            var r = Ext.create('EQ.model.ExternalUser', {
                surname: '',
                name: '',
                patronymic: '',
                is_teacher: isTeacher
            });
            var store = Ext.data.StoreManager.get("ExternalUsers");
            store.add(r);

            store.load({
                params: {type : euType},
                callback: function() {                    
                    var newRecord = store.last();
                    var sm = Ext.ComponentQuery.query('externalUsersGrid')[0].getSelectionModel();
                    sm.select(newRecord,true);
                    canEdit = true;
                    rowEditingEU.startEdit(newRecord,0);
                    rowEditingEU.startEdit(newRecord,0);
                    canEdit = false;
                }
            });                

        }
    }, {
        // text: '~',
        iconCls: 'icon-edit',
        itemId: 'editButton',
        tooltip: 'Редактировать',
        disabled: true,
        handler: function() {
            rowEditingEU.cancelEdit();
            canEdit = true;
            var selectedRecord = Ext.ComponentQuery.query('externalUsersGrid')[0].getSelectionModel().getSelection()[0];                        
            rowEditingEU.startEdit(selectedRecord, 0);
            rowEditingEU.startEdit(selectedRecord, 0);
            canEdit = false;
        }
    },{
        // text: '-',
        iconCls: 'icon-delete',
        itemId: 'deleteButton',
        tooltip: 'Удалить',
        disabled: true,
        handler: function() {
            rowEditingEU.cancelEdit();
            Ext.MessageBox.confirm('Подтверждение', 'Удалить?', showResult);
            function showResult(btn){
                if (btn == 'yes')
                {
                    var selectedRecord = Ext.ComponentQuery.query('externalUsersGrid')[0].getSelectionModel().getSelection();
                    var store = Ext.ComponentQuery.query('externalUsersGrid')[0].getStore();
                    store.remove(selectedRecord);
                }
            }
        }   
    }, {
        text: 'Копировать',
        itemId: 'copyButton',
        disabled: true,
        tooltip: 'Копировать логины и пароль в буфер обмена, при выделенном поле Провайдер копируется Ф.И.О., адрес и телефон',
        cls: 'copyButton'
    }, {
        text: 'Состав комплекта',
        itemId: 'showBundleButton',
        disabled: true,
        tooltip: 'Просмотр состава комплекта у выбранного пользователя',
        handler: function() {
            if (Ext.getCmp('externalUsersGrid').getSelectionModel().hasSelection())
                {
                    var user = Ext.getCmp('externalUsersGrid').getSelectionModel().getSelection()[0].data;
                    if ( user.bundle_id != null && user.bundle_id != 0 )
                    {
                        var win = Ext.widget('showItemsWindow');
                        var bundleTitle = win.getComponent('showItemsContainer').getComponent('bundleTitle');
                        bundleTitle.text = user.bundle_title; //+ ' ' + user.bundle_id;

                        var bStore = Ext.create('EQ.store.BundleItems');
                            bStore.proxy.url = '/bundleitems/' + user.bundle_id;
                            bStore.load(function() {
                                    var newData = bStore.getProxy().getReader().rawData;
                                    Ext.ComponentQuery.query('itemsGrid')[0].getStore().loadRawData(newData);
                                });
                        win.show();
                    } else {
                        Ext.MessageBox.alert('Информация', 'У пользователя нет комплекта');
                    }
                } else {
                    Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите пользователя для просмотра комплекта');
                }
        }
    }, {
        text: 'Перейти к комплекту',
        itemId: 'goToBundleButton',
        disabled: true,
        handler: function () {
            var user = Ext.getCmp('externalUsersGrid').getSelectionModel().getSelection()[0].data;
            window.location = "/bundles/index.html?b=" + user.bundle_title; 
        }
    },
    { xtype: 'tbfill' },
    {
        iconCls: 'icon-xls',
        itemId: 'exportUsersButton',
        tooltip: 'Экспортировать пользователей в Excel',
        handler: function() {
            window.open('/reports/exportusers')
        }
    },
    {
        xtype: 'checkbox',
        fieldLabel: 'Архивные',
        labelWidth: 70,
        handler: function() {
            var euType = Ext.ComponentQuery.query('externalUsersContainer')[0].euType;
            store = this.up().up().store;
            store.load(
            {
                params: {type : euType, archive : (this.getValue() ? 'true' : 'false') },
            })
        }
    }
    ]
  },
    // renderTo: 'externalUsersGrid',
    
    // initComponent: function() {
        

    //      this.callParent(arguments);
    // }, 
    plugins: [rowEditingEU
    // ,{
    //     ptype: 'bufferedrenderer',
    //         trailingBufferZone: 20, // Keep 20 rows rendered in the table behind scroll
    //         leadingBufferZone: 50 // Keep 50 rows rendered in the table ahead of scroll
    // }
    ]
});