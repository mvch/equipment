Ext.define('EQ.view.ExternalUsers.SelectWindowContainer', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.selectWindowContainer',
    id: 'selectWindowContainer',
    requires:[
        'EQ.view.ExternalUsers.SelectWindowGrid'
        ],
    flex:1,
    // height: 680,
    padding: 0,
    layout: 'border',
    items: [{
        xtype: 'panel',
        height: 30,
        border: false,
        region: 'north',
        items: [{
            xtype: 'textfield',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            width: 700,
            enableKeyEvents: true,
            anchor : '95%',
            listeners : {
                change: {
                    fn: function (txt, newValue, oldValue) {
                        var grid = Ext.getCmp('selectWindowGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                            grid.store.filter({
                                filterFn: function(record) {
                                    return matcher.test(record.get('fio'))  ||
                                        matcher.test(record.get('address')) ||
                                        matcher.test(record.get('skype')) ||
                                        matcher.test(record.get('vklogin'));
                                }
                            });
                        };
                    },
                    buffer: 1000
                }
            }

        }]
    },
    {
        xtype: 'panel',
        region: 'center',
        layout: 'column',
        margin: 0,
        border: false,
        items: [
        {
            xtype: 'selectWindowGrid',
            border: true,
            width: 700,
            height: 460
        }],

    }
    ]
});
