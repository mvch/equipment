Ext.define('EQ.view.HistoryGrid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.historyBundle',
    id: 'historyBundle',
        // title: 'Результаты поиска',
// layout: {
    // type: 'vbox',
    // align: 'left'
// },
        // height: 400,
        // width: '100%',
    store: 'BundleHistory',
    columns: [{
        header: 'Событие',
        dataIndex: 'event',
        width:100
    }, 
	// {
        // header: 'До события',
        // dataIndex: 'before',
        // flex:1
    // }, 
	{
        header: 'После события',
        dataIndex: 'after',
        flex:1
    }, {
        header: 'Пользователь',
        dataIndex: 'who',
        width:110
    }, {
        header: 'Дата',
        dataIndex: 'when',
        width:150,
        renderer : Ext.util.Format.dateRenderer('d.m.Y H:i:s')
    }],
    bbar: [{
        iconCls: 'icon-history',
        text: 'Информация о пользователе',
        action: 'showExternalUserInfo'
        // handler: function() {
        //     var selectedRecord = Ext.ComponentQuery.query('historyBundle')[0].getSelectionModel().getSelection()[0].data;            
        //     if ( selectedRecord.external_users_id != null && selectedRecord.external_users_id != 0 )
        //     {
        //         var storeEU = Ext.create('EQ.store.ExternalUsers');
        //         storeEU.proxy.url = '/externalusers/' + selectedRecord.external_users_id;
        //         storeEU.load(
        //             {
        //                 scope: this,
        //                 callback: function(records, operation, success) {
        //                     var win = Ext.widget('showEUWindow');
        //                     win.down('form').loadRecord(storeEU.getAt(0));

        //                     Ext.getCmp('saveButton').hide()
        //                     var form = win.down('form').getForm();
        //                     fields = form.getFields();
        //                     Ext.each(fields.items, function (f) {
        //                         f.readOnly = true;
        //                     });
        //                     win.show();
        //                 }
        //             });
        //     } else {
        //         Ext.MessageBox.alert('Информация', 'C этой записью не связан пользователь');
        //     }
        // }
    }]
        // , renderTo: 'historygrid'
});
