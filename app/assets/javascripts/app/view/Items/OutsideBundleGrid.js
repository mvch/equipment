var canEditrowEditingItemsOutsideBundle = true;
var rowEditingItemsOutsideBundle = Ext.create('Ext.grid.plugin.RowEditing', {
        clicksToMoveEditor: 2,
        autoCancel: false,
        listeners: {
            beforeedit: function() {
                // console.log('canEditrowEditingItemsOutsideBundle: ' + canEditrowEditingItemsOutsideBundle);
                return canEditrowEditingItemsOutsideBundle;
            }
        }
    });

Ext.define('EQ.view.Items.OutsideBundleGrid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.outsideBundleGrid',    
    id: 'outsideBundleGrid',
    title: 'Элементы вне комплектов',
    store: 'OutsideBundleItems',
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
	height: 600,
    columns: [{
        header: 'Наименование',
        dataIndex: 'title',
        editor: 'textfield',
        flex:1,
        editor: {
            xtype:'combo',
            store: 'ItemTitles',
            displayField: 'title',                    
            valueField: 'title',
            queryMode: 'local',
            editable: true
        },
        renderer: function (value, meta) {
           meta.attr = 'style="white-space:normal"';
           return value;
           }  
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                return ('<b>Всего: ' + value + '</b>');
        }
    }, {
        header: 'Серийный<br/>номер',
        dataIndex: 'main_serial',
        editor: 'textfield',
        flex:1
    },
    // {
    //     header: 'Дополнительный<br/> серийный номер',
    //     dataIndex: 'additional_serial',
    //     editor: 'textfield',
    //     flex:1
    // },
    {
        header: 'Инвентарный<br/>номер',
        dataIndex: 'inventory_number',
        editor: 'textfield',
        flex:1
    },{
        header: 'Кол-во',
        dataIndex: 'amount',
        align: 'right',
        editor: {
                xtype: 'numberfield',
                allowBlank: true,
                // minValue: 1
            },
        width: 70
    },{
        header: 'Стоимость',
        dataIndex: 'cost',
        align: 'right',
		// renderer: Ext.util.Format.Money, //!!!!
        renderer: function(value) {
             var result = Ext.util.Format.number(value, '0,000.00');
            return result.replace('.',' ');
        },
        // format: '0.00/i',
        editor: {
                xtype: 'numberfield',
                allowBlank: true,
				decimalSeparator: ',',
                minValue: 0
            }
        ,summaryType: 'sum'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
            var result = Ext.util.Format.number(value, '0,000.00');            
            return ('<b>' + result.replace('.',' ') + '</b>');
        },
        width: 90
    },{
        header: 'Приме-<br/>чание',
        dataIndex: 'note',
        editor: 'textfield',
        width: 80
    }
    , {
        header: 'Размещение',
        dataIndex: 'location',
        width: 100
    }, {
        header: 'Комплект',
        dataIndex: 'bundle',
        itemId: 'bundleColumn',
        hidden: true,
        width: 100
    }
    // , {
    //     header: 'id',
    //     dataIndex: 'location_id',
    //     flex:1
    // }
    ],
    bbar: {
        id: 'outsideBundleItemsGridBbar',
        items: [{
            iconCls: 'icon-add',
            tooltip: 'Добавить',
            // iconCls: 'employee-add',
            handler : function() {
               rowEditingItemsOutsideBundle.cancelEdit();
                // Create a model instance
                var newRecord = Ext.create('EQ.model.Item', {                
                    title: '',
                    note: '',
                    amount: 1,
                    bundle_id: 0,
                    location_id: 1
                });
                var store = Ext.data.StoreManager.get("OutsideBundleItems");
                store.add(newRecord);
                store.load(function() {
                    var lastRecord = store.last();
                    var sm = Ext.getCmp('outsideBundleGrid').getSelectionModel();
                    sm.select(lastRecord,true);
                    rowEditingItemsOutsideBundle.startEdit(lastRecord,0);    
                });                
            }
        }, {
            itemId: 'deleteButton',
            iconCls: 'icon-delete',
            tooltip: 'Удалить',
            disabled: true,
            handler: function() {            
                rowEditingItemsOutsideBundle.cancelEdit();
                Ext.MessageBox.confirm('Подтверждение', 'Удалить?', showResult);
                function showResult(btn){
                    if (btn == 'yes')
                    {
                        var selectedRecord = Ext.getCmp('outsideBundleGrid').getSelectionModel().getSelection();
                        var store = Ext.data.StoreManager.get("OutsideBundleItems");
                        store.remove(selectedRecord);
                    }
                }
                // store.sync();
                // if (store.getCount() > 0) {
                //     sm.select(0);
                // }
            }
        }, {
            iconCls: 'icon-history',
            tooltip: 'История',
            disabled: true,
            itemId: 'historyButton',
            action: 'outsideBundleItemHistory'
        }
    // ,{
    //             xtype: 'pagingtoolbar',
    //             pageSize: 6,
    //             store: 'Bundles',
    //             displayInfo: true
    //             // plugins: new Ext.ux.SlidingPager()
    //         }
    ]},
    // renderTo: 'itemsGrid',
    listeners: {            
        selectionchange: function ( selectionModel, selected, eOpts ) {
            this.getComponent('outsideBundleItemsGridBbar').getComponent('deleteButton').setDisabled( !(selectionModel.hasSelection()) );
            this.getComponent('outsideBundleItemsGridBbar').getComponent('historyButton').setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'writeOffFromOutsideButton' ).setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'moveOnRepairFromOutsideButton' ).setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'moveToBundleFromOutsideButton' ).setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'selectIUtoSetItemFromOutsideBundleButton' ).setDisabled( !(selectionModel.hasSelection()) );
            // если на складе
            Ext.getCmp( 'selectUserForTemporaryUseFromOutsideBundleButton' ).setDisabled( !( selectionModel.hasSelection() && ( selected[0].data.location_id == 1 || selected[0].data.location_id == 6 ) ) );
        }

    },
    initComponent: function() {
         this.callParent(arguments);
    }, 
    plugins: [rowEditingItemsOutsideBundle
        , {
            ptype: 'bufferedrenderer',
            trailingBufferZone: 20, // Keep 20 rows rendered in the table behind scroll
            leadingBufferZone: 50 // Keep 50 rows rendered in the table ahead of scroll
        }]
});
