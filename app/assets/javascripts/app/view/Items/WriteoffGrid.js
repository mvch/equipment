Ext.define('EQ.view.Items.WriteoffGrid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.writeoffGrid',    
    id: 'writeoffGrid',
    title: 'Списанные элементы',
    store: 'OutsideBundleItems',
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
	height: 600,
    columns: [{
        header: 'Наименование',
        dataIndex: 'title',
        flex:1,
        renderer: function (value, meta) {
           meta.attr = 'style="white-space:normal"';
           return value;
           }  
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                return ('<b>Всего: ' + value + '</b>');
        }
    }, {
        header: 'Серийный<br/>номер',
        dataIndex: 'main_serial',
        flex:1
    // },{
    //     header: 'Дополнительный<br/> серийный номер',
    //     dataIndex: 'additional_serial',
    //     flex:1
    },{
        header: 'Инвентарный<br/>номер',
        dataIndex: 'inventory_number',
        flex:1
    },{
        header: 'Кол-во',
        dataIndex: 'amount',
        align: 'right',
        width: 70
    },{
        header: 'Стоимость',
        dataIndex: 'cost',
        align: 'right',
        renderer: function(value) {
             var result = Ext.util.Format.number(value, '0,000.00');
            return result.replace('.',' ');
        },
        summaryType: 'sum',
        summaryRenderer: function(value, summaryData, dataIndex) {
            var result = Ext.util.Format.number(value, '0,000.00');
            return ('<b>' + result.replace('.',' ') + '</b>');
        },
        width: 90
    },{
        header: 'Приме-<br/>чание',
        dataIndex: 'note',
        width: 80
    }
    // , {
    //     header: 'Размещение',
    //     dataIndex: 'location',
    //     width: 100
    // }, {
    //     header: 'Комплект',
    //     dataIndex: 'bundle',
    //     itemId: 'bundleColumn',
    //     hidden: true,
    //     width: 100
    // }
    // , {
    //     header: 'bundle_id',
    //     dataIndex: 'id',
    //     flex:1
    // }
    ],
    bbar: {
        id: 'writeOffItemsGridBbar',
        items: [{
            text: 'i',
            tooltip: 'История',
            itemId: 'historyButton',
            disabled: true,
            action: 'writeOffItemHistory'
        }]
    },
    listeners: {            
        selectionchange: function ( selectionModel, selected, eOpts ) {
            this.getComponent('writeOffItemsGridBbar').getComponent('historyButton').setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'moveToStockFromWriteoffButton' ).setDisabled( !(selectionModel.hasSelection()) );
        }

    }
});
