Ext.define('EQ.view.Items.Writeoff', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.itemsWriteoff',
    requires:[
        'EQ.view.Items.WriteoffGrid'
        ],
    constructor: function() {
        this.height = $(window).height() - 115;
        this.callParent(arguments);
    },
    flex:1,
    padding: 0,
    plain: true,
    renderTo: 'writeoff',
    items: [{
        xtype: 'panel',
        height: 30,
        border: false,
        region: 'north',
        items: [{
            xtype: 'textfield',
            id: 'writeoffSearchField',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            width: 434,
            enableKeyEvents: true,
            anchor : '95%',
            listeners : {
                change: {
                    fn: function (txt, newValue, oldValue) {
                        var grid = Ext.getCmp('writeoffGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            if (txt.value.indexOf('#') == 0) // поиск по идентификатору
                            {
                                var matcher = new RegExp('^' + Ext.String.escapeRegex(txt.value.substring(1)) + '$', "i");
                                grid.store.filter({
                                    filterFn: function (record) {
                                        return matcher.test(record.get('id'));
                                    }
                                });
                                grid.getView().select(0);
                            }
                            else // поиск по тексту
                            {
                                var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                                grid.store.filter({
                                    filterFn: function(record) {
                                        return matcher.test(record.get('title')) ||
                                            matcher.test(record.get('main_serial')) ||
                                            matcher.test(record.get('additional_serial')) ||
                                            matcher.test(record.get('inventory_number'));
                                    }
                                });
                            }   
                        };
                    },
                    buffer: 1000
                }
            }

        }]
    },
    {
        xtype: 'panel',
        region: 'center',
        layout: 'column',
        margin: 0,
        border: false,
        items: [
        {
            xtype: 'writeoffGrid', 
            border: true,
            columnWidth:.99
        }]

    },{
        xtype: 'panel',
        region: 'south',
        layout: 'column',
        itemId: 'writeoffCommandButtons',
        items: [
        {
            xtype: 'button',
            scale: 'medium',
            margin: '10 0 0 10',
            disabled: true,
            id: 'moveToStockFromWriteoffButton',
            text: 'Восстановить на склад'
        }]
    }],
    listeners : {
        afterrender: function () {
            function getUrlVar(key) {
                var result = new RegExp(key + "=([^&]*)", "i").exec(decodeURIComponent(window.location.search));
                return result && unescape(result[1]) || "";
            }
            // console.log(getUrlVar('b'));
            var searchField = Ext.getCmp('writeoffSearchField');
            // для поиска по идентификаторам добавляем символ #
            if (getUrlVar('id') != "")
                searchField.setValue('#' + getUrlVar('id'));
        },
        beforerender: function ( container, eOpts ) {
            var grid = Ext.getCmp('writeoffGrid');
            grid.height = this.height - 70;
                grid.store.load({
                        params: {location_type : 'writeoff'},
                    });
            }
    }

});