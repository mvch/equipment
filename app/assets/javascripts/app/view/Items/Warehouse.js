Ext.define('EQ.view.Items.Warehouse', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.itemsWarehouse',
    requires:[
        'EQ.view.Items.OutsideBundle'
        ],
    flex:1,
    // height: 780,
    padding: 0,
    plain: true,
    renderTo: 'warehouse',
    items: [{
            margin: '10 0 0 0',
            filter: 'atWarehouseWithCommand',
            xtype: 'outsideBundleContainer'
        
    }]
});