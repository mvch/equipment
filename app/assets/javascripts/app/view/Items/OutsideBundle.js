Ext.define('EQ.view.Items.OutsideBundle', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.outsideBundleContainer',
    requires:[
        'EQ.view.Items.OutsideBundleGrid',
        'EQ.view.Bundles.SelectBundleWindow'
        ],
    constructor: function(renderTo) {
        this.height = $(window).height() - 120;
        if (renderTo == 'outsideBundle') {
            this.renderTo = renderTo;            
            this.filter = renderTo;
        } 
        this.callParent(arguments);
    },
    flex:1,
    padding: 0,
    layout: 'border',
    items: [{
        xtype: 'panel',
        height: 30,
        border: false,
        region: 'north',
        items: [{
            xtype: 'textfield',
            id: 'outsideBundleSearchField',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            width: 434,
            enableKeyEvents: true,
            anchor : '95%',
            listeners : {
                change: {
                    fn: function (txt, newValue, oldValue) {
                        var grid = Ext.getCmp('outsideBundleGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            if ( txt.value.indexOf('#') == 0 ) // поиск по идентификатору
                            {
                                var matcher = new RegExp( '^' + Ext.String.escapeRegex(txt.value.substring(1)) + '$', "i");
                                grid.store.filter({
                                    filterFn: function(record) {
                                        return matcher.test(record.get('id'));
                                    }
                                });
                                grid.getView().select(0);
                            }
                            else // поиск по тексту
                            {
                                var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                                grid.store.filter({
                                    filterFn: function(record) {
                                        return matcher.test(record.get('title')) ||
                                            matcher.test(record.get('main_serial')) ||
                                            matcher.test(record.get('additional_serial')) ||
                                            matcher.test(record.get('inventory_number'));
                                    }
                                });
                            }
                        };
                    },
                    buffer: 1000
                }
            }

        }]
    },
    {
        xtype: 'panel',
        region: 'center',
        layout: 'column',
        margin: 0,
        border: false,
        items: [
        {
            xtype: 'outsideBundleGrid', 
            // title: 'Элементы',
            border: true,
            columnWidth:.99
        }]

    },{
        xtype: 'panel',
        region: 'south',
        layout: 'column',
        itemId: 'outsideBundleCommandButtons',
        items: [
        {
            xtype: 'panel',
            items: [
            {
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                id: 'moveOnRepairFromOutsideButton',
                disabled: true,
                text: 'На ремонт'
            },{
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                disabled: true,
                id: 'writeOffFromOutsideButton',
                text: 'Списать'
            },{
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                disabled: true,
                id: 'moveToBundleFromOutsideButton',
                text: 'Переместить в комплект',
                handler: function() {
                    if (Ext.getCmp('outsideBundleGrid').getSelectionModel().hasSelection())
                    {
                        var item = Ext.getCmp('outsideBundleGrid').getSelectionModel().getSelection()[0].data;
                        var win = Ext.widget('selectBundleWindow');
                        var bundleTitle = win.getComponent('selectBundleContainer').getComponent('bundleTitle');
                        bundleTitle.text = item.title;
                        win.editableGridToSelectBundleWindow = 'outsideBundleGrid';
                        win.show();
                    } else {
                        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для перемещения в комплект');
                    }
                }
            },{
                xtype: 'button',
                id: 'selectIUtoSetItemFromOutsideBundleButton',
                scale: 'medium',
                margin: '0 0 0 10',
                // width: 200,
                text: 'Установить в кабинет',
                tooltip: 'Установить элемент для внутреннего использования',
                disabled: true                
            },{
                xtype: 'button',
                id: 'selectUserForTemporaryUseFromOutsideBundleButton',
                scale: 'medium',
                margin: '0 0 0 10',
                text: 'Временное использование',
                tooltip: 'Выбрать пользователя для передачи во временное использование',
                disabled: true                
            }]
        }
        ]
    }
    ],
    listeners : {
        afterrender: function() {
            function getUrlVar(key){
                var result = new RegExp(key + "=([^&]*)", "i").exec(decodeURIComponent(window.location.search)); 
                return result && unescape(result[1]) || ""; 
            }
            // console.log(getUrlVar('b'));
            var searchField = Ext.getCmp('outsideBundleSearchField');
            // для поиска по идентификаторам добавляем символ #
            if (getUrlVar('id') != "")
                searchField.setValue('#' + getUrlVar('id'));
        },
        beforerender: function ( container, eOpts ) {
            var grid = Ext.getCmp('outsideBundleGrid');
            grid.height = this.height - 70;
            grid.store.load({
                    params: {location_type : container.filter},
                });
            if ( container.filter == 'atWarehouseWithCommand' )
            {
                grid.title = 'Элементы на складе и элементы в комплектах на складе';
                grid.headerCt.child('#bundleColumn').show();
            }
            
        },
        beforeshow: function () {
            // console.log(this);
        }
    }
});
