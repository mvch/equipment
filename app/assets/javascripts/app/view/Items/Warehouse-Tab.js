Ext.define('EQ.view.Items.Warehouse', {
    extend: 'Ext.tab.Panel',
    alias : 'widget.itemsWarehouseTabs',
    requires:[
        'EQ.view.BundlesContainer',
        'EQ.view.Items.OutsideBundle'
        ],
    flex:1,
    height: 780,
    padding: 0,
    plain: true,
    renderTo: 'warehouse',
    items: [{
        title: 'Все элементы',
        items: [{            
            margin: '10 0 0 0',
            filter: 'atWarehouseWithCommand',
            xtype: 'outsideBundleContainer'
        }]
    }, {
        title: 'Комплекты',
        items: [
        // {
        //     xtype: 'bundlesContainer',
        //     filter: 'atWarehouseWithCommand',
        //     // autoLoad: true,
        //     margin: '10 0 0 0'
        // }
        ],
        listeners: {
                activate: function (tab) {
                    var panel = new Ext.create('EQ.view.BundlesContainer');
                    tab.add(panel);;
                    tab.doLayout();
                },
                deactivate: function (tab) {
                    tab.removeAll();
                }
            }
        // tooltip: 'Все комплекты на складе'
    }]
});