var canEdit = false;            // можно ли редактировать таблицу в строке
var viewTypeBundlesSelectGrid = "";   // параметр представления таблицы в зависимости от места вызова

var filters = {
    ftype: 'filters',
    encode: true,
    local: false
};      
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        clicksToMoveEditor: 2,
        autoCancel: false,
        listeners: {
            beforeedit: function() {
                return canEdit;
            }
        }

    });
var bundleCurrentRecordId = 0;
var bundleCurrentRecordTitle = '';
Ext.define('EQ.view.Bundles.SelectGrid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.bundlesSelectGrid',
    id: 'bundlesSelectGrid',
    height: 600,
    title: 'Комплекты',
    requires:[
        'EQ.view.Items'
        ],
    // features: [filters],
    store: 'BundlesList',    
    columns: [{
        header: 'Комплект',
        dataIndex: 'title',
        editor: 'textfield',
        width:80,
        height: 43,
        filter: {
                type: 'string'
            }    
    }, {
        header: 'Размещение',
        dataIndex: 'location',
        flex:1
    }
    // , {
    //     header: 'id',
    //     dataIndex: 'id',
    //     width:40
    // }
    ],
    bbar: {
    id: 'bundlesSelectGridBbar',
    items: [
    {
        iconCls: 'icon-add',
        tooltip: 'Добавить',
        handler : function() {
           rowEditing.cancelEdit();
            // Create a model instance
            var r = Ext.create('EQ.model.Bundle', {                
                title: '',
                note: '',
                location_id: 1
            });            
            var store = Ext.data.StoreManager.get("Bundles");
            store.add(r);
            store.load(function() {
                var newRecord = store.last();
                var sm = Ext.ComponentQuery.query('bundlesSelectGrid')[0].getSelectionModel();
                sm.select(newRecord,true);
                rowEditing.startEdit(newRecord,0);    
            });            
            
        }
    }, {
        iconCls: 'icon-edit',
        tooltip: 'Редактировать',
        disabled: true,
        itemId: 'editButton',
        handler: function() {
            rowEditing.cancelEdit();
            canEdit = true;
            var selectedRecord = Ext.ComponentQuery.query('bundlesSelectGrid')[0].getSelectionModel().getSelection()[0];                        
            rowEditing.startEdit(selectedRecord, 0);
            canEdit = false;
        }
    }, {
        iconCls: 'icon-delete',
        tooltip: 'Удалить',
        disabled: true,
        itemId: 'deleteButton',
        handler: function() {
            rowEditing.cancelEdit();
            Ext.MessageBox.confirm('Подтверждение', 'Удалить?', showResult);
            function showResult(btn){
                if (btn == 'yes')
                {
                    var view = Ext.ComponentQuery.query('bundlesSelectGrid')[0];
                    var sm = view.getSelectionModel();
                    var store = Ext.data.StoreManager.get("Bundles");
                    store.remove(sm.getSelection());

                    Ext.getCmp('itemsGrid').store.loadData([], false);
                }
            }
        }
    },{
        iconCls: 'icon-history',
        tooltip: 'История',
        disabled: true,
        itemId: 'historyButton', 
        action: 'historyBundle'
    }
    // ,{
    //             xtype: 'pagingtoolbar',
    //             pageSize: 6,
    //             store: 'Bundles',
    //             displayInfo: true
    //             // plugins: new Ext.ux.SlidingPager()
    //         }
    ]},
    // , renderTo: 'bundlesSelectGrid'
    initComponent: function() {        
    //     this.addBundleButton = new Ext.Button({
    //       text: '+',
    //       action: 'addBundle'
    //     });
    //     this.editBundleButton = new Ext.Button({
    //           text: '~',
    //           action: 'editBundle',
    //           disabled: false
    //         });
    //     this.deleteBundleButton = new Ext.Button({
    //       text: '-',
    //       action: 'deleteBundle',
    //       disabled: false
    //     });
    //     this.bbar = [this.addBundleButton, this.editBundleButton, this.deleteBundleButton]; 

        this.callParent(arguments);
    }, 
    plugins: [
        rowEditing
        // Ext.create('Ext.grid.plugin.RowEditing', {
        //     clicksToEdit: 2
        // })
    ]
});