Ext.define('EQ.view.Bundles.SelectBundleWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.selectBundleWindow',
    requires:[
        'EQ.view.Bundles.SelectGrid'
        ],
    title: 'Выбор комплекта',
    closable: true,
    // closeAction: 'hide',
    autoShow: false,
    floatable: false,
    modal: true,
    width: 500,
    // height: 550,
    items: [{
		xtype: 'container',
		itemId: 'selectBundleContainer',
		// height: 40,
		margin: '10 10 10 10',
		items : [{
			xtype:'label',	
			componentCls: 'labelHeader',
			text: 'Выбор комплекта для элемента:',
			margin: '10 10 0 0'
		}, {
			xtype:'label',	
			componentCls: 'labelHeader strongText',
			text: 'title',			
			itemId: 'bundleTitle'
		}
		]
	}, {
        xtype: 'textfield',
        plugins:['clearbutton'],
        fieldLabel: 'Поиск',
        labelWidth: 50,
        width: 470,
        margin: '20 10 10 10',
        enableKeyEvents: true,
        anchor : '95%',
        listeners : {
            change: {
                fn: function (txt, newValue, oldValue) {
                    var grid = Ext.getCmp('bundlesSelectGrid');
                    grid.store.clearFilter();
                    if (txt.value) {
                        var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                        grid.store.filter({
                            filterFn: function(record) {
                                return matcher.test(record.get('title'));
                            }
                        });
                    };
                },
                buffer: 1000
            }
        }

    },{
        xtype: 'bundlesSelectGrid',
        height: 350,
        border: true,
        width: 470,
        margin: '10 10 10 10',
        header: false

    }, {
        xtype: 'button',
        scale: 'medium',
        margin: '0 0 10 10',
        text: 'Подтвердить',
        handler: function() {
            if (Ext.getCmp('bundlesSelectGrid').getSelectionModel().hasSelection())
            {
                // обновляем у элемента ссылку на комплект
                var bundle = Ext.getCmp('bundlesSelectGrid').getSelectionModel().getSelection()[0].data;
                var editableGrid = this.up().editableGridToSelectBundleWindow;
                var store = Ext.getCmp(editableGrid).getStore();
                var currentItemData = Ext.getCmp(editableGrid).getSelectionModel().getSelection()[0].data;
                var curBundle_id = currentItemData.bundle_id;
                var currentItem = store.getById(currentItemData.id);
                currentItem.set({ bundle_id: bundle.id, location_id: 6 });                
                // обновляем содержимое таблицы
                if ( editableGrid == 'itemsGrid' )
                {                    
                    var bStore = Ext.create('EQ.store.BundleItems');
                    bStore.proxy.url = '/bundleitems/' + curBundle_id;
                    bStore.load(function() {
                            var newData = bStore.getProxy().getReader().rawData;
                            // console.log(newData);
                            Ext.ComponentQuery.query('itemsGrid')[0].getStore().loadRawData(newData);                                                
                        });                

                } else
                {
                   store.load(); 
                }

                this.up('.window').close();
            } else {
                Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите комплект для передачи');
            }

        }
    }],
    listeners: {
        beforerender: function ( ) {
            viewTypeBundlesSelectGrid = 'selectBundle';
            var bundlesSelectGrid1 = Ext.getCmp('bundlesSelectGrid');
            bundlesSelectGrid1.store.load();
            bundlesSelectGrid1.store.clearFilter();
            bundlesSelectGrid1.columns[0].flex = 1;

            // bundlesSelectGrid.getComponent('titleColumn').flex = 1;
        },
        beforeclose: function() {
            viewTypeBundlesSelectGrid = 'editBundle';
        }
    }
});