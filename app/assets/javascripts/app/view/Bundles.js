var canEdit = false;            // можно ли редактировать таблицу в строке
var viewTypeBundlesGrid = "";   // параметр представления таблицы в зависимости от места вызова

var filters = {
    ftype: 'filters',
    encode: true,
    local: false
};      
var rowEditingBundles = Ext.create('Ext.grid.plugin.RowEditing', {
        autoCancel: false,
        listeners: {
            beforeedit: function() {
                return canEdit;
            }
        }

    });
var bundleCurrentRecordId = 0;
var bundleCurrentRecordTitle = '';

Ext.define('EQ.view.Bundles', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.bundlesGrid',
    id: 'bundlesGrid',
    // height: 600,
    title: 'Комплекты',
    requires:[
        'EQ.view.Items'
        ],
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
    store: 'Bundles',    
    columns: [{
        header: 'Комплект',
        dataIndex: 'title',
        editor: 'textfield',
        width:90,
        // height: 43,
        filter: {
                type: 'string'
            },
        summaryType: 'count',
        summaryRenderer: function(value, summaryData, dataIndex) {
                return ('<b>Всего: ' + value + '</b>');
        }      
    }, {
        header: 'Размещение',
        dataIndex: 'location',
        width: 120,
        flex:1
        , summaryRenderer: function (value, summaryData, dataIndex) {
            return ('<b>На складе: ' + value + '</b>');
        }
        , summaryType: function (records) {
            var total = records.reduce(function (sums, record) {
                return Number(sums) + Number((record.data.location == 'Склад') ? 1 : 0);
            }, [0]);

            return total;
        }
    }
    // , {
    //     header: 'id',
    //     dataIndex: 'id',
    //     width:40
    // }
    ],
    bbar: {
    id: 'bundlesGridBbar',
    items: [
    {
        // text: '+',
        iconCls: 'icon-add',
        tooltip: 'Добавить',
        // iconCls: 'employee-add',
        handler : function() {
           rowEditingBundles.cancelEdit();
            // Create a model instance
            var r = Ext.create('EQ.model.Bundle', {                
                title: '',
                note: '',
                location_id: 1
            });            
            var store = Ext.data.StoreManager.get("Bundles");
            store.add(r);
            store.load(function() {
                var newRecord = store.last();
                var sm = Ext.ComponentQuery.query('bundlesGrid')[0].getSelectionModel();
                sm.select(newRecord,true);
                canEdit = true;
                rowEditingBundles.startEdit(newRecord,0);    
                canEdit = false;
            });            
            
        }
    }, {
        // text: '~',
        iconCls: 'icon-edit',
        tooltip: 'Редактировать',
        disabled: true,
        itemId: 'editButton',
        handler: function() {
            rowEditingBundles.cancelEdit();
            canEdit = true;
            var selectedRecord = Ext.getCmp('bundlesGrid').getSelectionModel().getSelection()[0];//Ext.ComponentQuery.query('bundlesGrid')[0].getSelectionModel().getSelection()[0];                        
            rowEditingBundles.startEdit(selectedRecord, 0);
            canEdit = false;
        }
    }, {
        // text: '-',
        iconCls: 'icon-delete',
        tooltip: 'Удалить',
        disabled: true,
        itemId: 'deleteButton',
        handler: function() {
            rowEditingBundles.cancelEdit();
            Ext.MessageBox.confirm('Подтверждение', 'Удалить?', showResult);
            function showResult(btn){
                if (btn == 'yes')
                {
                    var view = Ext.ComponentQuery.query('bundlesGrid')[0];
                    var sm = view.getSelectionModel();
                    var store = Ext.data.StoreManager.get("Bundles");
                    store.remove(sm.getSelection());

                    Ext.getCmp('itemsGrid').store.loadData([], false);
                }
            }
        }
    }, {
        iconCls: 'icon-history',
        tooltip: 'История',
        disabled: true,
        itemId: 'historyButton', 
        action: 'historyBundle'
    }, {
        iconCls: 'icon-print',
        tooltip: 'Печать',
        disabled: true,
        itemId: 'printBundleButton', 
        action: 'printBundleButton'
    }
    // ,{
    //             xtype: 'pagingtoolbar',
    //             pageSize: 6,
    //             store: 'Bundles',
    //             displayInfo: true
    //             // plugins: new Ext.ux.SlidingPager()
    //         }
    ]},
    // , renderTo: 'bundlesGrid'    
    plugins: [
        rowEditingBundles
        // Ext.create('Ext.grid.plugin.RowEditing', {
        //     clicksToEdit: 2
        // })
    ]
});