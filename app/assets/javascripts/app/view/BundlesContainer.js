var searchValue = "";
Ext.define('EQ.view.BundlesContainer', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.bundlesContainer',
    id: 'bundlesContainer',
    requires:[
        'EQ.view.Items',
        'EQ.view.Bundles',
        'EQ.view.ExternalUsers.SelectWindow',
        'EQ.view.InternalUsers.SelectWindow'
        ],
    constructor: function() {
        this.height = $(window).height() - 125;
        this.callParent(arguments);
        //return this;
    },
    flex:1,
    // height: 680,
    padding: 0,
    layout: 'border',
    items: [{
        xtype: 'panel',
        height: 30,
        border: false,
        region: 'north',
        layout: 'hbox',
        items: [{
            xtype: 'textfield',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            width: 203,
            enableKeyEvents: true,
            anchor : '95%',
            value: '',
            id: 'searchBundleTextField',
            listeners : {
                change: {
                    fn: function (txt, newValue, oldValue) {
                        var grid = Ext.getCmp('bundlesGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            if ( txt.value.indexOf('#') == 0 ) // поиск по идентификатору
                            {
                                var matcher = new RegExp( '^' + Ext.String.escapeRegex(txt.value.substring(1)) + '$', "i");
                                grid.store.filter({
                                    filterFn: function(record) {
                                        return matcher.test(record.get('id'));
                                    }
                                });
                                if ( grid.store.getCount() > 0 ) { 
                                    grid.getView().select(0); 
                                }
                            }
                            else // поиск по тексту
                            {
                                var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                                grid.store.filter({
                                    filterFn: function(record) {
                                        return matcher.test(record.get('title'));
                                    }
                                });
                            }                            
                        };                        
                    },
                    buffer: 1000
                }
            }

        }
        , {
            // xtype: 'textfield',
            xtype: 'label',
            id: 'bundlesCountLabel',
            text: '',
            margin: '5 5 0 10'
        }
        ]
    },
    {
        xtype: 'panel',
        region: 'center',
        layout: 'column',
        // height: 600,
        margin: 0,
        border: false,
        items: [
        {
            xtype: 'bundlesGrid',
            title: 'Комплекты',
            border: true,
            width: 203
        },
        {
            xtype: 'panel',
            html: '&nbsp;',
            width: 10
        },
        {
            xtype: 'itemsGrid', //itemsGrid
            // title: 'Элементы',
            border: true,
            // height: 600,
            columnWidth:.99
        }],

    },{
        xtype: 'panel',
        itemId: 'buttonsPanel',
        region: 'south',
        layout: 'column',
        items: [
        {
            xtype: 'panel',
            width: 205,
            items: [
            {
                xtype: 'button',
                id: 'selectEUtoSetBundleButton',
                scale: 'medium',
                text: 'Передать пользователю',
                width: 200,
                disabled: true,
                margin: '0 0 5 0',
                handler: function() {
                    if (Ext.getCmp('bundlesGrid').getSelectionModel().hasSelection())
                    {
                        var store = Ext.getStore('ExternalUsersSelect');
                        store.load();
                        var bundle = Ext.getCmp('bundlesGrid').getSelectionModel().getSelection()[0].data;
                        var win = Ext.widget('selectEUWindow');
                        var bundleTitle = win.getComponent('selectEUContainer').getComponent('bundleTitle');
                        bundleTitle.text = bundle.title;
                        win.show();
                    } else {
                        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите комплект для передачи');
                    }
                }
            },{
                xtype: 'button',
                id: 'selectIUtoSetBundleButton',
                scale: 'medium',
                width: 200,
                text: 'Установить в кабинет',
                tooltip: 'Установить комплект для внутреннего использования',
                disabled: true,
                margin: '0 0 5 0',
                handler: function() {
                    if (Ext.getCmp('bundlesGrid').getSelectionModel().hasSelection())
                    {
                        var store = Ext.getStore('InternalUsers');
                        store.load();
                        var bundle = Ext.getCmp('bundlesGrid').getSelectionModel().getSelection()[0].data;
                        var win = Ext.widget('selectIUWindow');
                        var selectedTitle = win.getComponent('selectIUContainer').getComponent('selectedTitle'),
                            selectedId = win.getComponent('selectIUContainer').getComponent('selectedId'),
                            gridId = win.getComponent('selectIUContainer').getComponent('gridId');
                        selectedTitle.text = bundle.title;
                        selectedId.value = bundle.id;
                        gridId.value = 'bundlesGrid';
                        win.show();
                    } else {
                        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите комплект для установки в кабинет');
                    }
                }
            },{
                xtype: 'button',
                id: 'selectIUtoSetReturnBundleButton',
                scale: 'medium',
                width: 200,
                text: 'Вернуть на склад',
                tooltip: 'Вернуть комплект на склад',
                disabled: true,
                handler: function() {
                    if (Ext.getCmp('bundlesGrid').getSelectionModel().hasSelection())
                    {
                        function showResult(btn){
                          if (btn == 'yes')
                            {
                              var bundleRecord = Ext.getCmp('bundlesGrid').getSelectionModel().getSelection()[0];
                              var bundle = bundleRecord.data;
                              var today = new Date(),
                                todayDate = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);

                              // находится ли у внешнего пользователя
                              if (bundle.external_users_id != 0 || bundle.internal_users_id != 0) {
                                var euStore = Ext.data.StoreManager.get("ExternalUsers");
                                euStore.load(function(){
                                    if (bundle.external_users_id != 0) { 
                                        // удаляем у внешнего пользователя ссылку на комплект и дату возврата комплекта
                                        var euRecord = euStore.getById(bundle.external_users_id);
                                        euRecord.set({ bundle_id: null, bundle_title: null, uninstalldate: todayDate });
                                    }
                                    if (bundle.internal_users_id != 0) { 
                                        // удаляем у внутреннего пользователя ссылку на комплект и дату возврата комплекта
                                        var euRecord = euStore.getById(bundle.internal_users_id);
                                        euRecord.set({ bundle_id: null, bundle_title: null, uninstalldate: todayDate });
                                    }
                                });
                              }
                              // удаляем у комплекта ссылку на пользователя                              
                              bundleRecord.set({ external_users_id: null, location_id: 1 });
                              Ext.getCmp('bundlesGrid').store.reload();                              

                            }
                        }
                        Ext.MessageBox.confirm('Подтверждение', 'Вернуть комплект?', showResult);                        
                    } else {
                        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите комплект для возврата на склад.');
                    }
                }
            }]

        },{
            xtype: 'panel',
            html: '&nbsp;',
            width: 10
        },{
            xtype: 'panel',
            layout: 'auto',
            items: [
            {
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                id: 'moveToStockButton',
                disabled: true,
                text: 'На склад',
                action: 'moveToStock'
            },{
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                id: 'moveOnRepairButton',
                disabled: true,
                text: 'В ремонт'
            },{
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                id: 'writeOffButton',
                text: 'Списать',
                disabled: true
            },{
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                id: 'moveToBundleButton',
                disabled: true,
                text: 'Переместить в комплект'
            },{
                xtype: 'button',
                id: 'selectIUtoSetItemButton',
                scale: 'medium',
                margin: '0 0 0 10',
                // width: 200,
                text: 'В кабинет',
                tooltip: 'Установить элемент для внутреннего использования',
                disabled: true                
            },{
                xtype: 'button',
                id: 'selectUserForTemporaryUseItemButton',
                scale: 'medium',
                margin: '0 0 0 10',
                 // width: 150,
                text: 'Временное использование',
                tooltip: 'Выбрать пользователя для передачи во временное использование',
                disabled: true                
            }
            // ,{
            //     xtype: 'button',
            //     scale: 'medium',
            //     margin: '0 0 0 10',
            //     id: 'itemCheckButton',
            //     disabled: true,
            //     text: 'Оборудование проверено'
            // }
            ]
        }
        ]
    }
    ],
    listeners : {
        afterrender: function() {
            function getUrlVar(key){
                var result = new RegExp(key + "=([^&]*)", "i").exec(decodeURIComponent(window.location.search)); 
                return result && unescape(result[1]) || ""; 
            }
            var searchField = Ext.getCmp('searchBundleTextField');
            if (getUrlVar('b') != "")
                searchField.setValue(getUrlVar('b'));
            // для поиска по идентификаторам добавляем символ #
            if (getUrlVar('id') != "")
                searchField.setValue('#' + getUrlVar('id'));
            // var grid = Ext.getCmp('bundlesGrid');
            // grid.getSelectionModel().select(0);
        },
        beforerender: function ( container, eOpts ) {
            var bundlesGrid = Ext.getCmp('bundlesGrid');
            var itemsGrid = Ext.getCmp('itemsGrid');
            if ( container.invoker != 'BundleWindow' ) {
                bundlesGrid.height = this.height - 140;
                itemsGrid.height = this.height - 140;
            }
            
            if ( container.filter == 'atWarehouse' || container.filter == 'atWarehouseWithCommand' )
            {   
                bundlesGrid.store.load({
                        params: {filter : container.filter},
                    })
            }
            if ( container.filter == 'atWarehouse' )
            {
                container.getComponent('buttonsPanel').hide();
            }


        }
    }
});
