Ext.define('EQ.view.Search.Main', {
    extend: 'Ext.container.Container',
    alias : 'widget.searchMainContainer',
    id: 'searchMainContainer',
    requires:[
        'EQ.view.Search.Grid'
        ],    
    flex:1,
    constructor: function() {
        this.height = $(window).height() - 115;
        this.callParent(arguments);
    },
    padding: 0,    
    items: [
        {
            xtype: 'textfield',
            id: 'searchField',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            tooltip: 'tooltip2',
            width: 300,
            enableKeyEvents: true,
            anchor : '95%',            
            inputAttrTpl: " data-qtip='Введите искомый текст (минимум два символа)' ",
            listeners : {   
                change: {
                    fn: function (txt, newValue, oldValue) {
                        if ( newValue.length > 1) {
                            var store = Ext.data.StoreManager.get("Searches");
                            store.load(
                            {
                                params: {searchstr : newValue},
                            });
                        }
                    },
                    buffer: 1000
                }
            }
        },
        {
            xtype: 'searchGrid'            
        }, {
            xtype: 'container',
            margin: '10 0 0 0',

            items:  [{
                xtype: 'button',
                id: 'goToButton',
                scale: 'medium',
                disabled: true,
                text: 'Перейти',
                handler: function () {
                    var selectedRow = Ext.getCmp('searchGrid').getSelectionModel().getSelection()[0].data;
                    var categoryPath = "";
                    switch (selectedRow.category) {
                        case 1: categoryPath = "/teachers/index.html?id=" + selectedRow.element_id; break;
                        case 2: categoryPath = "/students/index.html?id=" + selectedRow.element_id; break;
                        case 3: categoryPath = "/bundles/index.html?id=" + selectedRow.element_id; break;
                        case 4: categoryPath = "/items/index.html?id=" + selectedRow.element_id; break;
                        case 5: categoryPath = "/bundles/index.html?id=" + selectedRow.bundle_id; break;
                        case 6: categoryPath = "/writeoff/index.html?id=" + selectedRow.element_id; break;
                        case 7: categoryPath = "/internalusers/index.html?id=" + selectedRow.element_id; break;
                        case 8: categoryPath = "/warehouse/index.html?id=" + selectedRow.element_id; break;
                    }
                    window.location = categoryPath;
                }
            }]
        }],
    listeners : {
        beforerender: function ( container, eOpts ) {
            var searchGrid = Ext.getCmp('searchGrid');
            searchGrid.height = this.height - 70;
        }
    },
    renderTo: 'search'
});