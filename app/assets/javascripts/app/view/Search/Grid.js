Ext.define('EQ.view.Search.Grid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.searchGrid',    
    id: 'searchGrid',
    title: 'Результаты поиска',
    store: 'Searches',
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
    border: true,
    columns: [{
        header: 'Категория',
        dataIndex: 'category',
        align: 'center',
        width:100,
        renderer: function(value){
            var categoryText = ""
            switch(value) {
                case 1: categoryText = "Преподаватель"; break;
                case 2: categoryText = "Ученик"; break;
                case 3: categoryText = "Комплект"; break;
                case 4: categoryText = "Элемент вне комплекта"; break;
                case 5: categoryText = "Элемент комплекта"; break;
                case 6: categoryText = "Элемент списан"; break;
                case 7: categoryText = "Внутреннее использование"; break;
            }            
            s = '<div class="iconCategory' + value + '" title="' + categoryText + '"></div>';
            return s;
            // return '<img src="category' + value + '.png" alt="Категория" />';
        }
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                return ('<b>Всего: ' + value + '</b>');
        }
    },{
        header: 'Наименование',
        dataIndex: 'title',
        flex:1
    },{
        header: 'Описание',
        dataIndex: 'description',
        flex:1
    }
    // , {
    //     header: 'element_id',
    //     dataIndex: 'element_id',
    //     width:110
    // }
    ],
    listeners: {            
        selectionchange: function ( selectionModel, selected, eOpts ) {
            Ext.getCmp( 'goToButton' ).setDisabled( !(selectionModel.hasSelection()) );
        }
    }
});
