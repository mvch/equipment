Ext.define('EQ.view.Repairs.Main', {
    extend: 'Ext.container.Container',
    alias : 'widget.repairsMainContainer',
    id: 'repairsMainContainer',
    requires:[
        'EQ.view.Repairs.Grid',
        'EQ.view.Repairs.ShowWindow',
        ],    
    flex:1,
    constructor: function() {
        this.height = $(window).height() - 115;
        this.callParent(arguments);
    },
    padding: 0,    
    // plugins: ,
    // layout:'column',
    items: [
        {
            xtype: 'textfield',
            id: 'repairsSearchField',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            tooltip: 'tooltip2',
            width: 300,
            enableKeyEvents: true,
            anchor : '95%',            
            inputAttrTpl: " data-qtip='Поиск по Ф. И. О., адресу и комплекту' ",
            listeners : {   
                change: {
                    fn: function (txt, newValue, oldValue) {
                        var grid = Ext.getCmp('repairsGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                            grid.store.filter({
                                filterFn: function(record) {
                                    return matcher.test(record.get('surname')) ||
                                        matcher.test(record.get('name')) ||
                                        matcher.test(record.get('patronymic')) ||
                                        matcher.test(record.get('address')) ||
                                        matcher.test(record.get('bundle_title'));
                                }
                            });
                        };
                    },
                    buffer: 1000
                }
            }
        },
        {
            xtype: 'repairsGrid'            
        }, {
            xtype: 'container',
            margin: '10 0 0 0',

            items:  [{
                xtype: 'button',
                id: 'setBundleToEUButton1',
                scale: 'medium',                
                text: 'Передать комплект',
                handler: function() {
                    console.log(this);
                    var win = Ext.widget('showRepairWindow');
                    win.show();
                    // if (Ext.getCmp('externalUsersGrid').getSelectionModel().hasSelection())
                    // {
                    //     var userFIO = Ext.getCmp('externalUsersGrid').getSelectionModel().getSelection()[0].data;
                    //     var win = Ext.widget('setBundleWindow');
                    //     var bundleTitle = win.getComponent('selectBundleContainer').getComponent('userFIO');
                    //     bundleTitle.text = userFIO.surname + ' ' + userFIO.name + ' ' + userFIO.patronymic;
                    //     win.show();
                    // } else {
                    //     Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите кому нужно передать комплект');
                    // }
                }
            }]
        }],

    listeners : {
        beforerender: function ( container, eOpts ) {
            var repairsGrid = Ext.getCmp('repairsGrid');
            repairsGrid.height = this.height - 70;
            // загрузка данных
            var store = Ext.data.StoreManager.get("Repairs");
            store.load();
        }
    },
    renderTo: 'repairs'
});