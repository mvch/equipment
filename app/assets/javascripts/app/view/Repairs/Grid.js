Ext.define('EQ.view.Repairs.Grid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.repairsGrid',    
    id: 'repairsGrid',
    title: 'Элементы в ремонте',
    store: 'Repairs',
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
    border: true,
    columns: [{
        header: 'Место ремонта',
        dataIndex: 'place',
        flex:1,
        renderer: function (value, meta) {
           meta.attr = 'style="white-space:normal"';
           return value;
           }  
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                return ('<b>Всего: ' + value + '</b>');
        }
    }, {
        header: 'Дата отправки<br/> на ремонт',
        dataIndex: 'sent',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width:110
    },{
        header: 'Кол-во дней<br/> для контроля',
        dataIndex: 'control_days',
        width:110
    },{
        header: 'Дата получения<br/> из ремонта',
        dataIndex: 'receive',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width:120
    },{
        header: 'Примечание',
        dataIndex: 'description',
        flex:1
    },{
        header: 'Элемент',
        dataIndex: 'item_title',
        flex:1
    }, {
        header: 'Серийный номер',
        dataIndex: 'item_serial',
        flex:1
    }
    // , {
    //     header: 'bundle_id',
    //     dataIndex: 'id',
    //     flex:1
    // }
    ],
    bbar: {
        id: 'repairsGridBbar',
        items: [{
            text: 'i',
            tooltip: 'История',
            itemId: 'historyButton',
            disabled: true,
            // iconCls: 'employee-add',
            handler : function() {
                console.log(this);
            }
        }]
    },
    listeners: {            
        selectionchange: function ( selectionModel, selected, eOpts ) {
            this.getComponent('repairsGridBbar').getComponent('historyButton').setDisabled( !(selectionModel.hasSelection()) );
            // Ext.getCmp( 'moveToStockFromWriteoffButton' ).setDisabled( !(selectionModel.hasSelection()) );
        }
    },
    viewConfig: {
        getRowClass: function(record) {
            var highlight = false;
            if (record.get('receive') == null && record.get('sent') != null) {
                var sentDate = new Date(record.get('sent'));
                var controlDate = new Date(record.get('sent'));
                var control_days = record.get('control_days');
                controlDate.setDate(sentDate.getDate()+control_days);
                var currentdate = new Date();
                highlight = currentdate > controlDate;
            }
            return highlight
                ? 'highlight'
                : '';

        }
    }
});
