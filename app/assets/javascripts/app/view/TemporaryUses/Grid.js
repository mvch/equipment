Ext.define('EQ.view.TemporaryUses.Grid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.temporaryUsesGrid',    
    id: 'temporaryUsesGrid',
    title: 'Элементы, переданные во временное использование',
    store: 'TemporaryUses',
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
    border: true,
    columns: [{
        header: 'Пользователь',
        dataIndex: 'fio',
        flex:1,
        renderer: function (value, meta) {
           meta.attr = 'style="white-space:normal"';
           return value;
           }  
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                return ('<b>Всего: ' + value + '</b>');
        }
    },
    {
        header: 'Преподаватель',
        dataIndex: 'is_teacher',
        xtype: 'checkcolumn',
        editor: 'checkbox',
        // hidden: true,
        itemId: 'columnIsTeacherTU',
        id: 'columnIsTeacherTU',
        width: 120
    },
    {
        header: 'Дата передачи',
        dataIndex: 'from',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width:110
    },{
        header: 'На срок до',
        dataIndex: 'before',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width:110
    },{
        header: 'Дата возврата',
        dataIndex: 'receive',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width:120
    },{
        header: 'Примечание',
        dataIndex: 'description',
        flex:1
    },{
        header: 'Элемент',
        dataIndex: 'item_title',
        flex:1
    }, {
        header: 'Серийный номер',
        dataIndex: 'item_serial',
        flex:1
    }
    // , {
    //     header: 'bundle_id',
    //     dataIndex: 'id',
    //     flex:1
    // }
    ],
    bbar: {
        id: 'temporaryUsesGridBbar',
        items: [{
        iconCls: 'icon-edit',
        itemId: 'editButton',
        tooltip: 'Редактировать',
        disabled: true,
        visible: false,
        handler: function() {
                console.log('edit');
                // rowEditingEU.cancelEdit();
                // canEdit = true;
                // var selectedRecord = Ext.ComponentQuery.query('externalUsersGrid')[0].getSelectionModel().getSelection()[0];                        
                // rowEditingEU.startEdit(selectedRecord, 0);
                // rowEditingEU.startEdit(selectedRecord, 0);
                // canEdit = false;
            }
        }, {
            tooltip: 'История элемента',
            itemId: 'historyButton',
            disabled: true,
            iconCls: 'icon-history',
            handler : function() {
                console.log('item history');
            }
        }, { 
            xtype: 'tbfill' 
        }, {
            xtype: 'checkbox',
            fieldLabel: 'Архивные',
            labelWidth: 70,
            handler: function() {
                store = this.up().up().store;
                store.load(
                {
                    params: { archive : (this.getValue() ? 'true' : 'false') },
                })
        }
    }]
    },
    listeners: {            
        selectionchange: function ( selectionModel, selected, eOpts ) {
            this.getComponent('temporaryUsesGridBbar').getComponent('historyButton').setDisabled( !(selectionModel.hasSelection()) );
            this.getComponent('temporaryUsesGridBbar').getComponent('editButton').setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'returnFromTUButton' ).setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'printTemporaryUse' ).setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'printTemporaryUseAct' ).setDisabled( !(selectionModel.hasSelection()) );
        }
    },
    viewConfig: {
        getRowClass: function(record) {
            var highlight = false;
            if (record.get('receive') == null && record.get('sent') != null) {
                var sentDate = new Date(record.get('sent'));
                var controlDate = new Date(record.get('sent'));
                var control_days = record.get('control_days');
                controlDate.setDate(sentDate.getDate()+control_days);
                var currentdate = new Date();
                highlight = currentdate > controlDate;
            }
            return highlight
                ? 'highlight'
                : '';

        }
    }
});
