Ext.define('EQ.view.TemporaryUses.SelectUserWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.selectUserForTemporaryUseWindow',
    id: 'selectUserForTemporaryUseWindow',
    requires:[ 'EQ.view.ExternalUsers.SelectWindowContainer' ],
    title: 'Выбор пользователя для передачи',    
    closable: true,
    closeAction: 'destroy',
    modal: true,
    width: 730,
    // height: 680,
    items: [{
        xtype: 'container',
        itemId: 'selectEUContainer',
        height: 40,
        margin: '10 10 10 10',
        items : [{
            xtype:'label',  
            componentCls: 'labelHeader',
            text: 'Выберите пользователя для передачи во временное использование элемента:',
            margin: '0 10 0 0'
        }, {
            xtype:'label',  
            componentCls: 'labelHeader strongText',
            // text: ' ',
            itemId: 'selectedTitle'
        }, {
            xtype: 'hidden',
            itemId: 'selectedId'
        }, {
            xtype: 'hidden',
            itemId: 'gridId'
        }, {
            xtype: 'hidden',
            itemId: 'bundleId'
        }]
    }, {
        xtype: 'selectWindowContainer',
        margin: '0 0 0 10',
        height: 500
    }, {
        xtype: 'container',
        itemId: 'datesContainer',
        height: 40,
        margin: '0 10 0 10',
        layout: 'hbox',
        items : [{
            xtype: 'datefield',
            format: 'd.m.Y',
            submitFormat: 'Y-m-d\\TH:i:s',
            itemId: 'fromdate',
            fieldLabel: 'Дата передачи',
            value: new Date(),
            validator: function (value) {
                var result = true,
                    pattern = /(\d{2})\.(\d{2})\.(\d{4})/,
                    fromDate = new Date(value.replace(pattern,'$3-$2-$1')),
                    beforeDate = this.up('window').getComponent('datesContainer').getComponent('beforedate').value;
                if ( beforeDate < fromDate ) {
                    result = 'Начало периода не может быть позже его окончания'
                }
                return result;
            }
        },{
            itemId: 'beforedate',
            margin: '0 0 0 20',
            labelWidth: 250,
            xtype: 'datefield',
            format: 'd.m.Y',
            submitFormat: 'Y-m-d\\TH:i:s',
            fieldLabel: 'Окончание временного использования',
            validator: function (value) {
                var result = true,
                    pattern = /(\d{2})\.(\d{2})\.(\d{4})/,
                    beforeDate = new Date(value.replace(pattern,'$3-$2-$1')),
                    fromDate = this.up('window').getComponent('datesContainer').getComponent('fromdate').value;
                if ( beforeDate < fromDate ) {
                    result = 'Окончание периода не может быть раньше его начала'
                }
                return result;
            }
        }]
    },{
        xtype: 'container',
        itemId: 'fieldsContainer',
        height: 40,
        margin: '0 10 0 10',
        layout: 'hbox',
        items : [{
            itemId: 'description',
            margin: '0 10 10 0',
            width: 495,
            xtype: 'textfield',
            fieldLabel: 'Примечание'
        },{
            itemId: 'printContract',
            labelAlign: 'top',
            xtype: 'checkbox',            
            boxLabel: 'Печать договора передачи'
        }]
    }, {
        xtype: 'button',
        scale: 'medium',
        margin: '0 0 10 10',        
        action: 'save',
        text: 'Подтвердить'
    }
    ]
});
