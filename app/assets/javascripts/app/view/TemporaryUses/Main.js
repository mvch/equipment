Ext.define('EQ.view.TemporaryUses.Main', {
    extend: 'Ext.container.Container',
    alias : 'widget.temporaryUsesMainContainer',
    id: 'temporaryUsesMainContainer',
    requires:[
        'EQ.view.TemporaryUses.Grid',
        'EQ.view.TemporaryUses.ShowWindow',
        ],    
    flex:1,
    constructor: function() {
        this.height = $(window).height() - 115;
        this.callParent(arguments);
    },
    padding: 0,    
    // plugins: ,
    // layout:'column',
    items: [
        {
            xtype: 'textfield',
            id: 'temporaryUsesSearchField',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            tooltip: 'tooltip2',
            width: 300,
            enableKeyEvents: true,
            anchor : '95%',            
            inputAttrTpl: " data-qtip='Поиск по ...' ",
            listeners : {   
                change: {
                    fn: function (txt, newValue, oldValue) {
                        var grid = Ext.getCmp('temporaryUsesGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                            grid.store.filter({
                                filterFn: function(record) {
                                    return matcher.test(record.get('before')) // ||
                                        // matcher.test(record.get('name')) ||
                                        // matcher.test(record.get('patronymic')) ||
                                        // matcher.test(record.get('address')) ||
                                        matcher.test(record.get('from'));
                                }
                            });
                        };
                    },
                    buffer: 1000
                }
            }
        },
        {
            xtype: 'temporaryUsesGrid'            
        }, {
            xtype: 'container',
            margin: '10 0 0 0',

            items:  [{
                xtype: 'button',
                id: 'returnFromTUButton',
                scale: 'medium',
                disabled: true,
                text: 'Вернуть'
            }, {
                xtype: 'button',
                id: 'printTemporaryUse',
                scale: 'medium',
                disabled: true,
                margin: '0 0 0 10',
                text: 'Печать договора передачи'
            }, {
                xtype: 'button',
                id: 'printTemporaryUseAct',
                scale: 'medium',
                disabled: true,
                margin: '0 0 0 10',
                text: 'Печать акта возврата'
            }]
        }],

    listeners : {
        beforerender: function ( container, eOpts ) {
            var temporaryUsesGrid = Ext.getCmp('temporaryUsesGrid');
            temporaryUsesGrid.height = this.height - 70;
            // загрузка данных
            var store = Ext.data.StoreManager.get("TemporaryUses");
            store.load();
        }
    },
    renderTo: 'temporaryUses'
});