Ext.define('EQ.view.TemporaryUses.ReturnConfirmWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.returnConfirmWindow',
    id: 'returnConfirmWindow',
    title: 'Подтверждение возврата',
    closable: true,
    closeAction: 'destroy',
    modal: true,
    width: 380,
    // height: 480,
    items: [{
        xtype: 'container',
        itemId: 'returnConfirmContainer',
        // height: 60,
        layout: 'vbox',
        margin: '10 10 10 10',
        items : [{
            xtype:'label',  
            // componentCls: 'labelHeader',
            text: 'Укажите дату возврата из временного использования:',
            margin: '0 10 0 0'
        }, {
            xtype:'label',  
            componentCls: 'strongText',
            itemId: 'selectedTitle'
        }, {
            xtype: 'hidden',
            itemId: 'selectedId'
        }, {
            xtype: 'hidden',
            itemId: 'fromdate'
        }, {
            xtype: 'hidden',
            itemId: 'bundleId'
        }]
    }, {
        xtype: 'container',
        itemId: 'dataContainer',
        height: 60,
        margin: '0 10 0 10',
        layout: 'vbox',
        items : [{
            xtype: 'datefield',
            format: 'd.m.Y',
            submitFormat: 'Y-m-d\\TH:i:s',
            itemId: 'receivedate',
            fieldLabel: 'Дата возврата',
            value: new Date(),
            validator: function (value) {
                var result = true,
                    pattern = /(\d{2})\.(\d{2})\.(\d{4})/,
                    receiveDate = new Date(value.replace(pattern,'$3-$2-$1')),
                    fromDate = this.up('window').getComponent('returnConfirmContainer').getComponent('fromdate').value;
                if ( fromDate > receiveDate ) {
                    result = 'Дата возврата не может быть раньше даты передачи'
                }
                return result;
            }
        },{
            itemId: 'printAct',
            // labelWidth: 250,
            labelAlign: 'top',
            xtype: 'checkbox',            
            boxLabel: 'Печать акта возврата'
        }]
    }],
    buttons: [{
        text: 'Подтвердить',
        action: 'save'
    },{
        text: 'Отмена',
        handler: function () { this.up('.window').close(); }
    }]
});
