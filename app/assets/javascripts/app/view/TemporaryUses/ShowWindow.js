Ext.define('EQ.view.TemporaryUses.ShowWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.showTemporaryUseWindow',
    id: 'showTemporaryUseWindow',
    title: 'Временное использование',    
    closable: true,
    closeAction: 'destroy',
    modal: true,
    width: 625,
    height: 300,
    
    initComponent: function() {
        this.items = [{
            xtype: 'form',
            margin: 20,
            id: 'showTemporaryUseWindowForm',
            // layout:'vbox',
            items: [
            {
                // xtype: 'textfield',
                width: 575,
                name: 'place',                    
                fieldLabel: 'Место ремонта',
                xtype:'combo',
                store: 'RepairPlacesList',
                displayField: 'place',                    
                valueField: 'place',
                queryMode: 'local',
                editable: true
            },{
                xtype: 'textarea',
                width: 575,
                height: 50,
                name: 'description',
                fieldLabel: 'Примечание' 
            },
            {
                xtype: 'panel',
                border: false,
                layout:'hbox',
                itemId: 'hgroupFieldsPanel',
                items: [
                {
                    xtype: 'panel',
                    width: 330,
                    border: false,
                    layout:'vbox',
                    items: [                                
                        {
                            xtype: 'datefield',
                            labelWidth: 180,
                            width: 290,
                            submitFormat: 'Y-m-d',
                            format: 'd.m.Y',
                            name: 'sent',
                            fieldLabel: 'Дата отправки на ремонт' 
                        },
                        {
                            xtype: 'numberfield',
                            labelWidth: 180,
                            width: 290,
                            maxValue: 365,
                            minValue: 0,
                            name: 'control_days',
                            fieldLabel: 'Количество дней для контроля' 
                        },{
                            xtype: 'datefield',
                            labelWidth: 180,
                            width: 290,
                            submitFormat: 'Y-m-d',
                            format: 'd.m.Y',
                            name: 'receive',
                            fieldLabel: 'Дата получения из ремонта' 
                        }]
                },{
                    xtype: 'panel',                
                    border: false,
                    layout:'vbox',
                    itemId: 'vgroup2FieldsPanel',
                    items: [
                    {
                        xtype: 'textfield',
                        name: 'item_title',
                        itemId: 'item_title',
                        labelWidth: 80,
                        readOnly: true,
                        fieldLabel: 'Элемент' 
                    },{
                        xtype: 'textfield',
                        name: 'item_serial',
                        readOnly: true,
                        labelWidth: 80,
                        fieldLabel: 'Серийный номер' 
                    },{
                        xtype: 'checkbox',
                        name: 'archive',
                        labelWidth: 80,
                        fieldLabel: 'Архивный' 
                    },{
                        xtype: 'hidden',
                        name: 'item_id',
                        itemId: 'item_id'
                    },{
                        xtype: 'hidden',
                        name: 'gridId',
                        itemId: 'gridId'
                    }
                    ]
                }]
        }]
    }];
        this.buttons = [{
            text: 'Сохранить',
            action: 'save',
            id: 'saveButton1'
        },{
            text: 'Отмена',
            scope: this,
            handler: this.close
        }];

        this.callParent(arguments);
    },

    listeners: {
        // beforerender: function ( ) {
        //     // console.log(1);
        //     // canEditrowEditingItems = false;
        //     // var bbar = Ext.getCmp('itemsGridBbar');
        //     // bbar.hide();
        //     // grid.getBottomToolbar().hide();
        // }
    }
});
