Ext.define('EQ.view.BundlesMain', {
    extend: 'Ext.container.Container',
    alias : 'widget.bundlesMain',
    requires:[
        'EQ.view.BundlesContainer'
        ],
    flex:1,
    // height: 700,
    padding: 0,
    // layout:'border',
    items: [{
        xtype: 'bundlesContainer'
    }],
    renderTo: 'bundlesContainer1'
});