var canEditrowEditingItemsBundleChecks = true;
var rowEditingItemsBundleChecks = Ext.create('Ext.grid.plugin.RowEditing', {
    clicksToMoveEditor: 2,
    autoCancel: false,
    listeners: {
        beforeedit: function () {
            // console.log('canEditrowEditingItemsBundleChecks: ' + canEditrowEditingItemsBundleChecks);
            return canEditrowEditingItemsBundleChecks;
        }
    }
});

Ext.define('EQ.view.BundleChecks.Grid', {
    store: 'BundleChecks',
    extend: 'Ext.grid.Panel',
    alias : 'widget.bundleChecksGrid',    
    id: 'bundleChecksGrid',
    title: 'Проверка оборудования',
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
	height: 590,
    border: true,
    columns: [{
        header: 'Дата проверки',
        dataIndex: 'created_at',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width:120,
        xtype: 'datecolumn',
        editor: {
            xtype: 'datefield',
            allowBlank: false
            // ,format: 'm/d/Y'
        }
    },{
        header: 'Комплект',
        dataIndex: 'bundle_title',
        width:110
    }, {
        header: 'Ф. И. О. пользователя',
        dataIndex: 'external_user_fio',
        flex:1
    }, {
        header: 'Примечание',
        dataIndex: 'note',
        flex:1,
        editor: 'textfield'
    }, {
        header: 'Проверил',
        dataIndex: 'username',
        width:200
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
            return ('<b>Всего: ' + value + '</b>');
        }
    }],
    bbar: {
        id: 'bundleChecksGridBbar',
        items: [
            {
            itemId: 'deleteButton',
            iconCls: 'icon-delete',
            tooltip: 'Удалить',
            disabled: true,
            handler: function () {
                rowEditingItemsOutsideBundle.cancelEdit();
                Ext.MessageBox.confirm('Подтверждение', 'Удалить?', showResult);
                function showResult(btn) {
                    if (btn == 'yes') {
                        var selectedRecord = Ext.getCmp('bundleChecksGrid').getSelectionModel().getSelection();
                        var store = Ext.data.StoreManager.get("BundleChecks");
                        store.remove(selectedRecord);
                    }
                }
            }
            },
            { xtype: 'tbfill' },
            {
                iconCls: 'icon-xls',
                itemId: 'exportBundleChecksButton',
                tooltip: 'Экспортировать список в Excel',
                handler: function () {
                    window.open('/reports/exportbundlechecks')
                }
            }
        ]
    },
    listeners: {
        selectionchange: function (selectionModel, selected, eOpts) {
            this.getComponent('bundleChecksGridBbar').getComponent('deleteButton').setDisabled(!(selectionModel.hasSelection()));
        }

    },
    initComponent: function () {
        this.callParent(arguments);
    },
    plugins: [rowEditingItemsBundleChecks
        , {
            ptype: 'bufferedrenderer',
            trailingBufferZone: 20, // Keep 20 rows rendered in the table behind scroll
            leadingBufferZone: 50 // Keep 50 rows rendered in the table ahead of scroll
        }]
});
