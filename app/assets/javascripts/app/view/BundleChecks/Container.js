Ext.define('EQ.view.BundleChecks.Container', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.bundleChecksContainer',
    id: 'bundleChecksContainer',
    requires:[
        'EQ.view.BundleChecks.Grid'
        ],
    flex:1,
    constructor: function() {
        this.height = $(window).height() - 115;
        this.callParent(arguments);
    },
    padding: 0,    
    // plugins: ,
    // layout:'column',
    items: [
        {
            xtype: 'textfield',
            id: 'bundleChecksSearchField',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            tooltip: 'tooltip2',
            width: 300,
            enableKeyEvents: true,
            anchor : '95%',            
            inputAttrTpl: " data-qtip='Поиск по пользователю, комплекту, примечанию и проверяющему' ",
            listeners : {   
                change: {
                    fn: function (txt, newValue, oldValue) {
                        var grid = Ext.getCmp('bundleChecksGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                            grid.store.filter({
                                filterFn: function(record) {
                                    return matcher.test(record.get('username')) ||
                                        matcher.test(record.get('bundle_title')) ||
                                        matcher.test(record.get('note')) ||
                                        matcher.test(record.get('external_user_fio'));
                                }
                            });
                        };
                    },
                    buffer: 1000
                }
            }
        },
        {
            xtype: 'bundleChecksGrid'            
        }, {
            //  контейнер для кнопок
            xtype: 'container',
            margin: '10 0 0 0'

            
        }],
    listeners : {
        beforerender: function ( container, eOpts ) {
            var grid = Ext.getCmp('bundleChecksGrid');
            grid.height = this.height - 70;
            var store = Ext.data.StoreManager.get("BundleChecks");
            store.load();
        }
    },
    renderTo: 'bundleChecks'
});