// Ext.create('Ext.panel.Panel', {
//     title: 'Hello',
//     width: 200,
//     html: '<p>World!</p>',
//     renderTo: 'itemsblock'
// });
Ext.define('EQ.view.TestItems', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.testitemslist',
// Ext.create('Ext.grid.Panel', {
        title: 'Результаты поиска',
// layout: {
    // type: 'vbox',
    // align: 'left'
// },
        height: 400,
        // width: '100%',
        store: 'TestItems',
        selType: 'rowmodel',
        layout:'fit',
        plugins: [
            Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToEdit: 2
            })
        ],
        features: [{
            id: 'group',
            ftype: 'groupingsummary',
            groupHeaderTpl: '{name}',
            hideGroupedHeader: true,
            enableGroupingMenu: false
            ,startCollapsed: true
        }],
        columns: [{
            header: 'Наименование',
            dataIndex: 'title',
            editor: 'textfield',
            flex:1
        }, {
            header: 'Количество',
            dataIndex: 'amount',
            flex:1
        }, {
            header: 'Стоимость',
            dataIndex: 'cost',
            flex:1
        }, {
            header: 'Серийный номер',
            dataIndex: 'main_serial',
            flex:1
        }]
        , renderTo: 'itemsblock'
        , initComponent: function() {
            this.addUserButton = new Ext.Button({
              text: 'Add TestItem',
              action: 'addUser'
            });
            this.editUserButton = new Ext.Button({
              text: 'Edit TestItem',
              action: 'editUser',
              disabled: false
            });
            this.deleteUserButton = new Ext.Button({
              text: 'Delete TestItem',
              action: 'deleteUser',
              disabled: false
            });
            this.bbar = [this.addUserButton, this.editUserButton, this.deleteUserButton]; 

            this.callParent(arguments);
        },
        getSelectedTestItem: function() {
            return this.getSelectionModel().getSelection()[0];
          },

          enableRecordButtons: function() {
            this.editUserButton.enable();
            this.deleteUserButton.enable();
          },

          disableRecordButtons: function() {
            this.editUserButton.disable();
            this.deleteUserButton.disable();
          }
    });
