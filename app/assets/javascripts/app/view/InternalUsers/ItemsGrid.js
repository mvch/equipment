Ext.define('EQ.view.InternalUsers.ItemsGrid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.IUItemsGrid',    
    id: 'IUItemsGrid',
    title: 'Оборудование в кабинете',
    store: 'IUItems',
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
	// height: 600,
    features: [{
            id: 'group',
            ftype: 'grouping',
            groupHeaderTpl: '{name}',
            hideGroupedHeader: true,
            enableGroupingMenu: false,
            startCollapsed: true
        }],
    columns: [
     
    {
        header: 'Наименование',
        dataIndex: 'title',
        flex:1,
        editor: {
            xtype:'combo',
            store: 'ItemTitles',
            displayField: 'title',                    
            valueField: 'title',
            queryMode: 'local',
            editable: true
        },
        renderer: function (value, meta) {
           meta.attr = 'style="white-space:normal"';
           return value;
           }  
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                return ('<b>Всего: ' + value + '</b>');
        }
    },
    // {
    //     header: 'Комплект',
    //     dataIndex: 'bundle',
    //     sortable: true
    //     // editor: 'textfield',
    //     // group: 0,
    //     // flex:1
    // }, 

    {
        header: 'Серийный<br/>номер',
        dataIndex: 'main_serial',
        editor: 'textfield',
        flex:1
    },
    // {
    //     header: 'Дополнительный<br/> серийный номер',
    //     dataIndex: 'additional_serial',
    //     editor: 'textfield',
    //     flex:1
    // },
    {
        header: 'Инвентарный<br/>номер',
        dataIndex: 'inventory_number',
        editor: 'textfield',
        flex:1
    },{
        header: 'Кол-во',
        dataIndex: 'amount',
        align: 'right',
        editor: {
                xtype: 'numberfield',
                allowBlank: true,
                // minValue: 1
            },
        width: 70
    },{
        header: 'Стоимость',
        dataIndex: 'cost',
        align: 'right',
		// renderer: Ext.util.Format.Money, //!!!!
        renderer: function(value) {
            var result = Ext.util.Format.number(value, '0,000.00');
            return result.replace('.',' ');
        },
        // format: '0.00/i',
        editor: {
                xtype: 'numberfield',
                allowBlank: true,
				decimalSeparator: ',',
                minValue: 0
            }
        ,summaryType: 'sum'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                var result = Ext.util.Format.number(value, '0,000.00');
                return ('<b>' + result.replace('.',' ') + '</b>');
        },
        width: 90
    },{
        header: 'Приме-<br/>чание',
        dataIndex: 'note',
        editor: 'textfield',
        width: 80
    }
    // , {
    //     header: 'id',
    //     dataIndex: 'id',
    //     hidden: true,
    //     flex:1
    // }
    // , {
    //     header: 'bundle_id',
    //     dataIndex: 'bundle_id',
    //     flex:1
    // }
    ],
    bbar: {
        id: 'gridBbar',
        items: [
        {
            iconCls: 'icon-history',
            tooltip: 'История',
            disabled: true,
            itemId: 'historyButton',
            action: 'itemHistoryFromInternalUsers'
        }    
    ]},
    initComponent: function() {
        // console.log(this.up('bundlesContainer').height);
        // var browserHeight = Ext.lib.Dom.getViewHeight();
        // console.log($(window).height());
        // this.height = $(window).height() - 135;
        this.callParent(arguments);
    }
});