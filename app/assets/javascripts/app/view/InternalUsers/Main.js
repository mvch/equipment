var searchValue = "";
Ext.define('EQ.view.InternalUsers.Main', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.internalUsersMain',
    id: 'internalUsersMain',
    requires:[
        'EQ.view.InternalUsers.Grid'        
        ],
    constructor: function() {
        this.height = $(window).height() - 115;
        this.callParent(arguments);
        //return this;
    },
    flex:1,
    // height: 680,
    padding: 0,
    layout: 'border',
    items: [{
        xtype: 'panel',
        height: 30,
        border: false,
        region: 'north',
        items: [{
            xtype: 'textfield',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            width: 223,
            enableKeyEvents: true,
            anchor : '95%',
            value: '',
            id: 'searchInternalUsersTextField',
            listeners : {
                change: {
                    fn: function (txt, newValue, oldValue) {
                        var grid = Ext.getCmp('internalUsersGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            if (txt.value.indexOf('#') == 0) // поиск по идентификатору
                            {
                                var matcher = new RegExp('^' + Ext.String.escapeRegex(txt.value.substring(1)) + '$', "i");
                                grid.store.filter({
                                    filterFn: function (record) {
                                        return matcher.test(record.get('id'));
                                    }
                                });
                                grid.getView().select(0);
                            }
                            else // поиск по тексту
                            {
                                var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                                grid.store.filter({
                                    filterFn: function(record) {
                                        return matcher.test(record.get('room')) || 
                                                matcher.test(record.get('place')) ||
                                                matcher.test(record.get('note'));
                                    }
                                });
                            };
                        }
                    },
                    buffer: 1000
                }
            }

        }]
    },
    {
        xtype: 'panel',
        region: 'center',
        layout: 'column',
        // height: 600,
        margin: 0,
        border: false,
        items: [
        {
            xtype: 'internalUsersGrid',
            border: true,
            width: 223
        },
        {
            xtype: 'panel',
            html: '&nbsp;',
            width: 10
        },
        {
            xtype: 'IUItemsGrid',
            // title: 'Элементы',
            border: true,
            // height: 600,
            columnWidth:.99
        }],

    },{
        xtype: 'panel',
        itemId: 'buttonsPanel',
        region: 'south',
        layout: 'column',
        items: [
        {
            xtype: 'panel',
            // width: 225,
            items: [
            {
                xtype: 'panel',
                html: '&nbsp;',
                width: 223
            //     xtype: 'button',
            //     id: 'selectEUtoSetBundleButton',
            //     scale: 'medium',                
            //     text: 'Передать пользователю',
            //     disabled: true,
            //     handler: function() {
            //         if (Ext.getCmp('bundlesGrid').getSelectionModel().hasSelection())
            //         {
            //             var store = Ext.getStore('ExternalUsersSelect');
            //             store.load();
            //             var bundle = Ext.getCmp('bundlesGrid').getSelectionModel().getSelection()[0].data;
            //             var win = Ext.widget('selectEUWindow');
            //             var bundleTitle = win.getComponent('selectEUContainer').getComponent('bundleTitle');
            //             bundleTitle.text = bundle.title;
            //             win.show();
            //         } else {
            //             Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите комплект для передачи');
            //         }
            //     }
            }]

        },{
            xtype: 'panel',
            html: '&nbsp;',
            width: 10
        },{
            xtype: 'panel',
            items: [
            {
                xtype: 'button',
                scale: 'medium',
                margin: '0 0 0 10',
                id: 'moveToStockFromIUButton',
                disabled: true,
                text: 'Переместить на склад',
                action: 'moveToStock'
            }
            // ,{
            //     xtype: 'button',
            //     scale: 'medium',
            //     margin: '0 0 0 10',
            //     id: 'moveOnRepairButton',
            //     disabled: true,
            //     text: 'На ремонт'
            // },{
            //     xtype: 'button',
            //     scale: 'medium',
            //     margin: '0 0 0 10',
            //     id: 'writeOffButton',
            //     text: 'Списать',
            //     disabled: true
            // },{
            //     xtype: 'button',
            //     scale: 'medium',
            //     margin: '0 0 0 10',
            //     id: 'moveToBundleButton',
            //     disabled: true,
            //     text: 'Переместить в комплект'
            // }
            // ,{
            //     xtype: 'button',
            //     scale: 'medium',
            //     margin: '0 0 0 10',
            //     id: 'itemCheckButton',
            //     disabled: true,
            //     text: 'Оборудование проверено'
            // }
            ]
        }
        ]
    }
    ],
    listeners : {
        afterlayout: function() {
            function getUrlVar(key){
                var result = new RegExp(key + "=([^&]*)", "i").exec(decodeURIComponent(window.location.search)); 
                return result && unescape(result[1]) || ""; 
            }
            var searchField = Ext.getCmp('searchInternalUsersTextField');            
            if (getUrlVar('id') != "")
                searchField.setValue('#' + getUrlVar('id'));
            // searchField.setValue(getUrlVar('b'));

        },
        beforerender: function ( container, eOpts ) {
            // установка высоты таблиц
            var internalUsers = Ext.getCmp('internalUsersGrid');
            internalUsers.height = this.height - 70;
            var IUItemsGrid = Ext.getCmp('IUItemsGrid');
            IUItemsGrid.height = this.height - 70;

            // загрузка данных
            var store = Ext.data.StoreManager.get("InternalUsers");
            store.load();            
        }
    },
    renderTo: 'internalUsers'
});
