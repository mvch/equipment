Ext.define('EQ.view.InternalUsers.SelectWindow', {
    extend: 'Ext.window.Window',
    alias : 'widget.selectIUWindow',
    requires:[
    	// 'EQ.view.Items',
     //    'EQ.view.Bundles',
        'EQ.view.InternalUsers.Grid'
        ],
    title: 'Выбор кабинета',
    header: {
        titlePosition: 2,
        titleAlign: 'left'
    },
	//store: ['EQ.store.Bundles'],
    closable: true,
    closeAction: 'destroy',
    autoShow: false,
    floatable: false,
    modal: true,
    width: 530,
    height: 640,
    items: [{
		xtype: 'container',
		itemId: 'selectIUContainer',
		height: 40,
		margin: '10 10 0 10',
		items : [{
			xtype:'label',	
			componentCls: 'labelHeader',
            text: 'Выберите кабинет для установки оборудования:',
			margin: '0 10 0 0'
		}, {
			xtype:'label',	
			componentCls: 'labelHeader strongText',
			text: ' ',
			itemId: 'selectedTitle'
		},{
            xtype: 'hidden',
            name: 'selectedId',
            itemId: 'selectedId'
        },{
            xtype: 'hidden',
            name: 'gridId',
            itemId: 'gridId'
        }
		]
	}, {
        xtype: 'internalUsersGrid',
        // filter: 'atWarehouse',
        margin: '10 10 10 10',
        border: 1,
        height: 480
    },{
        xtype: 'button',
        scale: 'medium',
        margin: '5 0 0 10',        
        action: 'save',
        text: 'Подтвердить'
        // ,
        // handler: function() {
        //     if (Ext.getCmp('bundlesGrid').getSelectionModel().hasSelection())
        //     {
        //         // обновляем у элемента ссылку на комплект
        //         var bundle = Ext.getCmp('bundlesGrid').getSelectionModel().getSelection()[0].data;
        //         var store = Ext.getCmp('externalUsersGrid').getStore();
        //         var currentUserId = Ext.getCmp('externalUsersGrid').getSelectionModel().getSelection()[0].data.id;
        //         var currentUser = store.getById(currentUserId);
        //         currentUser.set({ bundle_id: bundle.id, bundle_title:bundle.title });                
        //         // обновляем у комплекта ссылку на  пользователя
        //         var bundleRecord = Ext.data.StoreManager.get("Bundles").getById(bundle.id);
        //         bundleRecord.set({ external_users_id: currentUserId, location_id: 3 });

        //         this.up('.window').close();
        //     } else {
        //         Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите комплект для передачи');
        //     }

        // }
    }
    ],
    listeners: {
        // beforerender: function ( ) {
            // canEditrowEditingItems = true;
            // var grid = Ext.getCmp('bundlesGrid');            
            // grid.height = 450;
            // grid = Ext.getCmp('itemsGrid');            
            // grid.height = 450;
        // }
    }

});