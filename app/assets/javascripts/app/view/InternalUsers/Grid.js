var canEdit = true;
var rowEditingIU = Ext.create('Ext.grid.plugin.RowEditing', {
    clicksToEdit: 2,
    clicksToMoveEditor: 2
        // listeners: {
        //     beforeedit: function() {
        //         return canEdit;
        //     }
        // }

});
var filters = {
    ftype: 'filters',
    encode: true,
    local: true
};

Ext.define('EQ.view.InternalUsers.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.internalUsersGrid',
    id: 'internalUsersGrid',
    title: 'Внутреннее использование',
    store: 'InternalUsers',
    // requires: [ 'EQ.view.InternalUsers.ShowItemsWindow' ],
    // selType : 'cellmodel',

    features: [
        filters,
        {
            ftype: 'summary',
            dock: 'bottom'
        }
    ],
    columnLines: true,
    columns: [{
        header: 'Кабинет',
        dataIndex: 'room',
        editor: 'textfield',
        filter: {
            type: 'string'
        },
        width: 70,
        summaryType: 'count',
        summaryRenderer: function(value, summaryData, dataIndex) {
            return ('<b>Всего: ' + value + '</b>');
        }
    }, {
        header: 'Стол',
        dataIndex: 'place',
        editor: 'textfield',
        width: 65
    }, {
        header: 'Примечание',
        dataIndex: 'note',
        editor: 'textfield',
        flex: 1
    }],
    bbar: {
        id: 'internalUsersGridBbar',
        itemId: 'bbar',
        items: [{
            iconCls: 'icon-add',
            itemId: 'addButton',
            tooltip: 'Добавить',
            handler: function() {
                rowEditingIU.cancelEdit();
                var r = Ext.create('EQ.model.InternalUser', {
                    room: '',
                    place: '',
                    note: ''
                });
                var store = Ext.data.StoreManager.get("InternalUsers");
                store.add(r);

                store.load({
                    callback: function() {
                        var newRecord = store.last();
                        var sm = Ext.ComponentQuery.query('internalUsersGrid')[0].getSelectionModel();
                        sm.select(newRecord, true);
                        canEdit = true;
                        rowEditingIU.startEdit(newRecord, 0);
                        canEdit = false;
                    }
                });

            }
        }, {
            iconCls: 'icon-edit',
            itemId: 'editButton',
            tooltip: 'Редактировать',
            handler: function() {
                rowEditingIU.cancelEdit();
                canEdit = true;
                var selectedRecord = Ext.ComponentQuery.query('internalUsersGrid')[0].getSelectionModel().getSelection()[0];
                rowEditingIU.startEdit(selectedRecord, 0);
                canEdit = false;
            },
            disabled: true
        }, {
            iconCls: 'icon-delete',
            itemId: 'deleteButton',
            tooltip: 'Удалить',
            handler: function() {
                rowEditingIU.cancelEdit();
                Ext.MessageBox.confirm('Подтверждение', 'Удалить?', showResult);

                function showResult(btn) {
                    if (btn == 'yes') {
                        var selectedRecord = Ext.ComponentQuery.query('internalUsersGrid')[0].getSelectionModel().getSelection();
                        var store = Ext.ComponentQuery.query('internalUsersGrid')[0].getStore();
                        store.remove(selectedRecord);
                    }
                }
            },
            disabled: true
        }, {
            iconCls: 'icon-print',
            tooltip: 'Печать',
            disabled: true,
            itemId: 'printIUButton',
            action: 'printIUButton'
        }]
    },
    plugins: [rowEditingIU]
});