Ext.define('EQ.view.ItemHistory.Window', {
    extend: 'Ext.window.Window',
    alias : 'widget.itemHistoryWindow',
    requires:[
        'EQ.view.ItemHistory.Grid'
        ],
    title: 'Просмотр истории',
    closable: true,
    closeAction: 'destroy',
    autoShow: false,
    floatable: false,
    modal: true,
    width: 800,
    height: 550,
    items: [{
		xtype: 'container',
		itemId: 'hwContainer',
		height: 40,
		margin: '10 10 0 10',
		items : [{
			xtype:'label',	
			componentCls: 'labelHeader',
			text: 'История изменения комплекта:',
			margin: '10 10 0 10'
		}, {
			xtype:'label',	
			componentCls: 'labelHeader strongText',
			text: 'title',			
			itemId: 'title'
		}]
	},
	{
        xtype: 'itemHistoryGrid',
        height: 450

    }]
});