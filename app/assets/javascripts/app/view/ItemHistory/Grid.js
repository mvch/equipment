Ext.define('EQ.view.ItemHistory.Grid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.itemHistoryGrid',
    id: 'itemHistoryGrid',
    store: 'ItemHistories',
    columns: [{
        header: 'Событие',
        dataIndex: 'event',
        width:120
    }, 
	{
        header: 'После события',
        dataIndex: 'after',
        flex:1
    }, {
        header: 'Пользователь',
        dataIndex: 'who',
        width:110
    }, {
        header: 'Дата',
        dataIndex: 'when',
        width:150,
        renderer : Ext.util.Format.dateRenderer('d.m.Y H:i:s')
    }],
    bbar: [{
            iconCls: 'icon-history',
            tooltip: 'Информация о внешнем пользователе',
            action: 'showExternalUserInfo'
        },
        {
            // iconCls: 'icon-clear',
            tooltip: 'Очистить историю',
            text: 'Очистить историю',
            action: 'clearHistory'
        }]
});
