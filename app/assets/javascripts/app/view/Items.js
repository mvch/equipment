var canEditrowEditingItems = false;
var rowEditingItems = Ext.create('Ext.grid.plugin.RowEditing', {
        clicksToMoveEditor: 2,
        autoCancel: false,
        listeners: {
            beforeedit: function() {
                // console.log(canEditrowEditingItems);
                return canEditrowEditingItems;
            }
        }
    });

Ext.define('EQ.view.Items', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.itemsGrid',    
    id: 'itemsGrid',
    title: 'Элементы комплекта',
    store: 'Items',
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
	// height: 600,
    columns: [{
        header: 'Наименование',
        dataIndex: 'title',
        editor: 'textfield',
        flex:1,
        editor: {
            xtype:'combo',
            store: 'ItemTitles',
            displayField: 'title',                    
            valueField: 'title',
            queryMode: 'local',
            editable: true
        },
        renderer: function (value, meta) {
           meta.attr = 'style="white-space:normal"';
           return value;
           }  
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                return ('<b>Всего: ' + value + '</b>');
        }
    }, {
        header: 'Серийный<br/>номер',
        dataIndex: 'main_serial',
        editor: 'textfield',
        flex:1
    },
    // {
    //     header: 'Дополнительный<br/> серийный номер',
    //     dataIndex: 'additional_serial',
    //     editor: 'textfield',
    //     flex:1
    // },
    {
        header: 'Инвентарный<br/>номер',
        dataIndex: 'inventory_number',
        editor: 'textfield',
        flex:1
    },{
        header: 'Кол-во',
        dataIndex: 'amount',
        align: 'right',
        editor: {
                xtype: 'numberfield',
                allowBlank: true,
                // minValue: 1
            },
        width: 70
    },{
        header: 'Стоимость',
        dataIndex: 'cost',
        align: 'right',
		// renderer: Ext.util.Format.Money, //!!!!
        renderer: function(value) {
            var result = Ext.util.Format.number(value, '0,000.00');
            return result.replace('.',' ');
        },
        // format: '0.00/i',
        editor: {
                xtype: 'numberfield',
                allowBlank: true,
				decimalSeparator: ',',
                minValue: 0
            }
        ,summaryType: 'sum'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
                var result = Ext.util.Format.number(value, '0,000.00');
                return ('<b>' + result.replace('.',' ') + '</b>');
        },
        width: 90
    },{
        header: 'Приме-<br/>чание',
        dataIndex: 'note',
        editor: 'textfield',
        width: 80
    }
    // , {
    //     header: 'id',
    //     dataIndex: 'id',
    //     hidden: false,
    //     flex:1
    // }
    // , {
    //     header: 'bundle_id',
    //     dataIndex: 'bundle_id',
    //     flex:1
    // }
    ],
    bbar: {
        id: 'itemsGridBbar',
        items: [{
            iconCls: 'icon-add',
            disabled: false,
            itemId: 'addButton',
            tooltip: 'Добавить',
            // iconCls: 'employee-add',
            handler : function() {
                if (Ext.getCmp('bundlesGrid').getSelectionModel().hasSelection())
                {
                       rowEditingItems.cancelEdit();
                        var bundleId = Ext.ComponentQuery.query('bundlesGrid')[0].getSelectionModel().getSelection()[0].data.id;
                        // console.log('add:' + bundleId);
                        // Create a model instance
                        var r = Ext.create('EQ.model.Item', {                
                            title: '',
                            note: '',
                            amount: 1,
                            bundle_id: bundleId,
                            location_id: 6
                        });
                        var store = Ext.data.StoreManager.get("Items");
                        store.add(r);
                        var bStore = Ext.create('EQ.store.BundleItems');
                        bStore.proxy.url = '/bundleitems/' + bundleId;
                        bStore.load(function() {
                                var newData = bStore.getProxy().getReader().rawData;
                                var itemsGrid = Ext.ComponentQuery.query('itemsGrid')[0];
                                itemsGrid.getStore().loadRawData(newData);
                                var newRecord = itemsGrid.getStore().last();
                                var sm = itemsGrid.getSelectionModel();
                                sm.select(newRecord,true);
                                rowEditingItems.startEdit(newRecord,0);
                            });                
                } else {
                Ext.MessageBox.alert('Информация', 'Выберите комплект для создания элемента');
                }
            } 
        }, {
            iconCls: 'icon-delete',
            disabled: true,
            itemId: 'deleteButton',
            tooltip: 'Удалить',
            handler: function() {            
                rowEditingItems.cancelEdit();
                Ext.MessageBox.confirm('Подтверждение', 'Удалить?', showResult);
                function showResult(btn){
                    if (btn == 'yes')
                    {
                        var selectedRecord = Ext.ComponentQuery.query('itemsGrid')[0].getSelectionModel().getSelection();
                        var store = Ext.data.StoreManager.get('Items');
                        store.remove(selectedRecord);
                    }
                }
                // store.sync();
                // if (store.getCount() > 0) {
                //     sm.select(0);
                // }
            } 
        }, {
            iconCls: 'icon-history',
            tooltip: 'История',
            disabled: true,
            itemId: 'historyButton',
            action: 'itemHistory'
        }, {
            iconCls: 'icon-index-up',
            tooltip: 'Переместить вверх',
            disabled: true,
            itemId: 'indexUpButton',
            action: 'indexUp'
        }, {
            iconCls: 'icon-index-down',
            tooltip: 'Переместить вниз',
            disabled: true,
            itemId: 'indexDownButton',
            action: 'indexDown'
        }
    // ,{
    //             xtype: 'pagingtoolbar',
    //             pageSize: 6,
    //             store: 'Bundles',
    //             displayInfo: true
    //             // plugins: new Ext.ux.SlidingPager()
    //         }
    ]},
    // renderTo: 'itemsGrid',
    listeners: {            
        selectionchange: function ( selectionModel, selected, eOpts ) {
            // this.getComponent('itemsGridBbar').getComponent('addButton').setDisabled( !(selectionModel.hasSelection()) );
            this.getComponent('itemsGridBbar').getComponent('deleteButton').setDisabled( !(selectionModel.hasSelection()) );
            this.getComponent('itemsGridBbar').getComponent('historyButton').setDisabled( !(selectionModel.hasSelection()) );
            this.getComponent('itemsGridBbar').getComponent('indexUpButton').setDisabled( !(selectionModel.hasSelection()) );
            this.getComponent('itemsGridBbar').getComponent('indexDownButton').setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'moveToStockButton' ).setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'moveOnRepairButton' ).setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'writeOffButton' ).setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'moveToBundleButton' ).setDisabled( !(selectionModel.hasSelection()) );
            Ext.getCmp( 'selectIUtoSetItemButton' ).setDisabled( !(selectionModel.hasSelection()) );
            // если только комплект на складе
            var bundleLocation = Ext.getCmp('bundlesGrid').getSelectionModel().getSelection()[0].data.location_id;
            Ext.getCmp( 'selectUserForTemporaryUseItemButton' ).setDisabled( !(selectionModel.hasSelection() && ( bundleLocation == 1 )) );
            
            // Ext.getCmp( 'itemCheckButton' ).setDisabled( !(selectionModel.hasSelection()) );
        }

    },
    initComponent: function() {
        // console.log(this.up('bundlesContainer').height);
        // var browserHeight = Ext.lib.Dom.getViewHeight();
        // console.log($(window).height());
        // this.height = $(window).height() - 135;
        this.callParent(arguments);
    }, 
    plugins: [rowEditingItems]
});
