Ext.define('EQ.view.ItemChecks.Container', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.itemChecksContainer',
    id: 'itemChecksContainer',
    requires:[
        'EQ.view.ItemChecks.Grid'
        ],
    flex:1,
    height: 700,
    padding: 0,    
    // plugins: ,
    // layout:'column',
    items: [
        {
            xtype: 'textfield',
            id: 'itemChecksSearchField',
            plugins:['clearbutton'],
            fieldLabel: 'Поиск',
            labelWidth: 50,
            tooltip: 'tooltip2',
            width: 300,
            enableKeyEvents: true,
            anchor : '95%',            
            inputAttrTpl: " data-qtip='Поиск по пользователю, элементу и серийному номеру' ",
            listeners : {   
                change: {
                    fn: function (txt, newValue, oldValue) {
                        var grid = Ext.getCmp('itemChecksGrid');
                        grid.store.clearFilter();
                        if (txt.value) {
                            var matcher = new RegExp(Ext.String.escapeRegex(txt.value), "i");
                            grid.store.filter({
                                filterFn: function(record) {
                                    return matcher.test(record.get('username')) ||
                                        matcher.test(record.get('item_title')) ||
                                        matcher.test(record.get('item_serial'));
                                }
                            });
                        };
                    },
                    buffer: 1000
                }
            }
        },
        {
            xtype: 'itemChecksGrid'            
        }, {
            xtype: 'container',
            margin: '10 0 0 0',

            items:  [{
                // xtype: 'button',
                // id: 'setBundleToEUButton1',
                // scale: 'medium',                
                // text: 'Передать комплект',
                // handler: function() {
                //     console.log(this);
                //     var win = Ext.widget('showRepairWindow');
                //     win.show();
                // }
            }]
        }],
    listeners : {
        beforerender: function ( container, eOpts ) {
            // var store = Ext.data.StoreManager.get("ItemChecks");
            // store.load();
        }
    },
    renderTo: 'itemChecks'
});