Ext.define('EQ.view.ItemChecks.Grid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.itemChecksGrid',    
    id: 'itemChecksGrid',
    title: 'Проверка оборудования',
    store: 'ItemChecks',
    features: [{
        ftype: 'summary',
        dock: 'bottom'
    }],
	height: 590,
    border: true,
    columns: [{
        header: 'Дата проверки',
        dataIndex: 'created_at',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width:120
    },{
        header: 'Элемент',
        dataIndex: 'item_title',
        flex:1
    }, {
        header: 'Серийный номер',
        dataIndex: 'item_serial',
        flex:1
    }, {
        header: 'Пользователь',
        dataIndex: 'username',
        flex:1
        ,summaryType: 'count'
        ,summaryRenderer: function(value, summaryData, dataIndex) {
            return ('<b>Всего: ' + value + '</b>');
        }
    }],
    bbar: {
        id: 'repairsGridBbar',
        items: [
        // {
        //     text: 'i',
        //     tooltip: 'История',
        //     itemId: 'historyButton',
        //     disabled: true,
        //     // iconCls: 'employee-add',
        //     handler : function() {
        //         console.log(this);
        //     }
        // }
        ]
    },
    listeners: {            
        // selectionchange: function ( selectionModel, selected, eOpts ) {
        //     this.getComponent('repairsGridBbar').getComponent('historyButton').setDisabled( !(selectionModel.hasSelection()) );
            // Ext.getCmp( 'moveToStockFromWriteoffButton' ).setDisabled( !(selectionModel.hasSelection()) );
        // }
    }
});
