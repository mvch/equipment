Ext.define('EQ.model.ItemCheck', {
   extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'username',
        type: 'string'
    }, {
        name: 'created_at',
        type: 'date',
        useNull: true
    }, {
        name: 'item_id',
        type: 'int',
        useNull: true
    }, {
        name: 'item_title',
        type: 'string',
        persist: false,
        useNull: true
    }, {
        name: 'item_serial',
        type: 'string',
        persist: false,
        useNull: true
    }],
    proxy: {
            // enablePaging: true,
            type: 'rest',
            url: '/item_check',
            format: 'json',
            // extraParams: { filter : 'all' },
            reader: {
              root: 'item_check'      
            },
            writer: {
              // writeAllFields: false,
              getRecordData: function(record) {                
                return { item_check: record.data };
              }
            }
      }
});
