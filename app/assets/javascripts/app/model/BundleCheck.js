Ext.define('EQ.model.BundleCheck', {
   extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'username',
        type: 'string'
    }, {
        name: 'created_at',
        type: 'date',
        useNull: true
    }, {
        name: 'bundle_id',
        type: 'int',
        useNull: true
    }, {
        name: 'external_user_id',
        type: 'int',
        useNull: true
    }, {
        name: 'bundle_title',
        type: 'string',
        persist: false,
        useNull: true
    }, {
        name: 'note',
        type: 'string',
        persist: false,
        useNull: true
    }, {
        name: 'external_user_fio',
        type: 'string',
        persist: false,
        useNull: true
    }],
    proxy: {
            // enablePaging: true,
            type: 'rest',
            url: '/bundle_check',
            format: 'json',
            // extraParams: { filter : 'all' },
            reader: {
              root: 'bundle_check'      
            },
            writer: {
              // writeAllFields: false,
              getRecordData: function(record) {                
                return { bundle_check: record.data };
              }
            }
      }
});
