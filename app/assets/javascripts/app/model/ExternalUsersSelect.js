Ext.define('EQ.model.ExternalUsersSelect', {
   extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'fio',
        type: 'string'
    }, {
        name: 'is_teacher',
        type: 'boolean'
    }, {
        name: 'address',
        type: 'string'
    }, {
        name: 'skype',
        type: 'string'
    }, {
        name: 'vklogin',
        type: 'string'
    }],
    
    proxy: {
            type: 'rest',
            url: '/externalusers/select',
            format: 'json',
            extraParams: { type : 'all', archive: 'f' },
            reader: {
              root: 'externalusers'      
            }            
      }
});
