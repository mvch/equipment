Ext.define('EQ.model.ItemTitle', {
   extend: 'Ext.data.Model',
             
    fields: [{
        name: 'title',
        type: 'string'
    }],

    proxy: {
            type: 'rest',
            url: '/items/titles',
            format: 'json',
            reader: {
              root: 'titles'      
            }
      }
});
