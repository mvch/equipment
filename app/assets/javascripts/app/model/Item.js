Ext.define('EQ.model.Item', {
   extend: 'Ext.data.Model',
             
    // idProperty: 'id',
             
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'title',
        type: 'string'
    }, {
        name: 'main_serial',
        type: 'string'
    }, {
        name: 'additional_serial',
        type: 'string'
    }, {
        name: 'inventory_number',
        type: 'string'
    }, {
        name: 'amount',
        type: 'int'
    }, {
        name: 'cost',
        type: 'float'
    }, {
        name: 'note',
        type: 'string'
    }, {
        name: 'bundle_id',
        type: 'int',
        useNull: true
    }, {
        name: 'bundle',
        type: 'string',
        useNull: true
    }, {
        name: 'location_id',
        type: 'int',
        useNull: true
    }, {
        name: 'location',
        type: 'string',
        useNull: true
    }, {
        name: 'repair_id',
        type: 'int',
        useNull: true
    }, {
        name: 'internal_users_id',
        type: 'int'
    }],
    belongsTo: 'EQ.model.Bundle'
    ,
    proxy: {
            type: 'rest',
            url: '/items',
            format: 'json',
            // appendId: false,
            reader: {
              root: 'items'      
              // ,record: 'testitem'
            },
            writer: {
              // writeRecordId: false,
              // wrap user params for Rails                      
               // root: 'testItems'
              writeAllFields: true,
              getRecordData: function(record) {
                return { item: record.data };
              }
            }
      }
});
