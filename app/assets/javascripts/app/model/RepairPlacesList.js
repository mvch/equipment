Ext.define('EQ.model.RepairPlacesList', {
   extend: 'Ext.data.Model',
             
    fields: [{
        name: 'place',
        type: 'string'
    }],

    proxy: {
            type: 'rest',
            url: '/repair/places',
            format: 'json',
            reader: {
              root: 'places'      
            }
      }
});
