Ext.define('EQ.model.Repair', {
   extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'place',
        type: 'string'
    }, {
        name: 'sent',
        type: 'date'
        // ,useNull: true
    }, {
        name: 'receive',
        type: 'date',
        useNull: true
    }, {
        name: 'control_days',
        type: 'int'
    }, {
        name: 'description',
        type: 'string'
    }, {
        name: 'archive',
        type: 'boolean'
    }, {
        name: 'item_id',
        type: 'int',
        useNull: true
    }, {
        name: 'item_title',
        type: 'string',
        persist: false,
        useNull: true
    }, {
        name: 'item_serial',
        type: 'string',
        persist: false,
        useNull: true
    }, {
        name: 'gridId',
        type: 'string',
        useNull: true
    }],
    proxy: {
            // enablePaging: true,
            type: 'rest',
            url: '/repair',
            format: 'json',
            // extraParams: { filter : 'all' },
            reader: {
              root: 'repair'      
            },
            writer: {
              // writeAllFields: false,
              getRecordData: function(record) {                
                return { repair: record.data };
              }
            }
      }
});
