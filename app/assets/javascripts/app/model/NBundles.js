Ext.define('EQ.model.NBundles', {
    extend: 'Ext.data.Model',
    // requires:[
    //     'EQ.model.Item'//,          /* rule 2 */
    //     // 'Ext.data.HasManyAssociation',
    //     // 'Ext.data.BelongsToAssociation'
    // ],
   
fields: [
	'id',
	'title',
	'note'        
    ],
    hasMany: [{ 
      associationKey: 'items',
      model:'EQ.model.Item', 
      name: 'items' 
    }],
    proxy: {
            type: 'rest',
            url: '/bundles',
            format: 'json',
            reader: {
            	// type: 'json',
              	root: 'bundles'      
            }
      }
});