Ext.define('EQ.model.ExternalUser', {
   extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'surname',
        type: 'string'
    }, {
        name: 'patronymic',
        type: 'string'
    }, {
        name: 'name',
        type: 'string'
    }, {
        name: 'is_teacher',
        type: 'boolean'
    }, {
        name: 'address',
        type: 'string'
    }, {
        name: 'phone',
        type: 'string'
    }, {
        name: 'email',
        type: 'string'
    }, {
        name: 'skype',
        type: 'string'
    }, {
        name: 'vklogin',
        type: 'string'
    }, {
        name: 'password',
        type: 'string'
    }, {
        name: 'archive',
        type: 'boolean'
    }, {
        name: 'installdate',
        type: 'date'
    }, {
        name: 'uninstalldate',
        type: 'date'
    }, {
        name: 'bundle_id',
        type: 'int',
        useNull: true
    }, {
        name: 'bundle_title',
        type: 'string',
        useNull: true
    }, {
        name: 'provider',
        type: 'string'
    }, {
        name: 'note',
        type: 'string'
    }],
    
    proxy: {
            type: 'rest',
            url: '/externalusers',
            format: 'json',
            extraParams: { type : 'all', archive: 'f' },
            reader: {
              root: 'externalusers'      
            },
            writer: {              
              writeAllFields: true,
              getRecordData: function(record) {
                return { externalUser: record.data };
              }
            }
      }
});
