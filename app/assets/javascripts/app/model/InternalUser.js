Ext.define('EQ.model.InternalUser', {
   extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'room',
        type: 'string'
    }, {
        name: 'place',
        type: 'string'
    }, {
        name: 'note',
        type: 'string'
    }],
    
    proxy: {
            type: 'rest',
            url: '/internalusers',
            format: 'json',
            // extraParams: { type : 'all', archive: 'f' },
            reader: {
              root: 'internalusers'      
            },
            writer: {              
              writeAllFields: true,
              getRecordData: function(record) {
                return { internalUser: record.data };
              }
            }
      }
});
