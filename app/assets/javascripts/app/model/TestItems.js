Ext.define('EQ.model.TestItems', {
   extend: 'Ext.data.Model',
             
    idProperty: 'id',
             
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'title',
        type: 'string'
    }, {
        name: 'main_serial',
        type: 'string'
    }, {
        name: 'additional_serial',
        type: 'string'
    }, {
        name: 'inventory_number',
        type: 'string'
    }, {
        name: 'amount',
        type: 'int'
    }, {
        name: 'cost',
        type: 'float'
    }],
    
    proxy: {
            type: 'rest',
            url: '/test_items',
            format: 'json',
            // appendId: false,
            reader: {
              // root: 'testitems'      
              // ,record: 'testitem'
            },
            writer: {
              // writeRecordId: false,
              // wrap user params for Rails                      
               // root: 'testItems'
              writeAllFields: false,
              getRecordData: function(record) {
                return { testItem: record.data };
              }
            }
      }
});
