Ext.define('EQ.model.NamesList', {
   extend: 'Ext.data.Model',
             
    fields: [{
        name: 'name',
        type: 'string'
    }],

    proxy: {
            type: 'rest',
            url: '/externalusers/names',
            format: 'json',
            reader: {
              root: 'names'      
            }
      }
});
