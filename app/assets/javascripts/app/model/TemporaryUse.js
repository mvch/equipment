Ext.define('EQ.model.TemporaryUse', {
   extend: 'Ext.data.Model',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'from',
        type: 'date'
    }, {
        name: 'before',
        type: 'date'
    }, {
        name: 'receive',
        type: 'date',
        useNull: true
    }, {
        name: 'description',
        type: 'string'
    }, {
        name: 'item_id',
        type: 'int'
    }, {
        name: 'external_users_id',
        type: 'int'
    }, {
        name: 'item_title',
        type: 'string',
        persist: false,
        useNull: true
    }, {
        name: 'item_serial',
        type: 'string',
        persist: false,
        useNull: true
    }, {
        name: 'fio',
        type: 'string',
        persist: false,
        useNull: true
    }, {
        name: 'is_teacher',
        type: 'boolean',
        persist: false,
        useNull: true
    }, {
        name: 'archive',
        type: 'boolean',
        useNull: true
    }
    ],
    proxy: {
            // enablePaging: true,
            type: 'rest',
            url: '/temporary_use',
            format: 'json',
            extraParams: { archive: 'f' },
            reader: {
              root: 'temporaryUse'
            },
            writer: {
              // writeAllFields: false,
              getRecordData: function(record) {
                return { temporaryUse: record.data };
              }
            }
      }
});
