Ext.define('EQ.model.PatronymicsList', {
   extend: 'Ext.data.Model',
             
    fields: [{
        name: 'patronymic',
        type: 'string'
    }],

    proxy: {
            type: 'rest',
            url: '/externalusers/patronymics',
            format: 'json',
            reader: {
              root: 'patronymics'      
            }
      }
});
