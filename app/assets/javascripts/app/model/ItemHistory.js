Ext.define('EQ.model.ItemHistory', {
   extend: 'Ext.data.Model',
                          
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'event',
        type: 'string'
    }, {
        name: 'after',
        type: 'string'
    }, {
        name: 'who',
        type: 'string'
    }, {
        name: 'when',
        type: 'datetime'
    }, {
        name: 'bundle_id',
        type: 'int',
        useNull: true
    }, {
        name: 'item_id',
        type: 'int'
    }, {
        name: 'repair_id',
        type: 'int',
        useNull: true
    }],
    
    proxy: {
            type: 'rest',
            url: '/itemhistory',
            format: 'json',

            reader: {
              root: 'items'      
            }
    }
      
});
