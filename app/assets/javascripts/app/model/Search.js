Ext.define('EQ.model.Search', {
   extend: 'Ext.data.Model',
    fields: [{
        name: 'element_id',
        type: 'int'
    }, {
        name: 'category',
        type: 'int'
    }, {
        name: 'title',
        type: 'string'
    }, {
        name: 'description',
        type: 'string'
    }, {
        name: 'searchdata',
        type: 'string'
    }, {
        name: 'bundle_id',
        type: 'int'
    }
    ],
    proxy: {
            // enablePaging: true,
            type: 'rest',
            url: '/search/index',
            format: 'json',
            extraParams: { searchstr: '', archive: 'f' },
            reader: {
              root: 'search'
            }
      }
});
