Ext.define('EQ.model.BundleHistory', {
   extend: 'Ext.data.Model',
             
    idProperty: 'id',
             
    fields: [{
        name: 'id',
        type: 'int'
    }, {
        name: 'element',
        type: 'string'
    }, {
        name: 'event',
        type: 'string'
    }, {
        name: 'before',
        type: 'string'
    }, {
        name: 'after',
        type: 'string'
    }, {
        name: 'who',
        type: 'string'
    }, {
        name: 'when',
        type: 'datetime'
    },{
        name: 'external_users_id',
        type: 'int'
    }],
    
    proxy: {
            type: 'rest',
            url: '/history',
            format: 'json',

            reader: {
              root: 'items'      
            }
    }
      
});
