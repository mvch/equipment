Ext.define('EQ.model.ProvidersList', {
   extend: 'Ext.data.Model',
             
    fields: [{
        name: 'provider',
        type: 'string'
    }],

    proxy: {
            type: 'rest',
            url: '/externalusers/providers',
            format: 'json',
            reader: {
              root: 'providers'      
            }
      }
});
