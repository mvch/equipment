Ext.define('EQ.model.BundlesCount', {
   extend: 'Ext.data.Model',
    fields: [{
        name: 'warehouse',
        type: 'int'
    }, {
        name: 'externalUser',
        type: 'int'
    }, {
        name: 'internalUser',
        type: 'int'
    }],
    proxy: {
            type: 'rest',
            url: '/bundlescount/index',
            format: 'json',
            reader: {
              root: 'bundlescount'      
            }            
      }
});
