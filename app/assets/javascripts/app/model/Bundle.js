Ext.define('EQ.model.Bundle', {
   extend: 'Ext.data.Model',
   // requires:[
   //      'EQ.model.Item',          /* rule 2 */
   //      'Ext.data.HasManyAssociation',
   //      'Ext.data.BelongsToAssociation'
   //  ],
    // idProperty: 'id',
             
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'location_id',
        type: 'int'
    }, {
        name: 'title',
        type: 'string'
    }, {
        name: 'note',
        type: 'string'
    }, {
        name: 'location',
        type: 'string'
    }, {
        name: 'external_users_id',
        type: 'int'
    }, {
        name: 'internal_users_id',
        type: 'int'
    }]
    ,
    hasMany: [{ 
      associationKey: 'items',
      model:'EQ.model.Item', 
      name: 'items' 
    }],
    proxy: {
            // enablePaging: true,
            type: 'rest',
            url: '/bundles',
            format: 'json',
            extraParams: { filter : 'all' },
            reader: {
              root: 'bundles'      
            },
            writer: {
              // writeRecordId: false,
              // wrap user params for Rails                      
               // root: 'testItems'
              writeAllFields: true,
              getRecordData: function(record) {
                return { bundle: record.data };
              }
            }
      }
});
