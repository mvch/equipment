Ext.define('EQ.store.ProvidersList', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.ProvidersList',
  autoLoad: true
});