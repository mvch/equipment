Ext.define('EQ.store.BundlesList', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.Bundle',
  // pageSize: 6,
  autoLoad: false,
  autoSync: false,
  proxy: {
            // enablePaging: true,
            type: 'rest',
            url: '/bundles',
            format: 'json',
            extraParams: { filter : 'all' },
            reader: {
              root: 'bundles'      
            },
            writer: {
              // writeRecordId: false,
              // wrap user params for Rails                      
               // root: 'testItems'
              writeAllFields: true,
              getRecordData: function(record) {
                return { bundle: record.data };
              }
            }
      }
	
});