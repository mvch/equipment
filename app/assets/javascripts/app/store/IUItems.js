var IUItemsStore = Ext.define('EQ.store.IUItems', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.Item',
  autoLoad: false,
  autoSync: false, 
  groupField: 'bundle', 
    proxy: {
            type: 'rest',
            url: '/internalusers/items',
            format: 'json',
            appendId: true,
            reader: {
              root: 'items'      
              // ,record: 'testitem'
            }
            // ,
            // writer: {
            //   // writeRecordId: false,
            //   // wrap user params for Rails                      
            //    // root: 'testItems'
            //   writeAllFields: true,
            //   getRecordData: function(record) {
            //     return { item: record.data };
            //   }
            // }
          }
      
});