var itemsStore = Ext.define('EQ.store.BundleHistory', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.BundleHistory',
  autoLoad: false,
  autoSync: false,  
    proxy: {
            type: 'rest',
            url: '/bundlehistory',
            format: 'json',
            appendId: true,
            reader: {
              root: 'bundlehistory'      
              // ,record: 'testitem'
            }
          }
      
});