Ext.define('EQ.store.ItemTitles', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.ItemTitle',
  autoLoad: true
});