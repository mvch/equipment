var itemsStore = Ext.define('EQ.store.ItemHistories', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.ItemHistory',
  autoLoad: false,
  autoSync: false,  
    proxy: {
            type: 'rest',
            url: '/itemhistory',
            format: 'json',
            appendId: true,
            reader: {
              root: 'itemhistory'      
              // ,record: 'testitem'
            }
          }
      
});