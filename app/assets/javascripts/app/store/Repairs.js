Ext.define('EQ.store.Repairs', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.Repair',
  // pageSize: 6,
  autoLoad: false,
  autoSync: true
});