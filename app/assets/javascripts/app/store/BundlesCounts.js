Ext.define('EQ.store.BundlesCounts', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.BundlesCount',
  autoLoad: false,
  autoSync: false
});