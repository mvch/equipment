var itemsStore = Ext.define('EQ.store.Items', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.Item',
  
  // autoLoad: true,
  autoSync: true//,
});