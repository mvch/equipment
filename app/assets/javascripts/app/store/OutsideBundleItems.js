var itemsStore = Ext.define('EQ.store.OutsideBundleItems', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.Item',
  
  autoLoad: false,
  autoSync: true,
  proxy: {
    type: 'rest',
    url: '/items',
    format: 'json',
    extraParams: { location_type : 'outsideBundle' },
    reader: {
      root: 'items'      
    },
    writer: {
      writeAllFields: true,
      getRecordData: function(record) {
        return { item: record.data };
      }
    }
  }
});