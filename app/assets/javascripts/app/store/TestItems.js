// Models are typically used with a Store, which is basically a collection of Model instances.
Ext.define('EQ.store.TestItems', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.TestItems',
  groupField: 'amount',
  autoLoad: false,
  autoSync: false//,

  // listeners: {
  //   load: function() {
  //     console.log(arguments);
  //   },
  //   update: function() {
  //     console.log(arguments);
  //   },
  //   beforesync: function() {
  //     console.log(arguments);
  //   }
  // }
});
