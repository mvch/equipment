Ext.define('EQ.store.BundleChecks', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.BundleCheck',
  // pageSize: 6,
  autoLoad: false,
  autoSync: true
});