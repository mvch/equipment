Ext.define('EQ.store.Bundles', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.Bundle',
  // pageSize: 6,
  autoLoad: true,
  autoSync: true,
  listeners : {
    load: {
      fn: function () {
      	var store = Ext.getStore('BundlesCounts');
      	store.load(function() {
	      	var record = store.first();
	      	var label = Ext.getCmp('bundlesCountLabel');
          if ( label != null ) {
  	      	var text = "Комплектов на складе: " +  record.data.warehouse
  	      				+ "; у пользователей: " +  record.data.externalUser
  	      				+ "; у внутренних пользователей: " +  record.data.internalUser;
  	      	label.setText( text );
          }
      	});      	
      }
	}
  }
});