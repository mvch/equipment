Ext.define('EQ.store.ItemChecks', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.ItemCheck',
  // pageSize: 6,
  autoLoad: false,
  autoSync: true
});