Ext.define('EQ.store.ExternalUsersSelect', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.ExternalUsersSelect',
  storeId: 'ExternalUsersSelect',
  autoLoad: false,
  autoSync: false,
	proxy: {
	            type: 'rest',
	            url: '/externalusers/select',
	            format: 'json',
	            extraParams: { type : 'all', archive: 'f' },
	            reader: {
	              root: 'externalusers'      
	            }            
	      }

});