Ext.define('EQ.store.Searches', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.Search',
  autoLoad: false,
  autoSync: false
});