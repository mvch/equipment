Ext.define('EQ.store.ExternalUsers', {
  extend: 'Ext.data.Store',
  // pageSize: 50,
  remoteSort: false,
  model: 'EQ.model.ExternalUser',
  storeId: 'ExternalUsers',
  autoLoad: false,
  autoSync: true
});