Ext.define('EQ.store.ExternalUsersSelect', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.ExternalUsersSelect',
  storeId: 'ExternalUsersSelect',
  autoLoad: false,
  autoSync: false
});