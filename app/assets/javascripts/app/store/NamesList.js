Ext.define('EQ.store.NamesList', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.NamesList',
  autoLoad: true
});