var itemsStore = Ext.define('EQ.store.BundleItems', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.Item',
  autoLoad: false,
  autoSync: true,  
    proxy: {
            type: 'rest',
            url: '/bundleitems',
            format: 'json',
            appendId: true,
            reader: {
              root: 'items'      
              // ,record: 'testitem'
            }
            ,
            writer: {
              // writeRecordId: false,
              // wrap user params for Rails                      
               // root: 'testItems'
              writeAllFields: true,
              getRecordData: function(record) {
                return { item: record.data };
              }
            }
          }
      
});