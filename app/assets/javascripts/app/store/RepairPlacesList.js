Ext.define('EQ.store.RepairPlacesList', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.RepairPlacesList',
  autoLoad: true
});