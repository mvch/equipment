Ext.define('EQ.store.PatronymicsList', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.PatronymicsList',
  autoLoad: true
});