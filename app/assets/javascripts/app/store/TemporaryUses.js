Ext.define('EQ.store.TemporaryUses', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.TemporaryUse',
  // pageSize: 6,
  autoLoad: false,
  autoSync: true
});