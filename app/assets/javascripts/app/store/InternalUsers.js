Ext.define('EQ.store.InternalUsers', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.InternalUser',
  storeId: 'InternalUsers',
  autoLoad: false,
  autoSync: true
});