Ext.define('EQ.store.History', {
  extend: 'Ext.data.Store',

  model: 'EQ.model.BundleHistory',
  autoLoad: true,
  autoSync: false
});
