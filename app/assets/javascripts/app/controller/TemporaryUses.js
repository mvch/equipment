Ext.define('EQ.controller.TemporaryUses', {
  extend: 'Ext.app.Controller',

  stores: ['TemporaryUses'],
  models: ['TemporaryUse'],
  views: [ 'TemporaryUses.Main', 'TemporaryUses.Grid', 'TemporaryUses.ShowWindow', 
           'TemporaryUses.SelectUserWindow', 'TemporaryUses.ReturnConfirmWindow',
           'Items.OutsideBundleGrid'  ],
  // refs: [{
  //   ref: 'repairsGrid',
  //   selector: 'repairsGrid'
  // }],
  init: function() {
    this.control({
      'selectUserForTemporaryUseWindow button[action=save]': { click: this.addTemporaryUse },
      '#columnIsTeacherTU': {  beforecheckchange: this.disableEditCheckboxInGrid },
      'returnConfirmWindow button[action=save]': { click: this.returnFromTemporaryUse },
      'button[id=returnFromTUButton]': { click: this.confirmReturnFromTemporaryUse },
      'button[id=printTemporaryUse]': {  click: this.printTemporaryUse },
      'button[id=printTemporaryUseAct]': {  click: this.printTemporaryUseAct },
      
    });
  },

  // печать акта возврата из временного использования
  printTemporaryUseAct: function () {
    if (Ext.getCmp('temporaryUsesGrid').getSelectionModel().hasSelection())
    {
      var temporaryUse = Ext.getCmp('temporaryUsesGrid').getSelectionModel().getSelection()[0].data;
      window.open('/reports/temporaryuseact/' + temporaryUse.id,'_blank');                
    } else {
        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите запись');
    }
  },

  // печать договора передачи во временное использование
  printTemporaryUse: function () {
    if (Ext.getCmp('temporaryUsesGrid').getSelectionModel().hasSelection())
    {
      var temporaryUse = Ext.getCmp('temporaryUsesGrid').getSelectionModel().getSelection()[0].data;
      window.open('/reports/temporaryuse/' + temporaryUse.id,'_blank');
    } else {
        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите запись');
    }
  },


  // сохраненине возврата из временного использования 
  returnFromTemporaryUse: function( button ) { 
    var win    = button.up('window'),
        printAct = win.getComponent('dataContainer').getComponent('printAct').value,
        receiveDate = win.getComponent('dataContainer').getComponent('receivedate');
    if ( receiveDate.isValid() ) { // если дата возврата заполнена верно, то сохраняем
      var receiveDateFormatted = (receiveDate.value == null) ? null : receiveDate.value.getFullYear() + '-' + ('0' + (receiveDate.value.getMonth() + 1)).slice(-2) + '-' + ('0' + receiveDate.value.getDate()).slice(-2);
      var grid = Ext.getCmp('temporaryUsesGrid');
      var record = grid.getSelectionModel().getSelection()[0];
      // возврат элемента на склад или в комплект
        Ext.Ajax.request({
          url: '/items/returnItemFromTemporaryUse',
          async: false,
          params: { id: record.data.item_id }          
        });

      // запись в архив с датой возврата 
      record.set( { archive : 1, receive: receiveDateFormatted } );
      grid.getStore().reload();

      // печать акта возврата
      if ( printAct ) {
        this.printTemporaryUseAct();
      }

      win.close();
    }
    
    
  },

  // подтверждение возврата из временного использования 
  confirmReturnFromTemporaryUse: function( button ) { 
    if (Ext.getCmp('temporaryUsesGrid').getSelectionModel().hasSelection())
    {
      var item = Ext.getCmp('temporaryUsesGrid').getSelectionModel().getSelection()[0].data;
      var win = Ext.widget('returnConfirmWindow');
      var selectedTitle = win.getComponent('returnConfirmContainer').getComponent('selectedTitle'),
          selectedId = win.getComponent('returnConfirmContainer').getComponent('selectedId'),
          fromDate = win.getComponent('returnConfirmContainer').getComponent('fromdate');
      selectedTitle.text = item.item_title + ' (' + item.item_serial + ')';
      selectedId.value = item.item_id;
      fromDate.value = item.from;

      win.show();
    } else {
        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для возврата');
    }
  },

  // отключение редактирования столбцов с чекбоксами
  disableEditCheckboxInGrid: function () {
    return false;
  },

  // создание записи о временном использовании
  addTemporaryUse: function( button ) { 
    if (Ext.getCmp('selectWindowGrid').getSelectionModel().hasSelection())
    {        
      var win = button.up('window');
      var selectedTitle = win.getComponent('selectEUContainer').getComponent('selectedTitle').text,
          selectedId = win.getComponent('selectEUContainer').getComponent('selectedId').value,
          bundleId = win.getComponent('selectEUContainer').getComponent('bundleId').value,
          gridId = win.getComponent('selectEUContainer').getComponent('gridId').value,
          fromDate = win.getComponent('datesContainer').getComponent('fromdate'),
          beforeDate = win.getComponent('datesContainer').getComponent('beforedate'),
          descriptionText = win.getComponent('fieldsContainer').getComponent('description').value,
          printContract = win.getComponent('fieldsContainer').getComponent('printContract').value;

      if ( fromDate.isValid() && beforeDate.isValid() ) { // если даты заполнены верно, то сохраняем
        var externalUserId = Ext.getCmp('selectWindowGrid').getSelectionModel().getSelection()[0].data.id;
        var fromDateFormatted = (fromDate.value == null) ? null : fromDate.value.getFullYear() + '-' + ('0' + (fromDate.value.getMonth() + 1)).slice(-2) + '-' + ('0' + fromDate.value.getDate()).slice(-2),
            beforeDateFormatted = (beforeDate.value == null) ? null : beforeDate.value.getFullYear() + '-' + ('0' + (beforeDate.value.getMonth() + 1)).slice(-2) + '-' + ('0' + beforeDate.value.getDate()).slice(-2);
        var store = Ext.getStore('TemporaryUses');
        var record = Ext.create('EQ.model.TemporaryUse', {                
                  from: fromDateFormatted,
                  before: beforeDateFormatted,
                  item_id: selectedId,
                  external_users_id: externalUserId,
                  description: descriptionText
              });
        store.add(record);
        // редактирование расположения элемента
        Ext.Ajax.request({
          url: '/items/moveItemToTemporaryUse',
          async: false,
          params: { id: selectedId },
          success: function(response) 
            { 
              if ( gridId == 'itemsGrid' )
                { EQ.app.getController('Items').loadBundleItems( Ext.getCmp(gridId), bundleId ); }
              if ( gridId == 'outsideBundleGrid' )
                { Ext.getCmp('outsideBundleGrid').getStore().load(); }
            }
        });

        // печать договора передачи
        if ( printContract ) {
          store.load(function() {
            var temporaryUse = store.last();
            console.log(temporaryUse);
            window.open('/reports/temporaryuse/' + temporaryUse.data.id,'_blank');  
          });                
          
        }

        win.close();
      }      

    } else {
        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите пользователя для передачи');
    }
  }
})