Ext.define('EQ.controller.ItemHistories', {
  extend: 'Ext.app.Controller',

  stores: ['ItemHistories'],
  models: ['ItemHistory'],

  views: ['ItemHistory.Grid']

});