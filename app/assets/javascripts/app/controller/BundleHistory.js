Ext.define('EQ.controller.BundleHistory', {
  extend: 'Ext.app.Controller',

  stores: ['BundleHistory'],
  models: ['BundleHistory'],

  views: ['HistoryGrid'],

	refs: [{ ref: 'historyBundle', selector: 'historyBundle' }],

	init: function() {
	this.control({
		  'historyBundle': {
		        itemdblclick: this.showExternalUserInfo
		    },
		  'button[action=showExternalUserInfo]': {
		    click: this.showExternalUserInfoByButton
		  },
		  'button[action=clearHistory]': {
		  	click: this.clearHistoryByButton
		  }
	  });
	},
	showExternalUserInfoByButton: function(button) {
      var grid = button.up().up();
      var record = grid.getSelectionModel().getSelection()[0];
      this.showExternalUserInfo(grid,record);
	},
	clearHistoryByButton: function (button) {
		var grid = button.up().up();
		var record = grid.getSelectionModel().getSelection()[0];
		this.clearHistory(grid, record);
	},

	showExternalUserInfo: function(grid, record) {
	    if ( record.get('external_users_id') != null && record.get('external_users_id') != 0 )
	    {
	        var storeEU = Ext.create('EQ.store.ExternalUsers');
	        storeEU.proxy.url = '/externalusers/' + record.get('external_users_id');
	        storeEU.load(
	            {
	                scope: this,
	                callback: function(records, operation, success) {
	                    var win = Ext.widget('showEUWindow');
	                    win.down('form').loadRecord(storeEU.getAt(0));

	                    Ext.getCmp('saveButton').hide()
	                    var form = win.down('form').getForm();
	                    fields = form.getFields();
	                    Ext.each(fields.items, function (f) {
	                        f.readOnly = true;
	                    });
	                    win.show();
	                }
	            });
	    } else {
	        Ext.MessageBox.alert('Информация', 'C этой записью не связан пользователь');
	    }
	},

	clearHistory: function (grid, record) {
		grid.getView().select(0);
		var item =  grid.getSelectionModel().getSelection()[0];
		// console.log(item.data.item_id);
		
		Ext.MessageBox.confirm('Подтверждение', 'Очистить историю элемента?', showResult, this);

		function showResult(btn) {
			if (btn == 'yes') {
				Ext.Ajax.request({
					url: '/itemhistory/clear',
					params: {
						id: item.data.item_id
					}, 
					success: function (response) {
						var win = grid.up('window');
						win.close();
						// grid.store.reload();
					}
				});	
			} else {
				Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для перемещения в комплект');
			}		
		}
	}

});