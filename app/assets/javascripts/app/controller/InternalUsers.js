Ext.define('EQ.controller.InternalUsers', {
    extend: 'Ext.app.Controller',

    views: ['InternalUsers.Main', 'InternalUsers.Grid', 'InternalUsers.SelectWindow', 'InternalUsers.ItemsGrid'],
    stores: ['InternalUsers', 'IUItems'],
    models: ['InternalUser'],

    init: function() {
        this.control({
            'internalUsersGrid': {
                selectionchange: this.iuGridSelectionChange
            },
            'IUItemsGrid': {
                selectionchange: this.IUItemsGridSelectionChange
            },
            'selectIUWindow button[action=save]': { click: this.setBundleToInternalUserFromSelectWindow },
            'button[id=moveToStockFromIUButton]': { click: this.moveToStockFromIU },
            'button[action=itemHistoryFromInternalUsers]': { click: this.showItemHistoryFromInternalUsers },
            'button[action=printIUButton]': { click: this.printInternalUser }
        });
    },

    // печать установленного в кабинете обоурдования
    printInternalUser: function(button) {
        if (Ext.getCmp('internalUsersGrid').getSelectionModel().hasSelection()) {
            var iu = Ext.getCmp('internalUsersGrid').getSelectionModel().getSelection()[0].data;
            window.open('/reports/internaluser/' + iu.id, '_blank');
        } else {
            Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите кабинет');
        }
    },

    // показ окна истории элемента
    showItemHistoryFromInternalUsers: function(button) {
        var grid = Ext.getCmp('IUItemsGrid');
        var record = grid.getSelectionModel().getSelection()[0];
        EQ.app.getController('Items').showHistoryWindow(grid, record);
    },

    // перемещение из кабинета на склад
    moveToStockFromIU: function(button) {
        var grid = Ext.getCmp('IUItemsGrid');
        var internalUserId = Ext.getCmp('internalUsersGrid').getSelectionModel().getSelection()[0].data.id;
        if (grid.getSelectionModel().hasSelection()) {
            var needUpdateGrid = false;
            var selected = grid.getSelectionModel().getSelection()[0];
            // если комплект, то задаём вопрос
            if (selected.data.bundle_id != 0 && selected.data.bundle_id != null) {
                function showResult(button) {
                    if (button == 'yes') { // перемещаем весь комплект
                        Ext.Ajax.request({
                            url: '/items/moveBundleToStock',
                            params: { id: selected.data.bundle_id },
                            success: function(response) {
                                EQ.app.getController('InternalUsers').loadInternalUserItems(grid, internalUserId);
                            },
                        });
                    };
                    if (button == 'no') { // перемещаем только выбранный элемент
                        Ext.Ajax.request({
                            url: '/items/moveItemToStock',
                            params: { id: selected.data.id },
                            success: function(response) { EQ.app.getController('InternalUsers').loadInternalUserItems(grid, internalUserId); },
                        });
                    }
                };

                Ext.MessageBox.show({
                    title: 'Перемещение',
                    msg: 'Переместить на склад?',
                    buttons: Ext.MessageBox.YESNOCANCEL,
                    fn: showResult,
                    buttonText: {
                        yes: "Весь комплект",
                        no: "Только элемент"
                    },
                    icon: Ext.MessageBox.QUESTION
                });

            } else { // отдельный элемент перемещаем сразу
                Ext.Ajax.request({
                    url: '/items/moveItemToStock',
                    params: { id: selected.data.id },
                    success: function(response) { EQ.app.getController('InternalUsers').loadInternalUserItems(grid, internalUserId); },
                });
            }
            EQ.app.getController('InternalUsers').loadInternalUserItems(grid, internalUserId);
        } else {
            Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для перемещения на склад');
        }
    },

    setBundleToInternalUserFromSelectWindow: function(button) {
        if (Ext.getCmp('internalUsersGrid').getSelectionModel().hasSelection()) {
            var container = button.up('window').getComponent('selectIUContainer');
            var selectedId = container.getComponent('selectedId').value;
            var gridId = container.getComponent('gridId').value;
            // console.log(selectedId + '::' + gridId);

            // меняем у элемента или комплекта ссылку на расположение
            var selected = Ext.getCmp(gridId).getSelectionModel().getSelection()[0];
            var currentUserId = Ext.getCmp('internalUsersGrid').getSelectionModel().getSelection()[0].data.id;
            // обновляем таблицу
            if (gridId == 'itemsGrid') {
                selected.set({ internal_users_id: currentUserId, location_id: 4, bundle_id: null });
                EQ.app.getController('Items').loadBundleItems(Ext.getCmp(gridId), selectedId);
            }
            if (gridId == 'bundlesGrid') {
                selected.set({ internal_users_id: currentUserId, location_id: 4 });
                Ext.getCmp(gridId).getStore().load();
            }
            if (gridId == 'outsideBundleGrid') {
                selected.set({ internal_users_id: currentUserId, location_id: 4, bundle_id: null });
                Ext.getCmp(gridId).getStore().load();
            }

            button.up('.window').close();
        } else {
            Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите кабинет для установки');
        }
    },

    // обновление таблицы установленного в кабинете оборудования
    loadInternalUserItems: function(grid, internalUserId) {
        var store = Ext.create('EQ.store.IUItems');
        store.proxy.url = '/internalusers/items/' + internalUserId;
        store.load(function() {
            var newData = store.getProxy().getReader().rawData;
            grid.getStore().loadRawData(newData);
        });
    },

    // смена фокуса в таблице с оборудованием кабинета
    IUItemsGridSelectionChange: function(selectionModel, selected, eOpts) {
        var grid = Ext.getCmp('IUItemsGrid');
        grid.getComponent('gridBbar').getComponent('historyButton').setDisabled(!(selectionModel.hasSelection()));
        Ext.getCmp('moveToStockFromIUButton').setDisabled(!(selectionModel.hasSelection()));
    },

    // смена фоку са  в таблице со списком кабинетов
    iuGridSelectionChange: function(selectionModel, selected, eOpts) {
        // переключаем доступность кнопок
        var grid = Ext.getCmp('internalUsersGrid');
        grid.getComponent('bbar').getComponent('editButton').setDisabled(!(selectionModel.hasSelection()));
        grid.getComponent('bbar').getComponent('deleteButton').setDisabled(!(selectionModel.hasSelection()));
        grid.getComponent('bbar').getComponent('printIUButton').setDisabled(!(selectionModel.hasSelection()));

        // Ext.getCmp( 'moveToStockButton' ).setDisabled( !(selectionModel.hasSelection()) );

        // обновление таблицы установленного оборудования        
        if (selectionModel.hasSelection()) {
            var IUItemsgrid = Ext.getCmp('IUItemsGrid'),
                internalUserId = selectionModel.getSelection()[0].data.id;
            EQ.app.getController('InternalUsers').loadInternalUserItems(IUItemsgrid, internalUserId);
        }
    }

});