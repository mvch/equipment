Ext.define('EQ.controller.Repairs', {
  extend: 'Ext.app.Controller',

  stores: ['Repairs'],
  models: ['Repair'],
  views: [ 'Repairs.Main', 'Repairs.Grid', 'Repairs.ShowWindow' ],
  refs: [{
    ref: 'repairsGrid',
    selector: 'repairsGrid'
  }],
  init: function() {
        this.control({
            'showRepairWindow button[action=save]': {
            	click: this.saveRepair
            },
            'repairsGrid': {
            	itemdblclick: this.editRepair
            }
        });
    },

	saveRepair: function(button) {
		var win    = button.up('window'),
	        form   = win.down('form'),
	        record = form.getRecord(),
	        values = form.getValues();
	 	var store = this.getRepairsStore();
	 	// создаём/обновляем запись о ремонте
	 	console.log(values);
	 	if (record)
	 	{
	 		record.set(values);
	 	}
	 	else
	 	{
	 		var newRecord = Ext.create('EQ.model.Repair', values);
	 		store.add(newRecord);
	 	}
	 	store.load( function(){
	 		if (values.gridId != null && values.gridId != '')
	 		{
		 		// перемещаем элемент в ремонт
		 		var repairId = this.last().data.id;
		 		var itemStore = Ext.data.StoreManager.get("Items");
		 		itemStore.proxy.url = '/items/' + values.item_id;
		 		itemStore.load( function (){
		 			var item = this.first();
		 			this.proxy.url = '/items';
		 			var bundle_id = item.get('bundle_id');
		 			item.set({ location_id: 5, repair_id: repairId });
		 			if (values.gridId == 'itemsGrid')
		 			{
		 				EQ.app.getController('Items').loadBundleItems( Ext.getCmp(values.gridId), bundle_id );
		 			} else {
						Ext.getCmp(values.gridId).getStore().reload();
		 			}
		 		});
		 	}
	 	});
	    win.close();
	},

	editRepair: function (){

		var record = Ext.getCmp('repairsGrid').getSelectionModel().getSelection()[0];
		// console.log(grid);
    	var win = Ext.widget('showRepairWindow');
    	win.down('form').loadRecord(record);
    	win.show();
	}
});