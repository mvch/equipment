Ext.define('EQ.controller.ItemTitles', {
  extend: 'Ext.app.Controller',

  stores: ['ItemTitles'],
  models: ['ItemTitle']
  
});