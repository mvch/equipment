Ext.define('NumberPrompt', {
    extend: 'Ext.window.MessageBox',
    initComponent: function() {
        this.callParent();
        // console.log(this);
        // console.log(this.myExtraParams);
        var index = this.promptContainer.items.indexOf(this.textField);
        this.promptContainer.remove(this.textField);
        this.textField = this._createNumberField(this.initialConfig);
        this.promptContainer.insert(index, this.textField);
    },

    _createNumberField: function(maxV) {
        //copy paste what is being done in the initComonent to create the textfield        
        return new Ext.form.field.Number({
                        id: this.id + '-textfield',
                        anchor: '100%',
                        minValue: 1,                        
                        maxValue: maxV,
                        enableKeyEvents: true,
                        listeners: {
                            keydown: this.onPromptKey,
                            scope: this
                        }
        });
    }
});

Ext.define('EQ.controller.Items', {
  extend: 'Ext.app.Controller',
  refs: [ 
  	{ ref: 'itemsGrid', selector: 'itemsGrid' },
    { ref: 'outsideBundleGrid', selector: 'outsideBundleGrid' },
    // { ref: 'writeoffGrid', selector: 'writeoffGrid' },
  	{ ref: 'bundlesGrid', selector: 'bundlesGrid' }
  ], 
  stores: ['Items','Bundles', 'BundleItems', 'OutsideBundleItems' ],
  models: ['Item'],
  views: [ 'Items', 'BundlesContainer', 'Bundles.SelectBundleWindow', 'Repairs.ShowWindow',
           'ItemHistory.Window', 'ItemHistory.Grid', 'Items.WriteoffGrid', 'TemporaryUses.SelectUserWindow' ],
  init: function() {
  	this.control({
  	   'itemsGrid': { 
          // itemclick: function () { console.log('itemsGridclick'); } 
          validateedit: this.validateItemsGrid

        },
        'outsideBundleGrid': { 
          validateedit: this.validateItemsGrid
        },
       'button[id=moveToStockButton]': {	click: this.moveToStock },
       'button[id=moveToBundleButton]': {  click: this.moveToBundle },
       'button[id=writeOffButton]': {  click: this.writeOffFromItems },
       'button[id=writeOffFromOutsideButton]': {  click: this.writeOffFromOutside },
       'button[id=moveToStockFromWriteoffButton]': {  click: this.moveToStockFromWriteoff },
       'button[id=moveOnRepairFromOutsideButton]': {  click: this.moveOnRepairFromOutside },
       'button[id=moveOnRepairButton]': {  click: this.moveOnRepairFromBundleItems },
       'button[id=selectIUtoSetItemButton]': {  click: this.selectIUtoSetItem },
       'button[action=itemHistory]': { click: this.showHistoryWindowRun },
       'button[action=indexUp]': { click: this.moveItem },
       'button[action=indexDown]': { click: this.moveItem },
       'button[action=outsideBundleItemHistory]': { click: this.showHistoryWindowRunOutsideBundle },
       'button[action=writeOffItemHistory]': { click: this.showHistoryWindowRunWriteOff },

       'button[id=itemCheckButton]': {  click: this.itemCheck },
       'button[id=selectIUtoSetItemFromOutsideBundleButton]': {  click: this.selectIUtoSetItemFromOutsideBundle },
       'button[id=selectUserForTemporaryUseItemButton]': {  click: this.selectUserForTemporaryUseItemFromItems },
       'button[id=selectUserForTemporaryUseFromOutsideBundleButton]': {  click: this.selectUserForTemporaryUseFromOutsideBundle }
       
  	})
  },

  //
  moveItem: function (button) {
    var gridName = 'itemsGrid';
    if (Ext.getCmp(gridName).getSelectionModel().hasSelection())
      {
        var item = Ext.getCmp(gridName).getSelectionModel().getSelection()[0].data;
        var offset = 0
        if ( button.itemId == 'indexUpButton' ) {
          offset = -1;
        } else {
          offset = 1;
        }
        
        Ext.Ajax.request({
          url: '/items/changeordernumber',
          format: 'json',
          async: false,
          params: {
              item_id: item.id,
              offset: offset
          }});

        EQ.app.getController('Items').loadBundleItems(Ext.getCmp(gridName), item.bundle_id);

      }
  },

  // вызов из вне комплектов
  selectUserForTemporaryUseFromOutsideBundle: function () {
    this.selectUserForTemporaryUseItem('outsideBundleGrid');
  },

  // вызов из комплектов
  selectUserForTemporaryUseItemFromItems: function () {
    this.selectUserForTemporaryUseItem('itemsGrid');
  },

  // открытие окна выбора пользователя для передачи во временное использование элемента
  selectUserForTemporaryUseItem: function (gridName) {
    if (Ext.getCmp(gridName).getSelectionModel().hasSelection())
      {
        var item = Ext.getCmp(gridName).getSelectionModel().getSelection()[0].data;
        var store = Ext.getStore('ExternalUsersSelect');
        store.load();        
        var win = Ext.widget('selectUserForTemporaryUseWindow');
        var selectedTitle = win.getComponent('selectEUContainer').getComponent('selectedTitle'),
            selectedId = win.getComponent('selectEUContainer').getComponent('selectedId'),
            bundleId = win.getComponent('selectEUContainer').getComponent('bundleId'),
            gridId = win.getComponent('selectEUContainer').getComponent('gridId'),
            fromDate = win.getComponent('datesContainer').getComponent('fromdate');
        selectedTitle.text = item.title;
        selectedId.value = item.id;
        bundleId.value = item.bundle_id;
        gridId.value = gridName;
        var today = new Date();//,
            // todayDate = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
        fromDate.value = today;//.getDate();
        win.show();
      } else {
          Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для передачи <br /> пользователю во временное использование');
      }        
  },

  // перемещение элемента в кабинет из "вне комплекта"
  selectIUtoSetItemFromOutsideBundle: function () {
    if (Ext.getCmp('outsideBundleGrid').getSelectionModel().hasSelection())
      {
        var item = Ext.getCmp('outsideBundleGrid').getSelectionModel().getSelection()[0].data;
        var store = Ext.getStore('InternalUsers');
        store.load();        
        var win = Ext.widget('selectIUWindow');
        var selectedTitle = win.getComponent('selectIUContainer').getComponent('selectedTitle'),
            selectedId = win.getComponent('selectIUContainer').getComponent('selectedId'),
            gridId = win.getComponent('selectIUContainer').getComponent('gridId');
        selectedTitle.text = item.title;
        selectedId.value = item.bundle_id;
        gridId.value = 'outsideBundleGrid';
        win.show();
      } else {
          Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для установки в кабинет');
      }
  },

  // перемещение элемента в кабинет
  selectIUtoSetItem: function () {
    if (Ext.getCmp('itemsGrid').getSelectionModel().hasSelection())
      {
        var item = Ext.getCmp('itemsGrid').getSelectionModel().getSelection()[0].data;
        var store = Ext.getStore('InternalUsers');
        store.load();        
        var win = Ext.widget('selectIUWindow');
        var selectedTitle = win.getComponent('selectIUContainer').getComponent('selectedTitle'),
            selectedId = win.getComponent('selectIUContainer').getComponent('selectedId'),
            gridId = win.getComponent('selectIUContainer').getComponent('gridId');
        selectedTitle.text = item.title;
        selectedId.value = item.bundle_id;
        gridId.value = 'itemsGrid';
        win.show();
      } else {
          Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для установки в кабинет');
      }
  },

  // фиксация факта проверки элемента
  newItemCheck: function (gridId) {
    if ( Ext.getCmp(gridId).getSelectionModel().hasSelection() ) {

      function showResult(btn){
        if ( btn == 'yes' ) {
    
          var item = Ext.getCmp(gridId).getSelectionModel().getSelection()[0].data;
          var r = Ext.create('EQ.model.ItemCheck', { item_id: item.id });
          var store = Ext.data.StoreManager.get("ItemChecks");
          store.add(r);
        }
      }
        Ext.MessageBox.confirm('Подтверждение', 'Выбранный элемент проверен?', showResult, this);
      } else {
          Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для проверки');
      }
  },

  // вызов функции создания записи о проверке элемента в зависимости от текущей таблицы элементов (страницы)
  itemCheck: function (button) {
    switch (button.id) {
      case 'itemCheckButton': this.newItemCheck('itemsGrid'); break;
    }       
  },
 
  changeItemsGrid: function() {
    var result = false,
        store = e.grid.getStore(),
        oldValue = e.oldValues.main_serial;
    // console.log('oldValue');
    Ext.Ajax.request({
      url: '/items/checkExistsSerial',
      // method: 'get',
      format: 'json',
      params: {
          id: e.record.data.id,
          main_serial: e.newValues.main_serial
      },
      success: function(response) 
      {
        // console.log(response.responseText);
        var obj = JSON.parse(response.responseText);
        // console.log(obj.items);
        if (obj.items.length > 0) {
          editor.editor.getForm().findField('main_serial').markInvalid('Элемент с таким серийным номером уже существует');
          result = false;
          return result;
        } else {
          return true;
        }
      },
    });
    // console.log(result);
    // if ( store.find('main_serial', e.newValues.main_serial) != -1 
    //       && e.newValues.main_serial != '---------------' )  {
    //   editor.editor.getForm().findField('main_serial').markInvalid('Элемент с таким серийным номером уже существует');
    //   result = false;
    // }     
    // return result;
  },
  // проверка дубликатов при редактировании комплекта
  validateItemsGrid: function(editor, e) {
    var result = false,
        store = e.grid.getStore();
        // originalValue = e.originalValues.main_serial;
    Ext.Ajax.request({
      url: '/items/checkExistsSerial',
      format: 'json',
      async: false,
      params: {
          id: e.record.data.id,
          main_serial: e.newValues.main_serial,
          inventory_number: e.newValues.inventory_number
      },
      success: function(response) 
      {
        var obj = JSON.parse(response.responseText);        
        if (obj.items.length > 0) {          
          var existItemText = "расположение: <b>" + obj.items[0].location + "</b><br/>"
            + "комплект: <b>" + obj.items[0].bundle + "</b><br/>"
            + "наименование: <b>" + obj.items[0].title + "</b><br/>"
            + "серийный номер: <b>" + obj.items[0].main_serial + "</b><br/>"
            + "инвентарный номер: <b>" + obj.items[0].inventory_number + "</b><br/>"
            + "количество: <b>" + obj.items[0].amount + "</b><br/>"
            + "стоимость: <b>" + obj.items[0].cost + "</b><br/>"
            + "примечание: <b>" + obj.items[0].note + "</b>";
          if ( e.newValues.main_serial == obj.items[0].main_serial) {
            var msgText = "Элемент с таким серийным номером уже существует!" + "<br/>" + existItemText;
            editor.editor.getForm().findField('main_serial').markInvalid(msgText);
          }
          if ( e.newValues.inventory_number == obj.items[0].inventory_number) {
            var msgText = "Элемент с таким инвентарным номером уже существует!" + "<br/>" + existItemText;
            editor.editor.getForm().findField('inventory_number').markInvalid(msgText);
          }

          // e.record.data.main_serial = originalValue;
          // var record = store.getById(e.record.data.id);
          // console.log(record);
          // record.set({ main_serial: originalValue });
          // console.log(e);
          // rowEditingItems.startEdit(e.record,1);
          // console.log(obj.items);
          
        } else {
          result = true;
        }
      },
    });
    return result;
  },

  showHistoryWindowRunWriteOff: function() {
    // var grid = this.getWriteOffGrid();
    var grid = Ext.getCmp('writeoffGrid');
    var record = grid.getSelectionModel().getSelection()[0];
    this.showHistoryWindow(grid,record);
  },
  showHistoryWindowRunOutsideBundle: function() {
    var grid = this.getOutsideBundleGrid();
    var record = grid.getSelectionModel().getSelection()[0];
    this.showHistoryWindow(grid,record);
  },
  showHistoryWindowRun: function() {
    var grid = this.getItemsGrid();
    var record = grid.getSelectionModel().getSelection()[0];
    this.showHistoryWindow(grid,record);
  },

  showHistoryWindow: function(grid, record) {
    var bStore = Ext.create('EQ.store.ItemHistories');
        bStore.proxy.url = '/itemhistory/' + record.get('id');
        bStore.load(function() {
                var newData = bStore.getProxy().getReader().rawData;
                Ext.ComponentQuery.query('itemHistoryGrid')[0].getStore().loadRawData(newData);                                                
            });                
        
    var win = Ext.widget('itemHistoryWindow');
    var title = win.getComponent('hwContainer').getComponent('title');
    title.text = record.get('title');
    win.show();
  },

  loadBundleItems: function (grid, bundle_id) {
    // console.log('grid: '+ grid + ', bundle_id: ' + bundle_id);
      // обновляем таблицу элементов по текущему комплекту
    var bStore = Ext.create('EQ.store.BundleItems');
          bStore.proxy.url = '/bundleitems/' + bundle_id;
          bStore.load(function() {
              var newData = bStore.getProxy().getReader().rawData;
              grid.getStore().loadRawData(newData);
          });
  },

  moveToStock: function () {
  	Ext.MessageBox.confirm('Подтверждение', 'Переместить на склад?', showResult, this);
      function showResult(btn){
          if (btn == 'yes')
          {
      			var grid = this.getItemsGrid();
      			var record = grid.getSelectionModel().getSelection()[0];
      			var bundle_id = record.data.bundle_id;
      			record.set({ location_id: 1, bundle_id: 0 });

            EQ.app.getController('Items').loadBundleItems(grid, bundle_id);
  		    }
  	 }
  },

  moveToBundle: function () {
    if (Ext.getCmp('itemsGrid').getSelectionModel().hasSelection())
      {
          var item = Ext.getCmp('itemsGrid').getSelectionModel().getSelection()[0].data;
          var win = Ext.widget('selectBundleWindow');
          var bundleTitle = win.getComponent('selectBundleContainer').getComponent('bundleTitle');
          bundleTitle.text = item.title;
          win.editableGridToSelectBundleWindow = 'itemsGrid';
          win.show();
      } else {
          Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для перемещения в комплект');
      }
  },

  writeOff: function (gridName) {
    var grid = Ext.getCmp(gridName),
        record = grid.getSelectionModel().getSelection()[0],
        needUpdateGrid = false;
                  
    if (Ext.getCmp(gridName).getSelectionModel().hasSelection())
      {
        var bundle_id = record.data.bundle_id;
        if ( record.data.amount <= 1 ) {
          function showResult(btn){
              if (btn == 'yes')
              {
                // console.log(record.data.amount);                
                  record.set({ location_id: 2, bundle_id: 0 });
                  needUpdateGrid = true;
              }
            }

          Ext.MessageBox.confirm('Подтверждение', 'Списать элемент?', showResult, this);
        } else {          
            
            function showNumberPrompt(btn,text) {              
              console.log(btn);
              console.log(text);
              if (btn == 'ok') {
                if (text >= 1 && text <= record.data.amount ) { // проверка диапазона
                  console.log('спиcание');
                  if (text == record.data.amount) { //списываем всё количество элемента
                    record.set({ location_id: 2, bundle_id: 0 });
                  } else { // списываем часть количества элемента
                    Ext.Ajax.request({
                      url: '/items/writeOffPartial',                      
                      params: {
                          id: record.data.id,
                          amount: text
                      },
                      success: function(response) 
                      {
                        record.set({ amount: record.data.amount - text }); // уменьшаем количество в оставшемся элементе
                        needUpdateGrid = true;
                      },
                    });
                  }
                } else {
                  Ext.MessageBox.alert('Информация', 'Списание не произведено, неверно введено количество');
                }
              }
            }
            var msgbox = new NumberPrompt(record.data.amount);
            msgbox.prompt('Списание элеменов', 'Введите количество', showNumberPrompt, this, false, '1');
        }
      }

      // обновляем таблицу
      if ( needUpdateGrid ) {
        if ( gridName == 'itemsGrid' ) {
          EQ.app.getController('Items').loadBundleItems(grid, bundle_id);
        }
        else {
          grid.store.reload();
        }
      }
  },
  writeOffFromItems: function () {
    this.writeOff('itemsGrid');
  },

  writeOffFromOutside: function () {
    this.writeOff('outsideBundleGrid');
  },

  moveToStockFromWriteoff: function () {
    if (Ext.getCmp('writeoffGrid').getSelectionModel().hasSelection())
      {        
        function showResult(btn){
            if (btn == 'yes')
            {
              var grid = Ext.getCmp('writeoffGrid');
              var record = grid.getSelectionModel().getSelection()[0];
              record.set({ location_id: 1 });
              grid.store.reload();
            }
         }
         Ext.MessageBox.confirm('Подтверждение', 'Переместить на склад?', showResult, this);
      } else {
          Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для перемещения');
      }
    
  },
  moveOnRepair: function (gridId) {
    if (Ext.getCmp(gridId).getSelectionModel().hasSelection())
      {    
        var item = Ext.getCmp(gridId).getSelectionModel().getSelection()[0].data;
        if (item.location_id != 5)
        {
          var win = Ext.widget('showRepairWindow');
          var itemIdField = win.getComponent('showRepairWindowForm').getComponent('hgroupFieldsPanel')
                            .getComponent('vgroup2FieldsPanel').getComponent('item_id');
          var gridIdField = win.getComponent('showRepairWindowForm').getComponent('hgroupFieldsPanel')
                            .getComponent('vgroup2FieldsPanel').getComponent('gridId');
          itemIdField.setValue(item.id);
          gridIdField.setValue(gridId);
          win.show();
        } else {
          Ext.MessageBox.alert('Информация', 'Элемент уже находится на ремонте');
        }
      } else {
          Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите элемент для ремонта');
      }
    
  },

  moveOnRepairFromOutside: function () {
    this.moveOnRepair('outsideBundleGrid');
  },

  moveOnRepairFromBundleItems: function () {
    this.moveOnRepair('itemsGrid');
  }
});