Ext.define('EQ.controller.ExternalUsers', {
  extend: 'Ext.app.Controller',

  stores: [ 'ExternalUsers', 'ExternalUsersSelect' ],
  models: [ 'ExternalUser', 'ExternalUsersSelect' ],
  views:  [ 'ExternalUsers.Grid', 'ExternalUsers.ShowWindow', 
            'ExternalUsers.SelectWindow', 'ExternalUsers.SelectWindowGrid', 
            'ExternalUsers.ReceiveBundleWindow' ],

  init: function() {
        this.control({
            'externalUsersGrid': {
              itemdblclick: this.showEUWindow,
              afterrender: this.copy2Clipboard,
              selectionchange: this.manageEUButtons,
              edit: this.editEUGrid
            },
            '#columnIsTeacher': {  beforecheckchange: this.disableEditCheckboxInGrid },
            '#columnArchive': {  beforecheckchange: this.disableEditCheckboxInGrid },
            'showEUWindow button[action=save]': { click: this.saveEU },
            'showEUWindow field[itemId=surname]': { change: this.capitalizeFIO },
            'showEUWindow field[itemId=name]': { change: this.capitalizeFIO },
            'showEUWindow field[itemId=patronymic]': { change: this.capitalizeFIO },
            'selectWindowGrid button[action=add]': { click: this.addUserFromSelect },            
            'selectWindowGrid button[action=edit]': { click: this.editUserFromSelect },
            'selectWindowGrid button[action=delete]': { click: this.deleteUserFromSelect },
            'selectWindowGrid': { itemdblclick: this.editUserFromSelect },
            'selectEUWindow button[action=save]': { click: this.setBundleToUserFromSelectWindow },
            'button[id=bundleCheckButton]': {  click: this.bundleCheck },
            'button[id=receiveBundleFromEUButton]': {  click: this.receiveBundleFromEU },
            '#copyButton': {
              click: this.copy2Clipboard
            },
            '#prnContractSend': {  click: this.printContractSend },
            '#prnContractReceive': {  click: this.printContractReceiveClick },
        });
    },

  // метод сохранения данных при возврате комплекта
  receiveBundleFromEUSaveChanges: function (setArchive) {
    var currentUserId = Ext.getCmp('externalUsersGrid').getSelectionModel().getSelection()[0].data.id;
    var store = Ext.getCmp('externalUsersGrid').getStore();
    var currentUser = store.getById(currentUserId);
    // удаляем у комплекта ссылку на  пользователя
    var bundleStore = Ext.data.StoreManager.get("Bundles");
    bundleStore.load();    
    var bundleRecord = bundleStore.getById(currentUser.data.bundle_id);

    bundleRecord.set({ external_users_id: null, location_id: 1 });

    // обновляем у пользователя ссылку на комплект и дату возврата комплекта
    var today = new Date(),
        todayDate = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);

    if ( setArchive ) {
      currentUser.set({ bundle_id: null, bundle_title: null, uninstalldate: todayDate, archive: true }); 
      store.reload();
    } else {
      currentUser.set({ bundle_id: null, bundle_title: null, uninstalldate: todayDate });
    }
  },

  // инициация возврата комплекта
  receiveBundleFromEU: function () {
    Ext.MessageBox.confirm('Подтверждение', 'Вернуть комплект?<br /><br /><input id="doPrint" type="checkbox" /> Печать акта возврата<br /><input id="setArchive" type="checkbox" /> Переместить пользователя в архив', showResult);
    function showResult(btn){
      if (btn == 'yes')
        {
          if ( Ext.select('#doPrint').elements[0].checked == true ){
                EQ.app.getController('ExternalUsers').printContractReceive( true, Ext.select('#setArchive').elements[0].checked ); 
          } else {
            EQ.app.getController('ExternalUsers').receiveBundleFromEUSaveChanges( Ext.select('#setArchive').elements[0].checked ); 
          }
        }
    }
  },

  // отключение редактирования столбцов с чекбоксами
  disableEditCheckboxInGrid: function () {
    return false;
  },

  // вызов метода печати акта без сохранения
  printContractReceiveClick: function (doSave, setArchive) {
    EQ.app.getController('ExternalUsers').printContractReceive( false, false );
  },

  // метод печати акта
  printContractReceive: function (doSave, setArchive) {
    if (Ext.getCmp('externalUsersGrid').getSelectionModel().hasSelection())
    {
        var user = Ext.getCmp('externalUsersGrid').getSelectionModel().getSelection()[0].data;
        if (user.bundle_id != null && user.bundle_id != 0) {
          if (user.is_teacher == 1)
            { window.open('/reports/teacheract/' + user.id + "?bundle=" + user.bundle_id,'_blank') }
          else
            { window.open('/reports/studentact/' + user.id + "?bundle=" + user.bundle_id,'_blank') }
        }
    } else {
        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите пользователя');
    }
    if (doSave)
      EQ.app.getController('ExternalUsers').receiveBundleFromEUSaveChanges(setArchive); 
  },

  printContractSend: function () {
    if (Ext.getCmp('externalUsersGrid').getSelectionModel().hasSelection())
    {
        var user = Ext.getCmp('externalUsersGrid').getSelectionModel().getSelection()[0].data;
        if (user.bundle_id != null && user.bundle_id != 0) {
          // если дата установки не задана, ставим сегодняшнюю
          if (user.installdate == null)
            {
              var today = new Date(),
                todayDate = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
              var store = Ext.getCmp('externalUsersGrid').getStore();
              var currentUser = store.getById(user.id);
              currentUser.set({ installdate: todayDate });
            }
          if (user.is_teacher == 1)
            { window.open('/reports/teachercontract/' + user.id + "?bundle=" + user.bundle_id,'_blank') }
          else
            { window.open('/reports/studentcontract/' + user.id + "?bundle=" + user.bundle_id,'_blank') }
        }
        
    } else {
        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите пользователя');
    }
  },
  // фиксация факта проверки элемента
  bundleCheck: function () {
    var gridSelectionModel = Ext.getCmp('externalUsersGrid').getSelectionModel();
    if ( gridSelectionModel.hasSelection() ) {
      function showResultText(btn, text){        
        if ( btn == 'ok' ) {
          var externalUser = gridSelectionModel.getSelection()[0].data,
              store = Ext.data.StoreManager.get("BundleChecks"),
              bundleCheck = Ext.create('EQ.model.BundleCheck', 
                  { bundle_id: externalUser.bundle_id, 
                    external_user_id: externalUser.id,
                    note: text
                  });
          store.add(bundleCheck);
        }
      }
      Ext.MessageBox.show({
           title: 'Создание записи о проверке оборудования ',
           msg: 'Введите примечание:',
           width:400,
           buttons: Ext.MessageBox.OKCANCEL,
           multiline: true,
           fn: showResultText,
           animateTarget: 'mb3'
      });
    } else {
        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите пользователя для проверки оборудования');
    }
  },

  setBundleToUserFromSelectWindow: function(button) {    
    if (Ext.getCmp('selectWindowGrid').getSelectionModel().hasSelection())
    {
        // обновляем у элемента ссылку на комплект
        var bundle = Ext.getCmp('bundlesGrid').getSelectionModel().getSelection()[0].data;
        var store = Ext.getStore('ExternalUsers');
        var currentUserId = Ext.getCmp('selectWindowGrid').getSelectionModel().getSelection()[0].data.id;        
        store.proxy.url = '/externalusers/' + currentUserId;
        store.load(function() {
          var currentUser = store.getById(currentUserId);
          store.proxy.url = '/externalusers';
          currentUser.set({ bundle_id: bundle.id, bundle_title:bundle.title });                
          // обновляем у комплекта ссылку на  пользователя
          var bundleRecord = Ext.data.StoreManager.get("Bundles").getById(bundle.id);
          bundleRecord.set({ external_users_id: currentUserId, location_id: 3 });
          Ext.getCmp('bundlesGrid').getStore().reload();
          button.up('.window').close();
          canEdit = false;
        });
    } else {
        Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите комплект для передачи');
    }
  },

  addUserFromSelect: function() {    
    var grid = Ext.getCmp('selectWindowGrid');
    var r = Ext.create('EQ.model.ExternalUser', {
        surname: '',
        name: '',
        patronymic: ''
        // , is_teacher: isTeacher
    });
    var store = Ext.data.StoreManager.get("ExternalUsers");
    store.add(r);

    store.load({
        // params: {type : euType},
        callback: function() {                    
            var newRecord = store.last();
            EQ.app.getController('ExternalUsers').showEUWindow(grid, store.last(), 'add');
        }
    });
  },

  editUserFromSelect: function() {
    if ( Ext.getCmp('selectWindowGrid').getSelectionModel().hasSelection() ) {
      var grid = Ext.getCmp('selectWindowGrid'),
          selectedRecord = grid.getSelectionModel().getSelection()[0];
      var store = Ext.create('EQ.store.ExternalUsers');
      store.proxy.url = '/externalusers/' + selectedRecord.data.id;
      store.load(function() {
        EQ.app.getController('ExternalUsers').showEUWindow(grid, store.first(), null);        
      });
    } else {
      Ext.MessageBox.alert('Информация', 'Выберите строку для редактирования');
    }
  },

  deleteUserFromSelect: function() {
    if ( Ext.getCmp('selectWindowGrid').getSelectionModel().hasSelection() ) {
      function showResult(btn){
        if (btn == 'yes')
        {
          var grid = Ext.getCmp('selectWindowGrid'),
              selectedRecord = grid.getSelectionModel().getSelection()[0];
          var store =  Ext.getStore('ExternalUsers');
          store.proxy.url = '/externalusers/' + selectedRecord.data.id;
          store.load(function() {        
            var delStore = Ext.getStore('ExternalUsers');
            delStore.proxy.url = '/externalusers';
            delStore.remove(store.first());
            grid.getStore().reload();
          });              
        }
      };
      Ext.MessageBox.confirm('Подтверждение', 'Удалить?', showResult);
    } else {
      Ext.MessageBox.alert('Информация', 'Выберите строку для удаления');
    }
  },

copy2Clipboard: function () {
  // определяем текст для копирования
  console.log('copy2clip2');
  var selectionModel = Ext.getCmp('externalUsersGrid').getSelectionModel();
  var text = '';
  if (selectionModel.hasSelection()) {
    var record = selectionModel.getSelection()[0];
    var columnHeader = selectionModel.getCurrentPosition().columnHeader.dataIndex;
    // определяем наборы полей для проверки вхождения
    var personalInfoFields = ['surname', 'name', 'patronymic', 'bundle_title', 'provider', 'address', 'phone', 'note'];
    var netInfoFields = ['email', 'skype', 'vklogin', 'password', 'installdate', 'uninstalldate'];
    // проверяем вхождения
    if (personalInfoFields.indexOf(columnHeader) != -1) // копируем ФИО, адрес, телефон
    {
      text = 'Ф. И. О.: ' + record.get('surname') + ' ' + record.get('name') + ' ' + record.get('patronymic') +
        '\n' + 'Адрес: ' + record.get('address') +
        '\n' + 'Телефон: ' + record.get('phone');
    } else {
      if (netInfoFields.indexOf(columnHeader) != -1) { // выделен любой другой столбец, копируем информацию о логине/пароле
        text = 'E-mail: ' + record.get('email') +
          '\n' + 'Логин ВК: ' + record.get('vklogin') +
          '\n' + 'Skype: ' + record.get('skype') +
          '\n' + 'Пароль: ' + record.get('password');
      }
    }
  }
 console.log(text); 
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    // must use a temporary form element for the selection and copy
    target = document.getElementById(targetId);
    if (!target) {
      var target = document.createElement("textarea");
      target.style.position = "absolute";
      target.style.left = "-9999px";
      target.style.top = "0";
      target.id = targetId;
      document.body.appendChild(target);
    }
    target.textContent = text;    
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
      succeed = document.execCommand("copy");
    } catch (e) {
      succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
      currentFocus.focus();
    }

  // clear temporary content
  target.textContent = "";

  return succeed;
},

  // copy2Clipboard: function(grid) {
  //   ZeroClipboard.config( { moviePath: '/ZeroClipboard.swf' } );
  //   var clipboardClient = new ZeroClipboard( $(".copyButton") );    
  //   clipboardClient.on( 'load', function() {
  //           console.log( "movie is loaded" );
  //       });
  //   clipboardClient.on('dataRequested', function(client, args) {
  //     var selectionModel = grid.getSelectionModel();
  //     if ( selectionModel.hasSelection() ) 
  //     {
  //       var record = selectionModel.getSelection()[0];
  //       var text = '';
  //       var columnHeader = selectionModel.getCurrentPosition().columnHeader.dataIndex;
  //       // определяем наборы полей для проверки вхождения
  //       var personalInfoFields = ['surname', 'name', 'patronymic', 'bundle_title', 'provider', 'address', 'phone', 'note'];
  //       var netInfoFields = ['email', 'skype', 'vklogin', 'password', 'installdate', 'uninstalldate'];
  //       // проверяем вхождения
  //       if ( personalInfoFields.indexOf(columnHeader) != -1 ) // копируем ФИО, адрес, телефон
  //       {
  //         text = 'Ф. И. О.: ' + record.get('surname') + ' ' + record.get('name') + ' ' + record.get('patronymic') 
  //             + '\n' + 'Адрес: ' + record.get('address')
  //             + '\n' + 'Телефон: ' + record.get('phone');
  //       } else {
  //         if ( netInfoFields.indexOf(columnHeader) != -1 )
  //         { // выделен любой другой столбец, копируем информацию о логине/пароле
  //           text = 'E-mail: ' + record.get('email') 
  //               + '\n' + 'Логин ВК: ' + record.get('vklogin')
  //               + '\n' + 'Skype: ' + record.get('skype')
  //               + '\n' + 'Пароль: ' + record.get('password');
  //         }
  //       }            
  //       client.setText(text);
  //     }
  //   });
  // },

  showEUWindow: function(grid, record, gridEvent) {    
    var win = Ext.widget('showEUWindow');
    win.down('form').loadRecord(record);
    win.grid = grid;
    win.gridEvent = gridEvent;
    win.show();
  },

  saveEU: function(button) {
  	var win    = button.up('window'),
          form   = win.down('form'),
          record = form.getRecord(),
          values = form.getValues();    
    var store = Ext.getStore('ExternalUsers');
    store.proxy.url = '/externalusers';
    record.set(values);    
    win.grid.getStore().reload();
    if ( win.gridEvent == 'add' ) {      
      win.grid.getSelectionModel().select(store.last(),true);
    }
    win.close();
  },

  manageEUButtons: function ( selectionModel, selected, eOpts ) {
    Ext.getCmp('setBundleToEUButton').setDisabled( !(selectionModel.hasSelection()) );
    var EUGridBbar = Ext.getCmp('externalUsersGrid').getComponent('EUGridBbar');
    EUGridBbar.getComponent('editButton').setDisabled( !(selectionModel.hasSelection()) );
    EUGridBbar.getComponent('deleteButton').setDisabled( !(selectionModel.hasSelection()) );
    EUGridBbar.getComponent('copyButton').setDisabled( !(selectionModel.hasSelection()) );

    if (selected[0]){
      if ( selected[0].data.bundle_id != null && selected[0].data.bundle_id != 0)
      {
        Ext.getCmp('setBundleToEUButton').setDisabled(true);
        Ext.getCmp('bundleCheckButton').setDisabled(false);
        Ext.getCmp('receiveBundleFromEUButton').setDisabled(false);
        Ext.getCmp('printMenuButton').setDisabled(false);
        EUGridBbar.getComponent('showBundleButton').setDisabled( !(selectionModel.hasSelection()) );
        EUGridBbar.getComponent('goToBundleButton').setDisabled( !(selectionModel.hasSelection()) );

      }
    } else { 
      Ext.getCmp('setBundleToEUButton').setDisabled(true); 
      Ext.getCmp('bundleCheckButton').setDisabled(true);
      Ext.getCmp('receiveBundleFromEUButton').setDisabled(true);
      Ext.getCmp('printMenuButton').setDisabled(true);
      EUGridBbar.getComponent('showBundleButton').setDisabled( true );
      EUGridBbar.getComponent('goToBundleButton').setDisabled( true );

    }
  },

  capitalizeFIO: function(field, newValue, oldValue){
    field.setValue(Ext.String.capitalize(field.value));
  },

  editEUGrid: function(rowEditPlugin) {    
    var record = rowEditPlugin.grid.getSelectionModel().getSelection()[0];
    record.set('name', Ext.String.capitalize(record.get('name')));
    record.set('surname', Ext.String.capitalize(record.get('surname')));
    record.set('patronymic', Ext.String.capitalize(record.get('patronymic')));
  }

});