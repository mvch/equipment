Ext.define('EQ.controller.Bundles', {
  extend: 'Ext.app.Controller',
  refs: [{ ref: 'itemsGrid', selector: 'itemsGrid' }
    , { ref: 'bundlesContainer', selector: 'bundlesContainer' }
    , { ref: 'bundlesGrid', selector: 'bundlesGrid' }
  ], 
  stores: [ 'Bundles', 'BundlesCounts', 'BundlesList' ],
  models: [ 'Bundle' ],
  views:  [ 'Bundles' , 'HistoryWindow' ],

  init: function() {
    this.control({
      'bundlesGrid': {
            itemdblclick: this.showhistoryWindow,
            cellclick: this.cellclickBundlesGrid,
            selectionchange: this.selectionchangeBundlesGrid,
            validateedit: this.validateBundlesGrid,
            viewready: this.viewready

        },
      'button[action=historyBundle]': { click: this.showhistoryWindow1 },
      'button[action=printBundleButton]': { click: this.printBundle }
          //console.log('Initialized Configs Controller!');
          // this.control({            
          //     'button[action=addUser]': {
          //         click: this.addBundle
          //       },
          // });

      });
    },

    // печать карточки комплекта
    printBundle: function(button) {
      if (Ext.getCmp('bundlesGrid').getSelectionModel().hasSelection())
      {
        var bundle = Ext.getCmp('bundlesGrid').getSelectionModel().getSelection()[0].data;
        window.open('/reports/bundle/' + bundle.id,'_blank');
      } else {
          Ext.MessageBox.alert('Информация', 'Пожалуйста, выберите комплект');
      }
    },

    viewready: function(grid){
      // var store = Ext.getStore('Bundles');
      
      // grid.getSelectionModel().select();
      // console.log(grid.getSelectionModel().hasSelection());
      // grid.fireEvent('cellclick', grid, 0);
      // console.log(grid.getSelectionModel().hasSelection());
    },

    // проверка дубликатов при редактировании комплекта
    validateBundlesGrid: function(editor, e) {
      var result = true,
          store = e.grid.getStore();
      if ( store.find('title', e.newValues.title) != -1 )  {
        editor.editor.getForm().findField('title').markInvalid('Такой комплект уже существует');
        result = false;
      } 
      return result;
    },

    showhistoryWindow1: function() {
      var grid = this.getBundlesGrid();
      var record = grid.getSelectionModel().getSelection()[0];
      this.showhistoryWindow(grid,record);
    },
    showhistoryWindow: function(grid, record) {
      canEdit = false;
      rowEditingBundles.cancelEdit();
      var bStore = Ext.create('EQ.store.BundleHistory');
          bStore.proxy.url = '/bundlehistory/' + record.get('id');
          bStore.load(function() {
                  var newData = bStore.getProxy().getReader().rawData;
                  Ext.ComponentQuery.query('historyBundle')[0].getStore().loadRawData(newData);                                                
              });                
          
      var win = Ext.widget('historyWindow');
      var bundleTitle = win.getComponent('hwContainer').getComponent('bundleTitle');
      bundleTitle.text = record.get('title');
      win.show();
      canEdit = false;
    },

    // обновление списка элементов в зависимости от выбранного комплекта
    refreshBundleItems: function (currentBundle) { 
      if (viewTypeBundlesGrid != 'selectBundle') {
          canEditrowEditingItems = true;
          var store = Ext.data.StoreManager.get("ItemTitles");
          store.load();

          var bStore = Ext.create('EQ.store.BundleItems');
          bundleCurrentRecordId = currentBundle.get('id');
          bundleCurrentRecordTitle = currentBundle.get('title');
          bStore.proxy.url = '/bundleitems/' + currentBundle.get('id');
          bStore.load(function() {
                  var newData = bStore.getProxy().getReader().rawData;
                  // console.log(newData);
                  Ext.ComponentQuery.query('itemsGrid')[0].getStore().loadRawData(newData);                                                
              });                
      }
    },

  cellclickBundlesGrid: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
      this.refreshBundleItems(record);  
    },

  selectionchangeBundlesGrid: function ( selectionModel, selected, eOpts ) {
    var bbar = this.getBundlesGrid().getComponent('bundlesGridBbar');
    bbar.getComponent('historyButton').setDisabled( !(selectionModel.hasSelection()) );
    bbar.getComponent('printBundleButton').setDisabled( !(selectionModel.hasSelection()) );

    if (selected.length > 0) {
      // удалять и редактировать комплекты, находящиеся у пользователя нельзя
      if ( selected[0].data.location_id == 1 )
      {
        bbar.getComponent('editButton').setDisabled( !(selectionModel.hasSelection()) );
        bbar.getComponent('deleteButton').setDisabled( !(selectionModel.hasSelection()) );
        Ext.getCmp('selectEUtoSetBundleButton').setDisabled( !(selectionModel.hasSelection()) );
        Ext.getCmp('selectIUtoSetBundleButton').setDisabled( !(selectionModel.hasSelection()) );
        Ext.getCmp('selectIUtoSetReturnBundleButton').setDisabled( true );
        
      } else {
        bbar.getComponent('editButton').setDisabled(true);
        bbar.getComponent('deleteButton').setDisabled(true);
        Ext.getCmp('selectEUtoSetBundleButton').setDisabled(true);
        Ext.getCmp('selectIUtoSetBundleButton').setDisabled(true);
        Ext.getCmp('selectIUtoSetReturnBundleButton').setDisabled(false);
      }
    }
    // обновление элементов комплекта при смене фокуса на строке
    if ( selectionModel.hasSelection() ){ this.refreshBundleItems(selected[0]); }
  }

});