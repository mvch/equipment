Ext.define('EQ.controller.NBundles', {
  extend: 'Ext.app.Controller',

  stores: ['NBundles'],
  models: ['NBundles'],

  views: ['NBundles']
});