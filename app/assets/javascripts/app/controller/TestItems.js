Ext.define('EQ.controller.TestItems', {
  extend: 'Ext.app.Controller',

  stores: ['TestItems'],
  models: ['TestItems'],

   views: ['TestItems','form'],
   refs: [{
    ref: 'list',
    selector: 'testitemslist'
  }],
  init: function() {
    this.control({
      'testitemslist': {
        // itemdblclick: this.editUser,
        selectionchange: this.selectionChange
      },
      'testitemform button[action=save]': {
        click: this.createOrUpdateUser
      },
      'button[action=addUser]': {
        click: this.addUser
      },
      'button[action=editUser]': {
        click: this.editUser
      },
      'button[action=deleteUser]': {
        click: this.deleteUser
      }
    });
  },

  addUser: function() {
    var view = Ext.widget('testitemform');
    view.show();
  },

  editUser: function() {
    var record = this.getList().getSelectedTestItem();
    var view = Ext.widget('testitemform');
    view.down('form').loadRecord(record);
  },

  createOrUpdateUser: function(button) {    
    var win = button.up('window');
    var form = win.down('form');

    var store = this.getTestItemsStore();
    var values = form.getValues();

    var testItem = Ext.create('EQ.model.TestItems', values);
    var errors = testItem.validate();

    if (errors.isValid()) {
      var formRecord = form.getRecord();

      if (formRecord) {
        // perform update
        formRecord.set(values);
      } else {
        // perform create
        store.add(testItem);
      }

      store.sync({
        success: function() {
          win.close();
        },
        failure: function(batch, options) {
          console.log('some error');
          // extract server side validation errors
          // var serverSideValidationErrors = batch.exceptions[0].error;

          // var errors = new Ext.data.Errors();
          // for (var field in serverSideValidationErrors) {
          //   var message = serverSideValidationErrors[field].join(", ");
          //   errors.add(undefined, { field: field, message: message });
          // }
          // form.getForm().markInvalid(errors);
        }
      });
    } else {
      form.getForm().markInvalid(errors);
    }
  },

  deleteUser: function() {
    console.log(this.getList());
    // var selection = testitemslist.getView().getSelectionModel().getSelection()[0];
    // if (selection) {
    //       var store = this.getTestItemsStore();
    //       store.remove(selection);
    //       store.sync();
    //   }

    var record = this.getList().getSelectedTestItem();

    if (record) {
      var store = this.getTestItemsStore();
      store.remove(record);
      store.sync();
    }
  },
  selectionChange: function(selectionModel, selections) {
    var grid = this.getList();

    if (selections.length > 0) {
      grid.enableRecordButtons();
    } else {
      grid.disableRecordButtons();
    }
  }
  
});