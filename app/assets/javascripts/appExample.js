
Ext.require([
    'Ext.form.*'
     , 'Ext.ux.*'
]);
Ext.define('TestItems', {
           extend: 'Ext.data.Model',
                     
            idProperty: 'id',
                     
            fields: [{
                name: 'id',
                type: 'int'
            }, {
                name: 'title',
                type: 'string'
            }, {
                name: 'main_serial',
                type: 'string'
            }, {
                name: 'additional_serial',
                type: 'string'
            }, {
                name: 'inventory_number',
                type: 'string'
            }, {
                name: 'amount',
                type: 'int'
            }, {
                name: 'cost',
                type: 'float'
            }]
      });
                 
       var store = Ext.create('Ext.data.Store', {
                    model: 'TestItems',
                    autoLoad: true,
                    proxy: {
                            type: 'rest',
                            url: '/test_items/index',
                            format: 'json',

                            reader: {
                              root: 'items'      
                            }
                }
        });

Ext.onReady(function(){
     var formPanel = Ext.create('Ext.form.Panel', {
        frame: true,
        title: 'Введите параметры поиска',
        name: 'pan1',
        border: false,
        frame: false,
        title: false,
        //width: 340,
        bodyPadding: 5,

        fieldDefaults: {
            labelAlign: 'left',
            labelWidth: 70,
            anchor: '100%'
        },

        // items: [        
        // {
        //     xtype: 'searchfield',
        //     name: 'textfield1',
        //     fieldLabel: 'Поиск',
        //     store: store
        //     // value: ''
        // }
        // ,{
        //             xtype: 'button',
        //             text: 'Medium',
        //             scale: 'medium'
        //         }
        // ]  ,     
        renderTo: 'itemsblock'
    });
      // formPanel.render('colA');

 
    Ext.create('Ext.grid.Panel', {
        title: 'Результаты поиска',

        height: 400,
        // width: 100,
        store: store,
        columns: [{
            header: 'Наименование',
            dataIndex: 'title'
        }, {
            header: 'Количество',
            dataIndex: 'amount'
        }, {
            header: 'Стоимость',
            dataIndex: 'cost',
            flex:1
        }, {
            header: 'Серийный номер',
            dataIndex: 'main_serial',
            flex:1
        }]
        , renderTo: 'itemsblock'
    });
    var formPanel2 = Ext.create('Ext.form.Panel', {
    //Ext.FormPanel({ // инициализация панели с формой.
       renderTo: 'ex0', // результат поместить в контейнер с ID='ex0'
       items:[{              // массив элементов формы или их конфигураций
           xtype:'textfield',
           fieldLabel:'Фамилия И.О.',
           name:'name',
           value:'Тынгылчав И.'
       }]
   });

});
