class InternalUsers < ActiveRecord::Base
	has_many :items

	def self.getPlace(iuID)
	    iu = InternalUsers.find(iuID)
	    if (iu.place != nil)
	    	iu.room + "(" + iu.place + ")"
	    else
	    	iu.room
	    end	    
	end
end
