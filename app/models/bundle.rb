class Bundle < ActiveRecord::Base
	has_many :items
	has_many :bundle_histories
	has_many :item_histories

	belongs_to :location
	belongs_to :external_users

	after_create :createEvent
	before_destroy :deleteEvent
	after_update :logUpdateEvent, if: :title_changed?
	after_update :logExternalUserEvent, if: :external_users_id_changed? 
	after_update :logInternalUserEvent, if: :internal_users_id_changed? 
	
	

	protected
	def createEvent		
		#log event
		b = BundleHistory.newEvent(1, self)
	end

	def deleteEvent
		# очистка истории
		BundleHistory.where(bundle: self).delete_all

		# перенос элементов комплекта на склад
		Item.where(bundle_id: self.id).update_all(bundle_id: 0, location_id: 1)

	end

	def logUpdateEvent
		b = BundleHistory.newEvent(3, self)
	end

	def logExternalUserEvent
		if ( self.location_id == 1 ) # возврат на склад
			if (self.location_id_was == 3) && ( self.external_users_id_was != nil || self.external_users_id_was != 0 ) && (self.external_users_id == 0)
				b = BundleHistory.newEvent(9, self)
			end
		end

		if ( self.location_id == 3 ) # внешний пользователь
			if ( self.external_users_id_was == nil || self.external_users_id_was == 0 ) && (self.external_users_id != 0)
				b = BundleHistory.newEvent(8, self)
			else
				if ( self.external_users_id_was != nil && self.external_users_id_was != 0 )
					b = BundleHistory.newEvent(9, self)
				end
			end
		end

		if ( self.location_id == 4 ) # внутреннее использование
			Item.update_all({ :internal_users_id => self.external_users_id }, {:bundle_id => self.id})
			b = BundleHistory.newEvent(4, self)
		end
	end

	def logInternalUserEvent
		if ( self.location_id == 4 ) # внутреннее использование
			Item.update_all({ :internal_users_id => self.external_users_id }, {:bundle_id => self.id})
			b = BundleHistory.newEvent(4, self)
		end

		if ( self.location_id == 1 ) # возврат на склад
			if (self.location_id_was == 4) #&& ( self.internal_users_id_was != nil || self.internal_users_id_was != 0 ) && (self.internal_users_id == 0)
				b = BundleHistory.newEvent(9, self)
			end
		end
	end
end
