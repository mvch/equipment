class BundleCheck < ActiveRecord::Base
  belongs_to :bundle
  belongs_to :external_user

  def getBundleTitle()
	unless bundle_id.nil? or bundle_id == 0
  		bundle = Bundle.find(bundle_id)
  		bundle.title
  	else
  		""
  	end
  end

  def getExternalUserFIO()
  	unless external_user_id.nil? or external_user_id == 0
  		user = ExternalUsers.find(external_user_id)
  		user.fio
  	else
    		""
    end
  end
end
