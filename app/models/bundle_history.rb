# coding: utf-8
class BundleHistory < ActiveRecord::Base
  belongs_to :event
  belongs_to :bundle
  belongs_to :external_users

  def self.newEvent (eventType, bundle)
  	bh = BundleHistory.new
  	bh.event = Event.find(eventType)  	
  	bh.bundle = bundle
  	bh.before = bundle.id
    bh.username = User.current
    case eventType
      when 3 # изменение
        bh.after = bundle.title
      when 4
        bh.after = "Комплект: " + bundle.title + " премещён в кабинет: " + InternalUsers.getPlace(bundle.internal_users_id)
      when 8
    	  bh.after = "Комплект: " + bundle.title + " передан: " + ExternalUsers.getFIO(bundle.external_users_id)
      when 9
        bh.after = "Комплект возвращён на склад"
      else
        bh.after = ""
    end
    # unless (bundle.external_users_id = nil)
      bh.external_users_id = bundle.external_users_id
    # end
  	bh.save
  	bh
  end
end
