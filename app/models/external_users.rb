class ExternalUsers < ActiveRecord::Base
  #include Petrovich::Extension
  has_one :bundle
  has_one :bundle_history

  has_many :temporary_uses
  
  # petrovich :firstname  => :name,
  #           :middlename => :patronymic,
  #           :lastname   => :surname

# Petrovich(
#   lastname: 'Салтыков-Щедрин',
#   firstname: 'Михаил',
#   middlename: 'Евграфович',
# ).dative.to_s # => Салтыкову-Щедрину Михаилу Евграфовичу

  def getBundleTitle()
    title = ""
    unless bundle_id.nil? or bundle_id == 0
      if Bundle.exists?(bundle_id)
        bundle = Bundle.find(bundle_id)
        title = bundle.title
      else
        ExternalUsers.where(bundle_id: bundle_id).update_all(bundle_id: nil)
      end
    end
    title
  end

  def fio
    self.surname + " " + self.name + " " + self.patronymic
  end

  def fioGenitive
    begin
      self.surname_genitive + " " + self.name_genitive + " " + self.patronymic_genitive
    rescue
      self.surname + " " + self.name + " " + self.patronymic
    end
  end

  def shortFIO
    self.name[0..0] + ". " + self.patronymic[0..0] + ". " + self.surname
  end

  def self.getFIO(euID, format = 0)
    eu = ExternalUsers.find(euID)
    case format
    when 1 # Фамилия И. О.
      eu.surname + " " + eu.name + " " + eu.patronymic
    else # Фамилия Имя Отчество
      eu.surname + " " + eu.name + " " + eu.patronymic
    end
  end

end