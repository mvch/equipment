class ItemCheck < ActiveRecord::Base
  belongs_to :item

  def getItemTitle()
		items = Item.where("id" => item_id)
		unless (items.count == 0)
			items[0].title
		else
	  		""
	  	end
  	end

  	def getItemSerial()
		items = Item.where("id" => item_id)
		unless (items.count == 0)
			items[0].main_serial
		else
	  		""
	  	end
  	end
end
