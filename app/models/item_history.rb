# coding: utf-8
class ItemHistory < ActiveRecord::Base
  belongs_to :item
  belongs_to :event
  belongs_to :bundle
  belongs_to :repair

  # копирование записей
  # amoeba do 
  #   enable
  # end

  def self.newEvent (eventType, item, bundle, repair)
    # if (eventType == 2 && ItemHistory.where('item_id = ?', item.id).count == 1 && )
  	ih = ItemHistory.new
    case eventType
      when 11 # передача во врем. исп.
        ih.event = Event.find(8)
      when 12 # возврат из врем. исп.
        ih.event = Event.find(9)
      else
        ih.event = Event.find(eventType)
    end
  	
  	ih.item = item
  	ih.bundle = bundle
  	ih.repair = repair
    ih.username = User.current
    case eventType
      when 3 # изменение
        ih.after = item.title + "; " + item.main_serial + "; " + item.additional_serial + "; " + item.inventory_number + "; " + item.amount.to_s + "; " + item.cost.to_s + "; " + item.note
      when 4 # перемещение
        if (bundle != nil)
          ih.after = "В комплект: " + bundle.title
        else
        # end
        # if (bundle == nil) && (internal_users_id == nil)
          ih.after = "Перемещён на склад"
        end
      when 5 # списание
        ih.after = ""
      when 6 # ремонт
        ih.after = "В "+ repair.place      
      when 8 # внутреннее использование
        ih.after = "Перемещён в кабинет "+ InternalUsers.getPlace(item.internal_users_id)
      when 10 # восстановление из списания
        ih.after = ""
      when 11 # временное использование
        # logger.debug "_______________________________________"
        # logger.debug "item: #{item.id}"
        # tu = TemporaryUse.where( "item_id" => item.id, "archive" => false ) 

        # logger.debug "tmp: #{tu.item_id}"
        # user = ExternalUsers.find( tu.external_users_id )
        ih.after = "во временное использование" # + user.fio()
      when 12 # временное использование
        ih.after = "из временного использования"
      else
        ih.after = ""
    end
    # unless (bundle.external_users_id = nil)
    # ih.external_users_id = bundle.external_users_id
    # end
  	ih.save
  	# ih
  end
end
