class Repair < ActiveRecord::Base
	has_many :items
	has_many :item_histories

	def getItemId()
		items = Item.where("repair_id" => id)
		unless (items.count == 0)
			items[0].id
		else
	  		""
	  	end
  	end

  	def getItemTitle()
		items = Item.where("repair_id" => id)
		unless (items.count == 0)
			items[0].title
		else
	  		""
	  	end
  	end

  	def getItemSerial()
		items = Item.where("repair_id" => id)
		unless (items.count == 0)
			items[0].main_serial
		else
	  		""
	  	end
  	end
end
