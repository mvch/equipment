class Item < ActiveRecord::Base
	belongs_to :bundle
	belongs_to :location
  belongs_to :repair
  belongs_to :internal_users

  has_many :item_histories
  has_many :item_checks
  has_many :temporary_uses

  after_create :createEvent
  before_destroy :deleteEvent
  after_update :updateEvent, if: :main_fields_changed?
  after_update :updateEvent_move_to_bundle, if: :move_to_bundle?
  after_update :updateEvent_move_to_warehouse, if: :move_to_warehouse?
  after_update :updateEvent_writeOff, if: :writeOff?
  after_update :updateEvent_undoWriteOff, if: :undoWriteOff?
  after_update :updateEvent_moveOnRepair, if: :moveOnRepair?
  after_update :updateEvent_moveToInternalUser, if: :moveToInternalUser?
  after_update :updateEvent_move_to_warehouse_from_internal_user, if: :move_to_warehouse_from_internal_user?
  after_update :updateEvent_moveToTemporaryUse, if: :moveToTemporaryUse?
  after_update :updateEvent_returnFromTemporaryUse, if: :returnFromTemporaryUse?
  

  def returnFromTemporaryUse?
    location_id_changed? and location_id_was == 7
  end

  def moveToTemporaryUse?
    location_id_changed? and location_id == 7
  end

  def moveOnRepair?
    location_id_changed? and location_id == 5 and repair_id_changed? and !(repair_id.nil? or repair_id == 0)
  end

  def undoWriteOff?
    location_id_changed? and location_id == 1 and location_id_was != 7 and !(bundle_id_changed?) and !(repair_id_changed?) and !(internal_users_id_changed?)
  end

  def writeOff?
    location_id_changed? and location_id == 2 and (bundle_id.nil? or bundle_id == 0)
  end

  def moveToInternalUser?
    location_id_changed? and location_id == 4 #and (bundle_id.nil? or bundle_id == 0)
  end

  def move_to_warehouse?
    location_id_changed? and location_id == 1 and location_id_was != 7 #and bundle_id_changed? and (bundle_id.nil? or bundle_id == 0)
  end

  def move_to_warehouse_from_internal_user?
    location_id_changed? and location_id == 1 and internal_users_id? and (internal_users_id.nil? or internal_users_id == 0)
  end

  def move_to_bundle?
    (location_id_changed? and location_id == 6) or bundle_id_changed?
  end

  def main_fields_changed?
    title_changed? or main_serial_changed? or additional_serial_changed? or inventory_number_changed? or amount_changed? or cost_changed? or note_changed?
  end

  def getBundleTitle()
  	unless bundle_id.nil? or bundle_id == 0
  		# bundle_id
  		bundle = Bundle.find(bundle_id)
  		bundle.title
  	else
  		""
  	end
  end

  protected

  def createEvent   
    #log event
    ih = ItemHistory.newEvent(1, self, nil, nil)
  end

  def deleteEvent
    # очистка истории
    ItemHistory.where(item: self).delete_all
  end

  def updateEvent
    ih = ItemHistory.newEvent(3, self, nil, nil)
  end

  def updateEvent_move_to_bundle
    ih = ItemHistory.newEvent(4, self, self.bundle, nil)
  end

  def updateEvent_move_to_warehouse
    ItemHistory.newEvent(4, self, nil, nil)
  end

  def updateEvent_writeOff
    ItemHistory.newEvent(5, self, nil, nil)
  end

  def updateEvent_undoWriteOff
    ItemHistory.newEvent(10, self, nil, nil)
  end  

  def updateEvent_moveOnRepair
    ItemHistory.newEvent(6, self, nil, self.repair)
  end  

  def updateEvent_moveToInternalUser
    ItemHistory.newEvent(8, self, nil, nil)
  end

  def updateEvent_move_to_warehouse_from_internal_user
    ItemHistory.newEvent(9, self, nil, nil)
  end

  def updateEvent_moveToTemporaryUse
    ItemHistory.newEvent(11, self, nil, nil)
  end

  def updateEvent_returnFromTemporaryUse
    ItemHistory.newEvent(12, self, nil, nil)
  end
  
end
