class BundlesController < ApplicationController
	before_action :authenticate_user!	
	# before_action :require_user # require_user will set the current_user in controllers
    before_action :set_current_user
	respond_to :json, :html
	
	def bundlescount
		@warehouse = Bundle.where('location_id = 1').count()
		@externalUser = Bundle.where('location_id = 3').count()
		@internalUser = Bundle.where('location_id = 4').count()

	end
	def show
		@bundles = Bundle.find(params[:id]) #
		# respond_with(@bundles) do |format|
	 #      format.json 
	 #    end
	end

	def index
		# logger.debug "__________________________________________________________"
  #   	logger.debug "____params: #{params[:filter]}"

		case params[:filter]
		when 'atWarehouse'
			@bundles = Bundle.includes(:location).where('location_id = 1')
		when 'atWarehouseWithCommand'
			@bundles = Bundle.includes(:location).where('location_id = 1')
		else
			@bundles = Bundle.includes(:location).all
		end
    
	    # respond_with(bundles) do |format|
	    #   format.json 
	    # end
	end
	def update
	    bundle = Bundle.find(params[:id])
	    bundle.update_attributes(bundleAttrs)

	    respond_with(bundle) do |format|
	      if bundle.valid?
	        format.json
	      else
	        format.json { render json: { success: false, errors: bundle.errors } }
	      end
	    end
	  end

	def new
		@bundle = Bundle.new
	end

	def create
		#render 	text: params[:TestItem].inspect
		# logger.debug "____params: #{params[:bundle]}"
		# @tmp = params[:bundle]
		# logger.debug "tmp: #{@tmp}"
	    bundle = Bundle.new(bundleAttrs)
	    # bundle.location = Location.find(1);
			# logger.debug "New bundle: #{bundle.attributes.inspect}"
   #  		logger.debug "Post should be valid: #{bundle.valid?}"
	    bundle.save

	    respond_with(bundle) do |format|
	      if bundle.valid?
	        format.json
	      else
	        format.json { render json: { success: false, errors: bundle.errors } }
	      end
    	end
    end

	def destroy
	    bundle = Bundle.find(params[:id])
	    bundle.destroy

	    respond_with(bundle) do |format|
	      format.json
	    end
	end

    private 
		def bundleAttrs
			params.require(:bundle).permit(:id, :title, :note, :location_id, :external_users_id, :internal_users_id)
		end
	

end
