class ItemCheckController < ApplicationController
	before_action :authenticate_user!	
	before_action :set_current_user
	respond_to :json

  def index
  	@itemCheck = ItemCheck.all
  end

  def show
    @itemCheck = Item.find(params[:id]).item_checks #.order("created_at DESC")
  end

  def create
    itemCheck = ItemCheck.new(itemCheckAttrs)
    itemCheck.username = User.current
    itemCheck.save

	item = Item.find(itemCheck.item_id)
	ih = ItemHistory.newEvent(7, item, nil, nil)

    respond_with(itemCheck) do |format|
      if itemCheck.valid?
        format.json
      else
        format.json { render json: { success: false, errors: itemCheck.errors } }
      end
	end
  end  

  private 
	def itemCheckAttrs
		params.require(:item_check).permit(:item_id) 
	end  

end
