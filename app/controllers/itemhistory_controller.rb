class ItemhistoryController < ApplicationController
	before_action :authenticate_user!	
	before_action :set_current_user
	respond_to :json

  def show
    @ih = Item.find(params[:id]).item_histories.order("created_at DESC")
    # @ih = ItemHistories.all
  end


  def index
	  @ih = ItemHistory.all
  end

  def clear # очистка истории
    curItem = Item.find(params[:id])
    logger.debug "____Item: #{curItem.title}"
    
    ItemHistory.where("item_id" => curItem.id).destroy_all
    @ih = Item.find(curItem.id).item_histories.order("created_at DESC")
  end
	


end
