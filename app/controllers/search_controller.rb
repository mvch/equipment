class SearchController < ApplicationController
  before_action :authenticate_user! 
  before_action :set_current_user
  respond_to :json, :html



  	# формирование строки ФИО для отображения в таблице результатов поиска для Внешних пользователей
  	def makeEUTitleString (item)
  		result = item[1] == true ? "Преподаватель: " : "Ученик: "
  		result += item[2] != "" ? item[2] + " " : ""
  		result += item[3] != "" ? item[3] + " " : ""
  		result += item[4] != "" ? item[4] + " " : ""  		
      result.to_s
  	end

  	# формирование описания для отображения в таблице результатов поиска для Внешних пользователей
  	def makeEUDescriptionString (item)
  		userInfo = ""
  		userInfo += item[5] != "" ? item[5] + ", " : ""
  		userInfo += item[6] != "" ? item[6] + ", " : ""
  		userInfo += item[7] != "" ? item[7] + ", " : ""
  		userInfo += item[8] != "" ? item[8] + ", " : ""
  		userInfo += item[9] != "" ? item[9] + ", " : ""
  		userInfo += item[10] != "" ? item[10] + ", " : ""

  		if userInfo.length > 2
  			userInfo = userInfo[ 0, userInfo.length - 2 ]
  		end
  	end

	# формирование строки для поиска по Внешним пользователям
	def makeEUSearchString(item)
  		result = ( item[2] + "|" + item[3] + "|" + item[4] + "|" \
			+ item[5] + "|" + item[6] + "|" + item[7] + "|" + item[8] + "|" + item[9] + "|" \
			+ item[10] ).mb_chars.downcase.to_s
  	end

  	# формирование строки для отображения в таблице результатов поиска для комплектов
  	def makeBundleTitleString (item)
  		result = ""
  		result += item[1] + " " if item[1] != ""
  		# result +=  item[2] != "" ? " (" + item[2] + ")" : ""
  		# logger.debug "note: #{item[2]}\n"
  	end

  	# формирование строки для отображения описания в таблице результатов поиска для комплектов
  	def makeBundleDescriptionString (item)
  		result = ""
  		result +=  item[3] != "" ? item[3] : ""
  		# logger.debug "note: #{item[2]}\n"
  	end

  	# формирование строки для поиска по комплектам
	def makeBundleSearchString(item)
  		result = ( item[1] + "|" + item[2] ).mb_chars.downcase.to_s
  	end

  	# формирование строки для отображения в таблице результатов поиска для элементов
  	def makeItemTitleString (item)
  		result = ""
  		result += item[1].to_s + " " if item[1] != ""
  	end

  	# формирование строки для отображения описания в таблице результатов поиска для элементов
  	def makeItemDescriptionString (item)
  		itemInfo = ""
  		
  		if ( item[2] == 'Комплект' ) # 6 - расположение в комплекте
  			itemInfo += item[2]
  			itemInfo += item[3] != nil && item[3] != "" ? ": " + item[3] : ""
			itemInfo += ", "
		elsif ( item[2] == 'Внутреннее использование' ) # если внутреннее испльзование, то добавляем место установки
			itemInfo += item[2] != "" ? item[2] + " - " : ""	
			itemInfo += item[3] != nil && item[3] != "" ? "каб. " + item[3] : ""
			itemInfo += item[8] != nil && item[8] != "" ? " (" + item[8] + ")": ""
			itemInfo += ": "
  		elsif ( item[2] != "" && item[2] != nil ) # если не в комплекте, то его наименование не указываем
  			itemInfo += item[2] != "" ? item[2] + ", " : ""	
  		end
  				
  		itemInfo += (item[4] != '' && item[4] != '---------------') ? item[4] + ', ' : ''
  		itemInfo += (item[5] != '' && item[5] != '---------------') ? item[5] + ', ' : ''
  		itemInfo += item[6] != nil && item[6] != '' ? item[6] + ", " : ""

  		if itemInfo.length > 2
  			itemInfo = itemInfo[ 0, itemInfo.length - 2 ]
  		end

		itemInfo != "" ? itemInfo : ""
  	end

  	# формирование строки для поиска по элементам
	def makeItemSearchString(item)
  		result = item[1].to_s
  		result += item[3] != nil && item[3] != '' ? item[3] + ", " : ""
  		result += (item[4] != '' && item[4] != '---------------') ? item[4] + ', ' : ''
  		result += (item[5] != '' && item[5] != '---------------') ? item[5] + ', ' : ''
  		result += item[6] != nil && item[6] != '' ? item[6] + ", " : ""

  		result = result.mb_chars.downcase.to_s
  	end

    # подсветка искомой строки
  	def highlight(text,searchStr)
  		if ( searchStr != '')
    		if ( text != nil)
          if ( text.scan(/#{searchStr}/i).size > 0)
    			 text.gsub!(/#{searchStr}/i, "<span class=\"highlight\">" + '\0' + "</span>")
          else
            text
          end
    		else
    			''
    		end
      else
        text
      end
  	end

  	# основная функця поиска
	def dosearch( searchStr, archive )

		# выбираем информацию о всех пользователях
		data = ExternalUsers.all

		# собираем в массив
		dataPluck = data.where( :archive => false ).pluck( :id, :is_teacher, :surname, :name, :patronymic, :address, :phone, :email, :skype, :vklogin, :provider )
		# logger.debug "data: #{dataPluck}\n"

		# преобразовываем массив к формату: [раздел,id,title,description,searchdata] 
		dataPluckMap = dataPluck.map {|item| [item[1] == true ? 1 : 2, item[0], highlight(makeEUTitleString(item), searchStr), \
			# makeEUDescriptionString(item), makeEUSearchString(item) ] }
      highlight(makeEUDescriptionString(item), searchStr), makeEUSearchString(item) ] }

		# выбираем информацию о всех комплектах
		dataBundles = Bundle.joins(:location).select('bundles.id, title, note, locations.name')

		# собираем в массив
		dataPluckBundles = dataBundles.pluck( :id, :title, :note, :name )
		# logger.debug "dataBundles: #{dataPluckBundles}\n"

		# преобразовываем массив к формату: [category,id,title,description,searchdata] 
		dataPluckMap += dataPluckBundles.map {|item| [3, item[0], highlight(makeBundleTitleString(item), searchStr), \
			highlight(makeBundleDescriptionString(item), searchStr), makeBundleSearchString(item) ] }

		# выбираем информацию о всех элементах вне комплекта, собираем в массив
		#dataPluckItems =  Item.includes(:location, :bundle).where.not( :location_id => 6).pluck('items.id, items.title items_title, 
		#	locations.name, bundles.title bundles_title, items.main_serial, inventory_number, items.note items_note' )

		dataPluckItems =  Item.includes(:location, :bundle).where("items.location_id not in (6, 2, 4, 1)").pluck('items.id, items.title items_title, 
			locations.name, bundles.title bundles_title, items.main_serial, inventory_number, items.note items_note' )
		# logger.debug "dataItems: #{dataPluckItems}\n"

		# преобразовываем массив к формату: [раздел,id,title,description,searchdata] 
		dataPluckMap += dataPluckItems.map {|item| [4, item[0], highlight(makeItemTitleString(item), searchStr), \
			highlight(makeItemDescriptionString(item), searchStr), makeItemSearchString(item) ] }

    # выбираем информацию о всех элементах в комплектах, собираем в массив
    dataPluckBundleItems =  Item.includes(:location, :bundle).where(location_id: 6).pluck('items.id, items.title items_title, 
      locations.name, bundles.title bundles_title, items.main_serial, inventory_number, items.note items_note, items.bundle_id' )
    # преобразовываем массив к формату: [раздел,id,title,description,searchdata] 
    dataPluckMap += dataPluckBundleItems.map {|item| [5, item[0], highlight(makeItemTitleString(item), searchStr), \
      highlight(makeItemDescriptionString(item), searchStr), makeItemSearchString(item), item[7] ] }

    # выбираем информацию о всех элементах на списании, собираем в массив
    dataPluckBundleItems =  Item.includes(:location, :bundle).where(location_id: 2).pluck('items.id, items.title items_title, 
      locations.name, bundles.title bundles_title, items.main_serial, inventory_number, items.note items_note, items.bundle_id' )
    # преобразовываем массив к формату: [раздел,id,title,description,searchdata] 
    dataPluckMap += dataPluckBundleItems.map {|item| [6, item[0], highlight(makeItemTitleString(item), searchStr), \
      highlight(makeItemDescriptionString(item), searchStr), makeItemSearchString(item), item[7] ] }

	# выбираем информацию о всех элементах во внутреннем использовании
    dataPluckItems =  Item.includes(:location, :internal_users).where(location_id: 4).pluck('items.internal_users_id, items.title items_title, 
	  locations.name, internal_users.room title, items.main_serial, inventory_number, items.note items_note, items.internal_users_id, internal_users.place' )
	# logger.debug "dataItems: #{dataPluckItems}\n"
    # преобразовываем массив к формату: [раздел,id,title,description,searchdata] 
    dataPluckMap += dataPluckItems.map {|item| [7, item[0], highlight(makeItemTitleString(item), searchStr), \
      highlight(makeItemDescriptionString(item), searchStr), makeItemSearchString(item), item[7] ] }

	# выбираем информацию о всех элементах на складе
    dataPluckItems =  Item.includes(:location, :bundle).where(location_id: 1).pluck('items.id, items.title items_title, 
	  locations.name, bundles.title bundles_title, items.main_serial, inventory_number, items.note items_note, items.internal_users_id' )
    # преобразовываем массив к формату: [раздел,id,title,description,searchdata] 
    dataPluckMap += dataPluckItems.map {|item| [8, item[0], highlight(makeItemTitleString(item), searchStr), \
      highlight(makeItemDescriptionString(item), searchStr), makeItemSearchString(item), item[7] ] }


		# фильтруем лишние
		# logger.debug "dataPluckMap: #{dataPluckMap}\n"
		dataPluckMapSelect = dataPluckMap.select {|item| item[4].include? searchStr.mb_chars.downcase.to_s}
		# logger.debug "dataPluckMapSelect: #{dataPluckMapSelect}"
	end

	def index
		# archive = params[:archive]
		searchStr = params[:searchstr] != nil ? params[:searchstr] : ''
		archive = 'false'
		# searchStr = ''
		# logger.debug "searchStr: #{searchStr}"
		@searchdata = dosearch( searchStr, archive )
	end
end
