class ExternalusersController < ApplicationController
  before_action :authenticate_user! 
  before_action :set_current_user
  respond_to :json

  def select
    @eu = ExternalUsers.where('archive' => false, 'bundle_id' => nil)
  end

  def patronymics
    @patronymics = ExternalUsers.select(:patronymic).distinct.order(:patronymic)
  end

  def names
    @names = ExternalUsers.select(:name).distinct.order(:name)
  end

  def providers
    @providers = ExternalUsers.select(:provider).distinct.order(:provider)
  end

  def index
    # logger.debug "____params: #{params[:archive]}"
    archive = params[:archive]
    case params[:type]
    when 'teachers'
      @eu = ExternalUsers.where("is_teacher = true and archive = ?", params[:archive])
    when 'students'
      @eu = ExternalUsers.where("is_teacher = false and archive = ?", params[:archive])
    else
    	@eu = ExternalUsers.where("archive" => to_boolean(archive))
    end
  end

  def show
  	@eu = ExternalUsers.where("id" => params[:id])#find(params[:id])
  end

  def create    
  	@eu = ExternalUsers.new(euAttrs)
  	@eu.archive = false
    if @eu.is_teacher == nil
      @eu.is_teacher = false
    end
    @eu.bundle_id = nil
    @eu.name = @eu.name.capitalize
  	@eu.save

    respond_to do |format|
        format.json { render :json => @eu }
    end
  end

  def update
  	eu = ExternalUsers.find(params[:id])
#logger.debug "____params: #{params[euAttrs]}"
#logger.debug "____params: #{params[:installdate]}"
# logger.debug "____params: #{params[:uninstalldate]}"
  	eu.update_attributes(euAttrs)
    # eu.name = eu.name.capitalize
    # eu.save

  	respond_with(eu) do |format|
      format.json
	  end
  end

  def destroy
  	eu = ExternalUsers.find(params[:id])
  	eu.destroy

  	respond_with(eu) do |format|
	      format.json
	  end
  end

  private
  	def euAttrs 
  		params.require(:externalUser).permit(:id, :surname, :patronymic, :name, :is_teacher, 
  			:address, :phone, :email, :skype, :vklogin, :password, :archive, :installdate, 
  			:uninstalldate, :bundle_id, :provider, :note)
  	end

end
