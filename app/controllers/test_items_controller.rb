class TestItemsController < ApplicationController
	respond_to :js
	def show
		@items = TestItems.all
    
	    respond_with(@items) do |format|
	      format.json 
	    end
	end

	def index
		@items = TestItems.all
    
	    respond_with(@items) do |format|
	      format.json 
	    end
	end

	def new
		@testItem = TestItems.new
	end


	def create
		# render 	text: params[:TestItem].inspect
		logger.debug "____params: #{params[:testItem]}"
		@tmp = params[:TestItem]
		logger.debug "tmp: #{@tmp}"
	    testItem = TestItems.new(testItem1)
	    # testItem = TestItems.new(params[:testItem])
	    testItem.amount = 1
			logger.debug "New testItem: #{testItem.attributes.inspect}"
    		logger.debug "Post should be valid: #{testItem.valid?}"
	    testItem.save

	    respond_with(testItem) do |format|
	      if testItem.valid?
	        format.json
	      else
	        format.json { render json: { success: false, errors: testItem.errors } }
	      end
    	end
  end

	def destroy
	    testItem = TestItems.find(params[:id])
	    testItem.destroy

	    respond_with(testItem) do |format|
	      format.json
	    end
	end

	  def update
	    testItem = TestItems.find(params[:id])
	    testItem.update_attributes(testItem1)

	    respond_with(testItem) do |format|
	      if testItem.valid?
	        format.json
	      else
	        format.json { render json: { success: false, errors: testItem.errors } }
	      end
	    end
	  end

  private 
	def testItem1
		params.require(:testItem).permit(:title,:id,:main_serial, :additional_serial, :inventory_number, :amount, :cost)
	end
end
