class BundleitemsController < ApplicationController
	before_action :authenticate_user!	
	before_action :set_current_user
	respond_to :json
	
	def show
		@bundleItems = Bundle.find(params[:id]).items.where('location_id = 6').order(:order_number, :id) 
	end

	def new
		@item = Item.new
	end

	def create
		#render 	text: params[:TestItem].inspect
		# logger.debug "____params: #{params[:item]}"
		# @tmp = params[:item]
		# logger.debug "tmp: #{@tmp}"
	    item = Item.new(itemAttrs)
	    # if ( item.main_serial == null or item.main_serial == '')
	    # 	item.main_serial = '---------------'
	    
			# logger.debug "New item: #{item.attributes.inspect}"
   #  		logger.debug "Post should be valid: #{item.valid?}"
	    item.save

		@bundleItems = Bundle.find(item.bundle_id).items
	    # respond_with(item) do |format|
	    #   if item.valid?
	    #     format.json
	    #   else
	    #     format.json { render json: { success: false, errors: item.errors } }
	    #   end
    	# end
    end

	def destroy
	    item = Item.find(params[:id])
	    item.destroy

	    # respond_with(item) do |format|
	    #   format.json
	    # end
	end

	private 
		def itemAttrs
			params.require(:item).permit(:id, :title, :note, :bundle_id, :main_serial, :additional_serial, :inventory_number, :amount, :cost, :location_id) #
		end

end
