class BundleCheckController < ApplicationController
	before_action :authenticate_user!	
	before_action :set_current_user
	respond_to :json

  def index
  	@bundleCheck = BundleCheck.all
  end

  def show
    @bundleCheck = bundle.find(params[:id]).bundle_checks #.order("created_at DESC")
  end

  def create
    bundleCheck = BundleCheck.new(bundleCheckAttrs)
    bundleCheck.username = User.current
    bundleCheck.save

  	bundle = Bundle.find(bundleCheck.bundle_id)
  	bh = BundleHistory.newEvent(7, bundle)

    respond_with(bundleCheck) do |format|
      if bundleCheck.valid?
        format.json
      else
        format.json { render json: { success: false, errors: bundleCheck.errors } }
      end
	  end
  end  

  def update
  	bc = BundleCheck.find(params[:id])
  	bc.update_attributes(bundleCheckAttrs)

  	respond_with(bc) do |format|
      format.json
	  end
  end


  def destroy
  	bc = BundleCheck.find(params[:id])
  	bc.destroy

  	respond_with(bc) do |format|
	      format.json
	  end
  end

  private 
	def bundleCheckAttrs
		params.require(:bundle_check).permit(:bundle_id, :external_user_id, :note, :created_at) 
	end  

end
