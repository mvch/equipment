class TestbundleController < ApplicationController
  before_action :authenticate_user!
  before_action :set_current_user 
  def index
  	# generateTestValues
  	@bundles = Bundle.all
  	@items = Item.all
  	
  end
  def generateTestValues
  	bundle = Bundle.new
  	bundle.title = "title 1"
  	bundle.note = "note 1"
  	item = bundle.items.new
  	item.title = "item title 1"
  	item.save
  	item = bundle.items.new
  	item.title = "item title 2"
  	item.save
  	item = bundle.items.new
  	item.title = "item title 3"
  	item.save
  	bundle.save
  end
end
