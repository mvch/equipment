class InternalusersController < ApplicationController
  before_action :authenticate_user! 
  before_action :set_current_user
  respond_to :json

  def items # оборудование, установленное в кабинете
    # комплекты
    @items = Item.joins(:bundle).where( bundles: { :internal_users_id => params[:id], :location_id => 4} )
    # элементы вне комплектов
    @items += Item.where(:internal_users_id => params[:id], :location_id => 4)
  end

  def index
  	@iu = InternalUsers.all
  end

  def create    
  	@iu = InternalUsers.new(iuAttrs)  	
  	@iu.save

    respond_to do |format|
        format.json { render :json => @iu }
    end
  end

  def update
  	iu = InternalUsers.find(params[:id])
  	iu.update_attributes(iuAttrs)

  	respond_with(iu) do |format|
      format.json
	  end
  end

  def destroy
  	iu = InternalUsers.find(params[:id])
  	iu.destroy

  	respond_with(iu) do |format|
	      format.json
	  end
  end

  private
  	def iuAttrs 
  		params.require(:internalUser).permit(:id, :room, :place, :note)
  	end
end
