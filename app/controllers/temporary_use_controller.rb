class TemporaryUseController < ApplicationController
  before_action :authenticate_user! 
  before_action :set_current_user
  respond_to :json, :html

  def index
    archive = params[:archive]
  	@tempUse = TemporaryUse.where('archive' => to_boolean(archive))
  end

  def show
  	@tempUse = TemporaryUse.where("id" => params[:id])#find(params[:id])
  end

  def create
    # logger.debug "__________________params: #{params[:item_id]}"
  	@tempUse = TemporaryUse.new(tempUseAttrs) #    
    @tempUse.archive = false
    @tempUse.save

    respond_to do |format|
        format.json { render :json => @tempUse }
    end
  end

  def update
  	tempUse = TemporaryUse.find(params[:id])
  	tempUse.update_attributes(tempUseAttrs)

  	respond_with(tempUse) do |format|
      format.json
	  end
  end

  def destroy
  	tempUse = TemporaryUse.find(params[:id])
  	tempUse.destroy

  	respond_with(tempUse) do |format|
	      format.json
	  end
  end

  def warning
    "1"
  end

  private
  	def tempUseAttrs 
  		params.require(:temporaryUse).permit(:id, :from, :before, :receive, :description, 
  			:external_users_id, :item_id, :archive)
  	end
end
