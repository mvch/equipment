class ItemsController < ApplicationController
	before_action :authenticate_user!	
	before_action :set_current_user
	respond_to :js, :html, :json

	# изменение порядкового номера элемента в комлпекте
	def changeOrderNumber
		offset = params[:offset]
		curItem = Item.find( params[:item_id] )

		# отбираем все элементы комплекта
		items = Bundle.find(curItem.bundle_id).items.where(location_id: 6).order(:order_number, :id) 

		# обновляем порядковые номера
		curNumber = 1
		items.each do |item|
			item.order_number = curNumber
			item.save
			if item.id == curItem.id
				curItem = item
			end
			curNumber += 1
		end
		 
		# вычисляем номер следующего/предыдущего
   		nextOrderNumber = curItem.order_number + offset.to_i

		# выполняем смещение
		nextItem = Item.where(order_number: nextOrderNumber, bundle_id: curItem.bundle_id, location_id: 6).first

		if ( nextItem != nil )
			# logger.debug "____nextItem: #{nextItem.order_number}"
			curItem.order_number = nextItem.order_number
			curItem.save
			nextItem.order_number = nextItem.order_number - offset.to_i
			nextItem.save
		end


		respond_with(curItem) do |format|
	      format.json
	    end
	end

	# перемещение комплекта на склад
	def moveBundleToStock
		@bundle = Bundle.find(params[:id])
		@bundle.location_id = 1
		@bundle.internal_users_id = nil
		@bundle.save
	end

	# перемещение элемента на склад
	def moveItemToStock
		curItem = Item.find(params[:id])
		curItem.location_id = 1
		curItem.bundle_id = nil
		curItem.internal_users_id = nil
		curItem.save
		
		respond_to do |format|
        	format.json { render json: "ok" }
    	end
	end

	# перемещение элемента во временное использование 
	def moveItemToTemporaryUse
		curItem = Item.find(params[:id])
		curItem.location_id = 7
		curItem.save

		respond_with(curItem) do |format|
	      format.json
	    end
	end

	# возврат элемента из временного использования 
	def returnItemFromTemporaryUse
		curItem = Item.find(params[:id])
		if ( curItem.bundle_id == nil || curItem.bundle_id == 0 )
			curItem.location_id = 1 # на склад
		else
			curItem.location_id = 6 # назад в комплект
		end
		curItem.save

		respond_with(curItem) do |format|
	      format.json
	    end
	end

	def checkExistsSerial # проверка серийного номера на наличие
		# if (params[:main_serial] != '---------------')
			@items = Item.where("id != ? and ((main_serial = ? and main_serial != '---------------') or (inventory_number = ? and inventory_number != '---------------'))", params[:id], params[:main_serial], params[:inventory_number])
		# end
		# @items = Item.all
		# @items.count()
		# respond_with(item) do |format|
	 #      format.json { render json: { success: false, errors: item } }
	 #    end
	end

	def writeOffPartial # частичное списание количества элемента
		# копируем элемент с указанием списываемого количества
		curItem = Item.find(params[:id])
		newItem = Item.new
		newItem.title = curItem.title
		newItem.main_serial = curItem.main_serial
		newItem.additional_serial = curItem.additional_serial
		newItem.inventory_number = curItem.inventory_number
		newItem.cost = curItem.cost
		newItem.note = curItem.note
		newItem.bundle_id = curItem.bundle_id
		newItem.amount = params[:amount]
		newItem.location_id = curItem.location_id
		newItem.save

		# удаляем запись в истории о создании
		itemHistory = ItemHistory.find_by(item_id: newItem.id)
		itemHistory.destroy
		# списываем вновь созданный элемент
		newItem.location_id = 2
		newItem.bundle_id = 0
		newItem.save
		# копируем историю старого элемента в новый
		curItemHistory = ItemHistory.where("item_id = ?", curItem.id)
		curItemHistory.find_each do |ih|
			newItemHistory = ItemHistory.new(ih.attributes.merge({:id => nil, :item_id => newItem.id}))
			newItemHistory.save
		end

		# cоздаём запись в истории о списании части элементов
		saveCurItemHistory = ItemHistory.new
		saveCurItemHistory.item_id = curItem.id
		saveCurItemHistory.username = User.current
		saveCurItemHistory.event = Event.find(5)
		# curItemAmount = curItem.amount + params[:amount].to_i
		saveCurItemHistory.after = "Cписание нескольких штук:" + params[:amount].to_s + " из " + curItem.amount.to_s
		saveCurItemHistory.save

		respond_with(newItem) do |format|
	      format.json
	    end
	end
	
	def titles
		@items = Item.select(:title).distinct
	end
	
	def index
		case params[:location_type]
		when 'warehouse'
			@items = Item.where('location_id = 1')
		when 'outsideBundle'
			@items = Item.where('bundle_id = 0 and location_id <> 2')
		when 'atWarehouseWithCommand'
			@items = Item.joins('LEFT OUTER JOIN bundles ON items.bundle_id = bundles.id')
						 .where('items.location_id = 1 or bundles.location_id = 1')
		when 'writeoff'
			@items = Item.where('location_id = 2')
		else
			@items = Item.all
		end
	end

	def show
	    @items = Item.where('id = ?', params[:id])
	end


	def update
	    item = Item.find(params[:id])
	    item.update_attributes(itemAttrs)
	    if ( item.main_serial == nil or item.main_serial == '')
	    	item.main_serial = '---------------'
	    end
	    item.save


	    respond_with(item) do |format|
	      if item.valid?
	        format.json
	      else
	        format.json { render json: { success: false, errors: item } }
	      end
	    end
	end

	def new
		@item = Item.new
	end

	def create
	    item = Item.new(itemAttrs)
	    if ( item.main_serial == nil or item.main_serial == '')
	    	item.main_serial = '---------------'
	    end
	    if ( item.inventory_number == nil or item.inventory_number == '')
	    	item.inventory_number = '---------------'
	    end
	    item.save

	    respond_with(item) do |format|
	      if item.valid?
	        format.json
	      else
	        format.json { render json: { success: false, errors: item.errors } }
	      end
    	end
    end

	def destroy
	    item = Item.find(params[:id])
	    item.destroy

	    respond_with(item) do |format|
	      format.json
	    end
	end
  private 
	def itemAttrs
		params.require(:item).permit(:id, :title, :main_serial, :additional_serial, :inventory_number, 
			:amount, :cost, :note, :bundle_id, :location_id, :repair_id, :internal_users_id) 
	end

end
