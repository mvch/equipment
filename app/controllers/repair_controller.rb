class RepairController < ApplicationController
  before_action :authenticate_user! 
  before_action :set_current_user
  respond_to :json, :html

  def places
    @places = Repair.select(:place).distinct.order(:place)
  end

  def index
  	@repair = Repair.all
  end

  def show
  	@repair = Repair.where("id" => params[:id])#find(params[:id])
  end

  def create
    # logger.debug "__________________params: #{params[:item_id]}"
  	@repair = Repair.new(repairAttrs) #    
    @repair.save

    respond_to do |format|
        format.json { render :json => @repair }
    end
  end

  def update
  	repair = Repair.find(params[:id])
  	repair.update_attributes(repairAttrs)

  	respond_with(repair) do |format|
      format.json
	  end
  end

  def destroy
  	repair = Repair.find(params[:id])
  	repair.destroy

  	respond_with(repair) do |format|
	      format.json
	  end
  end

  def warning
    "1"
  end

  private
  	def repairAttrs 
  		params.require(:repair).permit(:id, :place, :sent, :receive, :control_days, 
  			:description, :archive) #, :item_id, :item_title, :item_serial
  	end


end
