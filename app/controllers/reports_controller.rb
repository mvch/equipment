class ReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_current_user
  def index
  end

  def exportusers
  	require 'spreadsheet'
  	Spreadsheet.client_encoding = 'UTF-8'
  	book = Spreadsheet::Workbook.new
  	sheet1 = book.create_worksheet
  	sheet1.name = 'Пользователи'
  	sheet1[0,0] = '№ п.п.'
  	sheet1[0,1] = 'Фамилия'
  	sheet1[0,2] = 'Имя'
  	sheet1[0,3] = 'Отчество'
  	sheet1[0,4] = 'Преподаватель'
  	sheet1[0,5] = 'Адрес'
  	sheet1[0,6] = 'Телефон'
  	sheet1[0,7] = 'E-mail'
  	sheet1[0,8] = 'Skype'
  	sheet1[0,9] = 'Логин ВК'
  	sheet1[0,10] = 'Пароль'
  	sheet1[0,11] = 'Провайдер'
  	sheet1[0,12] = 'Комплект'
  	sheet1[0,13] = 'Дата установки'
  	sheet1[0,14] = 'Дата возврата'
  	format = Spreadsheet::Format.new :weight => :bold
  	sheet1.row(0).default_format = format
  	# выборка данных
  	row_number = 1
  	eu = ExternalUsers.where("archive" => false)
  	eu.each do |eu|
  		sheet1[row_number,0] = row_number
  		sheet1[row_number,1] = eu.surname
  		sheet1[row_number,2] = eu.name
  		sheet1[row_number,3] = eu.patronymic
  		sheet1[row_number,4] = (eu.is_teacher) ? 'да' : 'нет'
		sheet1[row_number,5] = eu.address
		sheet1[row_number,6] = eu.phone
		sheet1[row_number,7] = eu.email
		sheet1[row_number,8] = eu.skype
		sheet1[row_number,9] = eu.vklogin
		sheet1[row_number,10] = eu.password
		sheet1[row_number,11] = eu.provider
		sheet1[row_number,12] = eu.getBundleTitle()
		sheet1[row_number,13] = eu.installdate
		sheet1[row_number,14] = eu.uninstalldate
  		row_number += 1
  	end
  	# формирование файла
  	report_file_name = "#{Rails.root}/tmp/reports/" + 'users' + ".xls"
	book.write report_file_name
	send_file(report_file_name)
  end

  def exportbundlechecks
  	require 'spreadsheet'
  	Spreadsheet.client_encoding = 'UTF-8'
  	book = Spreadsheet::Workbook.new
  	sheet1 = book.create_worksheet
  	sheet1.name = 'Проверка оборудования'
  	sheet1[0,0] = 'Дата'
  	sheet1[0,1] = 'Комплект'
  	sheet1[0,2] = 'Ф. И. О. пользователя'
  	sheet1[0,3] = 'Примечание'
  	sheet1[0,4] = 'Проверил'
  	format = Spreadsheet::Format.new :weight => :bold
  	sheet1.row(0).default_format = format
  	# выборка данных
  	row_number = 1
  	bundleCheck = BundleCheck.all
  	bundleCheck.each do |bc|
  		sheet1[row_number,0] = bc.created_at
  		sheet1[row_number,1] = bc.getBundleTitle()
  		sheet1[row_number,2] = bc.getExternalUserFIO()
  		sheet1[row_number,3] = bc.note
  		sheet1[row_number,4] = bc.username
		
  		row_number += 1
  	end
  	# формирование файла
  	report_file_name = "#{Rails.root}/tmp/reports/" + 'bundlechecks' + ".xls"
	book.write report_file_name
	send_file(report_file_name)
  end

  def temporaryuseact
  	printTemporaryUse('temporaryuseact')
  end

  def temporaryuse
  	printTemporaryUse('temporaryuse')
  end

  #печать договора передачи или акта возврата (временное использование)
  def printTemporaryUse (filename)
  	@temporaryUse = TemporaryUse.find(params[:id])
  	@user = ExternalUsers.find(@temporaryUse.external_users_id)
  	generateFileName = "";
  	# все записи этого пользователя одной даты передач в один акт
  	if ( filename == "temporaryuse")
		@tu = TemporaryUse.where( "external_users_id" => @temporaryUse.external_users_id,
			"from" => @temporaryUse.from, "archive" => false)
		generateFileName = "врем. исп. договор";
	else
		@tu = TemporaryUse.where( "external_users_id" => @temporaryUse.external_users_id,
			"from" => @temporaryUse.from)
		generateFileName = "врем. исп. акт";
	end
   	# logger.debug "____params: #{@item.title}"
	  #  		@tu.each do |tu|
	  #  			logger.debug "____params: #{tu.item.title}"
		# end

	report = ODFReport::Report.new("#{Rails.root}/reports/" + filename + ".odt") do |r|
		# r.add_field :today, Date.today.strftime("%d.%m.%Y г.")
		# r.add_field :today, @temporaryUse.from.strftime("%d.%m.%Y г.")
		r.add_field :fio, @user.fio

		if ( (@temporaryUse.from != "") && (@temporaryUse.from != nil) )
			@from = @temporaryUse.from.strftime("%d.%m.%Y г.")
		else
			if ( filename == "temporaryuse" )
				@from = Date.today.strftime("%d.%m.%Y г.")
			else
				@from = "_______________________________"
			end
		end
		r.add_field :from, @from

		if ( (@temporaryUse.before != "") && (@temporaryUse.before != nil) )
			@before = @temporaryUse.before.strftime("%d.%m.%Y г.")
		else
			@before = "_______________________________"
		end
		r.add_field :before, @before

		if ( (@temporaryUse.receive != "") && (@temporaryUse.receive != nil) )
			@receive = @temporaryUse.receive.strftime("%d.%m.%Y г.")
		else
			@receive = Date.today.strftime("%d.%m.%Y г.")
		end
		r.add_field :receive, @receive

		@row_number = 0
		r.add_table("TABLE_ITEMS", @tu, :header=>true) do |t|
			t.add_column(:id) { |tu| (@row_number += 1) }
		    t.add_column(:item) { |tu| "#{tu.item.title}" }
		    t.add_column(:serial) { |tu| "#{tu.item.main_serial}" }
		    t.add_column(:inventory_number) { |tu| "#{tu.item.inventory_number}" }
		    t.add_column(:amount) { |tu| "#{tu.item.amount}" }
		    t.add_column(:cost) { |tu| "#{tu.item.cost}" }
		end

		note = "-----"
		total_amount = 0
		total_cost = 0
		item_number = 1
		@tu.each do |tu|
			total_amount += (tu.item.amount != nil) ? tu.item.amount : 0
			total_cost += (tu.item.cost != nil) ? tu.item.cost : 0
			if (tu.item.note != nil && tu.item.note != "")
				if (note == "-----")
					note = "№" + item_number.to_s + " - " + tu.item.note
				else
					note += ", " + "№" + item_number.to_s + " - " +  tu.item.note
				end
			end
			item_number += 1
		end

		r.add_field :total_amount, total_amount
		r.add_field :total_cost, total_cost
		r.add_field :note, note
	end

	send_data report.generate, type: 'application/vnd.oasis.opendocument.text',
                               disposition: 'attachment',
                               filename: generateFileName + @user.shortFIO + '.odt'
  end

  def studentact
  	printact ("studentact")
  end

  def teacheract
  	printact ("teacheract")
  end

  def printact (filename)
  	# logger.debug "__________________________________________________________"
   #  logger.debug "____params: #{params[:bundle]}"

  	# запрашиваем данные из БД
  	@user = ExternalUsers.find(params[:id])
  	@bundle = Bundle.find(params[:bundle])
	@items = Bundle.find(params[:bundle]).items.order(:order_number, :id)

	# формируем отчёт
	@title = @user.fio
	if ( (@user.address != "") && (@user.address != nil) )
		@title += ", " + @user.address
	end
	if ( (@user.phone != "") && (@user.phone != nil) )
		@title += ", " + @user.phone
	end

	report = ODFReport::Report.new("#{Rails.root}/reports/" + filename + ".odt") do |r|
		r.add_field :title, @title
		r.add_field :bundle_title, @bundle.title
		r.add_field :today, Date.today.strftime("%d.%m.%Y г.")
		r.add_field :fio, @user.fio
		r.add_field :shortfio, @user.shortFIO
		r.add_field :address, @user.address
		if (@user.installdate != nil)
			r.add_field :installdate, @user.installdate.strftime("%d.%m.%Y г.")
		else
			r.add_field :installdate, '"___" ___________ 20____ г.'
		end


		@row_number = 0
		r.add_table("TABLE_ITEMS", @items, :header=>true) do |t|
			t.add_column(:id) { |item| (@row_number += 1) }
		    t.add_column(:item, :title)
		    t.add_column(:serial, :main_serial)
		    t.add_column(:inventory_number, :inventory_number)
		    t.add_column(:amount, :amount)
		    t.add_column(:cost, :cost)
		    # t.add_column(:description) do { |item| "==> #{item.description}" }
		end

		total_amount, total_cost, note = calctotals(@items)

		r.add_field :total_amount, total_amount
		r.add_field :total_cost, total_cost
		r.add_field :note, note
	end

	send_data report.generate, type: 'application/vnd.oasis.opendocument.text',
                               disposition: 'attachment',
                               filename: 'акт ' + @bundle.title + ' с ' + @user.shortFIO + '.odt'
  end

  def studentcontract
  	printcontract ("studentcontract")
  end

  def teachercontract
  	printcontract ("teachercontract")
  end

  def printcontract (filename)
  	@user = ExternalUsers.find(params[:id])
  	@bundle = Bundle.find(@user.bundle_id)
	@items = Bundle.find(@user.bundle_id).items.order(:order_number, :id)

	report = ODFReport::Report.new("#{Rails.root}/reports/" + filename + ".odt") do |r|
		# r.add_field :replace_text, @bundle.title
		r.add_field :bundle_title, @bundle.title
		# r.add_field :today, Date.today.strftime("%d.%m.%Y г.")
		r.add_field :today, @user.installdate.strftime("%d.%m.%Y г.")
		r.add_field :fio, @user.fio

		r.add_field :surname, @user.surname
		r.add_field :email, @user.email
		r.add_field :skype, @user.skype
		r.add_field :vklogin, @user.vklogin
		r.add_field :password, @user.password

		if (@user.patronymic[-1..-1] == "а")
			@genderWordEnd = "й"
		else
			@genderWordEnd = "го"
		end

		r.add_field :wordgender, @genderWordEnd
		r.add_field :fiogenitive, @user.fioGenitive

		r.add_field :shortfio, @user.shortFIO

		if ( (@user.address != "") && (@user.address != nil) )
			@userAddress = @user.address
		else
			@userAddress = "_______________________________"
		end

		r.add_field :address, @userAddress

		if ( (@user.phone != "") && (@user.phone != nil) )
			@userPhone = @user.phone
		else
			@userPhone = "__________________________________"
		end
		r.add_field :phone, @userPhone


		@row_number = 0
		r.add_table("TABLE_ITEMS", @items, :header=>true) do |t|
			t.add_column(:id) { |item| (@row_number += 1) }
		    t.add_column(:item, :title)
		    t.add_column(:serial, :main_serial)
		    t.add_column(:inventory_number, :inventory_number)
		    t.add_column(:amount, :amount)
		    t.add_column(:cost, :cost)
		    # t.add_column(:description) do { |item| "==> #{item.description}" }
		end

		total_amount, total_cost, note = calctotals(@items)

		r.add_field :total_amount, total_amount
		r.add_field :total_cost, total_cost
		r.add_field :note, note
	end

  	send_data report.generate, type: 'application/vnd.oasis.opendocument.text',
                               disposition: 'attachment',
                               filename: 'договор ' + @bundle.title + ' с ' + @user.shortFIO + '.odt'
  end

  # подсчёт итогов, формирование строки примечаний
  def calctotals (items)
	note = "-----"
	total_amount = 0
	total_cost = 0
	item_number = 1
	items.each do |item|
		total_amount += (item.amount != nil) ? item.amount : 0
		total_cost += (item.cost != nil) ? item.cost : 0
		if (item.note != nil && item.note != "")
			if (note == "-----")
				note = "№" + item_number.to_s + " - " + item.note
			else
				note += ", " + "№" + item_number.to_s + " - " + item.note
			end
		end
		item_number += 1
	end

	return total_amount, total_cost, note
  end

  # печать карточки комплекта
  def bundle
  	@bundle = Bundle.find(params[:id])
	@items = Bundle.find(params[:id]).items.order(:order_number, :id)

	report = ODFReport::Report.new("#{Rails.root}/reports/bundle.odt") do |r|
		r.add_field :bundle, @bundle.title
		@locationName = @bundle.location.name
		if ( @bundle.location_id == 3) # у пользователя
			user = ExternalUsers.find(@bundle.external_users_id)
			@locationName += " - " + user.fio
		end
		if ( @bundle.location_id == 4 && @bundle.internal_users_id != 0 && @bundle.internal_users_id != nil ) # внутренее использование
			place = InternalUsers.find(@bundle.internal_users_id)
			@locationName += " - кабинет " + place.room
			if ( place.place != "" && place.place != nil )
				@locationName += " (" + place.place + ")"
			end
			@locationName += " " + place.note
		end
		r.add_field :place, @locationName
		r.add_field :today, Date.today.strftime("%d.%m.%Y г.")

		@row_number = 0
		r.add_table("TABLE_ITEMS", @items, :header=>true) do |t|
			t.add_column(:id) { |item| (@row_number += 1) }
		    t.add_column(:item, :title)
		    t.add_column(:serial, :main_serial)
		    t.add_column(:inventory_number, :inventory_number)
		    t.add_column(:amount, :amount)
		    t.add_column(:cost, :cost)
		    t.add_column(:note) { |items| "#{items.note}" }
		    t.add_column(:location) { |items| "#{items.location.name}" }
		end

		total_amount, total_cost, note = calctotals(@items)

		r.add_field :total_amount, total_amount
		r.add_field :total_cost, total_cost

	end

	send_data report.generate, type: 'application/vnd.oasis.opendocument.text',
                               disposition: 'attachment',
                               filename: 'комплект ' + @bundle.title + '.odt'
  end

  def internaluser
	place = InternalUsers.find(params[:id])
	# комплекты
    @items = Item.joins(:bundle).where( bundles: { :internal_users_id => params[:id], :location_id => 4} ).order(:order_number, :id)
    # элементы вне комплектов
    @items += Item.where(:internal_users_id => params[:id], :location_id => 4).order(:order_number, :id)

	report = ODFReport::Report.new("#{Rails.root}/reports/room.odt") do |r|
		@locationName = "Кабинет " + place.room
		if ( place.place != "" && place.place != nil )
			@locationName += " (" + place.place + ")"
		end
		@locationName += " " + place.note

		r.add_field :place, @locationName
		r.add_field :today, Date.today.strftime("%d.%m.%Y г.")

		@row_number = 0
		r.add_table("TABLE_ITEMS", @items, :header=>true) do |t|
			t.add_column(:id) { |item| (@row_number += 1) }
		    t.add_column(:item, :title)
		    t.add_column(:serial, :main_serial)
		    t.add_column(:inventory_number, :inventory_number)
		    t.add_column(:amount, :amount)
		    t.add_column(:cost, :cost)
		    t.add_column(:bundle) { |items| "#{items.getBundleTitle()}" }
		    t.add_column(:note) { |items| "#{items.note}" }
		    # t.add_column(:location) { |items| "#{items.location.name}" }
		end

		total_amount, total_cost, note = calctotals(@items)

		r.add_field :total_amount, total_amount
		r.add_field :total_cost, total_cost
	end

	send_data report.generate, type: 'application/vnd.oasis.opendocument.text',
                               disposition: 'attachment',
                               filename: @locationName  + '.odt'
  end
end
