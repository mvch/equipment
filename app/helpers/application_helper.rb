module ApplicationHelper

  def repairWarnings()
	repairs = []
	today = Date.today
	Repair.where('receive is null and sent is not null and control_days is not null').find_each do |r|
	  repairs << r if (r.sent + r.control_days.days) < today
	end
	countWarn = repairs.count()
	if (countWarn > 0)
    	render inline: '<div class="warnInfo">' + countWarn.to_s + '</div>'
    end
  end

  def appVersion
  	'1.0.22.197'
  end

  def copyright
  	today = Date.today.year
  	'2013 - ' + today.to_s
  end

  # def current_user_path
  # 	redirect_to url_for(:controller => :writeoff, :action => :index)
  # end

end
