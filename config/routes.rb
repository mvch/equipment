Equipment::Application.routes.draw do  
  devise_for :users


  get "internalusers/index"
  get "internalusers/items/:id" => "internalusers#items"
  get "bundle_check/index"
  get "item_check/index"
  
  get   'itemhistory/index'
  get   'itemhistory/:id'   => 'itemhistory#show'
  post  'itemhistory/clear' => 'itemhistory#clear'  

  get "search/:id" => "search#index"
  get "search/index"

  get 'reports/teacheract/:id' => 'reports#teacheract'
  get 'reports/studentact/:id' => 'reports#studentact'
  get 'reports/bundle/:id' => 'reports#bundle'
  get 'reports/internaluser/:id' => 'reports#internaluser'
  get 'reports/studentcontract/:id' => 'reports#studentcontract'
  get 'reports/teachercontract/:id' => 'reports#teachercontract'
  get 'reports/temporaryuse/:id' => 'reports#temporaryuse'
  get 'reports/temporaryuseact/:id' => 'reports#temporaryuseact'
  get 'reports/exportusers' => 'reports#exportusers'
  get 'reports/exportbundlechecks' => 'reports#exportbundlechecks'
  get "reports/index"
  get "repair/index"
  get 'repair/places' => 'repair#places'
  get "writeoff/index"
  get "warehouse/index"
  get "students/index"
  get "teachers/index"
  get "externalusers/index"
  get 'externalusers/select'
  get 'externalusers/providers' => 'externalusers#providers'
  get 'externalusers/names' => 'externalusers#names'
  get 'externalusers/patronymics' => 'externalusers#patronymics'
  
  get   'bundlehistory/index'
  get   'bundlescount/index'  => 'bundles#bundlescount'
  get   'bundlehistory/:id'   => 'bundlehistory#show'
  post  'bundlehistory/clear' => 'bundlehistory#clear'  
  
  get   'items/index'
  get   'items/titles'                    => 'items#titles'
  post  'items/writeOffPartial'           => 'items#writeOffPartial'  
  post  'items/checkExistsSerial'         => 'items#checkExistsSerial'
  post  'items/moveItemToStock'           => 'items#moveItemToStock'
  post  'items/moveItemToTemporaryUse'    => 'items#moveItemToTemporaryUse'
  post  'items/returnItemFromTemporaryUse'=> 'items#returnItemFromTemporaryUse'
  post  'items/moveBundleToStock'         => 'items#moveBundleToStock'  
  post  'items/changeordernumber'         => 'items#changeOrderNumber'

  get 'warehouse/index'
  get "testbundle/index"
  get "bundles/index"
  get "testitems/index"
  get "welcome/index"
  get "viewhistory/index"
  get "bundleitems/index"

  get "temporary_use/index"
  
  # get 'persons/profile', as: 'user_url'
  
  # get "public"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'search#index'

  resources :test_items
  resources :bundles
  resources :items
  resources :bundleitems
  resources :externalusers
  resources :repair
  resources :item_check
  resources :bundle_check
  resources :internalusers
  resources :temporary_use


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
